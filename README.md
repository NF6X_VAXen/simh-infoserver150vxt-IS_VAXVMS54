# simh-infoserver150vxt-VAXVMS54: Simulation of DEC InfoServer 150VXT providing VMS 5.4 consolidated binary distribution CD-ROMs

This is a pre-configured [SIMH](https://github.com/simh/simh) instance to simulate a [Digital Equipment Corporation (DEC)](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation) InfoServer 150VXT system, serving VMS consolidated binary distribution CD-ROMs over a LAN. This uses non-routable protocols such as [MOP](https://en.wikipedia.org/wiki/Maintenance_Operations_Protocol) and LAD, so it only works when the InfoServer emulation and any of its clients are on the same LAN.

**This repository does not include any VMS Product Authorization Keys (PAKs).** You can install the VMS operating system and layered products on a real or simulated VAX over a LAN with the simulation instances in this repository, but you will need to obtain valid PAKs to actually use the software. The author of this repository cannot provide PAKs.

The major contents of this repository are the distribution files in the `dist` subdirectory. They are a collection of CD-ROM images and scanned documentation uploaded to The Internet Archive by user `vaxstation3100`, and available at [VAX VMS 5.4 by Digital Equipment Corp.](https://archive.org/details/VAXVMS5.4)

**Note that the distribution includes several files whose names differ only in letter case.**

For convenience, I have also extracted the text and Postscript documentation from the CD-ROM images, and placed it in the `LINE_DOCS` and `POST_DOCS` subdirectories, respectively. These directories include cover letters Software Product Descriptions, and installation guides. The followgin files from the `CDROM` directories at the root of each CD-ROM describe the overall contents of the whole distribution set:

* [LINE_DOCS/CDMASTER_INDEX.TXT](LINE_DOCS/CDMASTER_INDEX.TXT)
* [LINE_DOCS/SPECIAL_NOTES.TXT](LINE_DOCS/SPECIAL_NOTES.TXT)
* [POST_DOCS/CDMASTER_INDEX.PS](POST_DOCS/CDMASTER_INDEX.PS)
* [POST_DOCS/SPECIAL_NOTES.PS](POST_DOCS/SPECIAL_NOTES.PS)

This simulation was set up using the InfoServer software provided on the OpenVMS Freeware v80 CD-ROM, which may be found [here](https://www.digiater.nl/openvms/freeware/v80/infoserver/). I have also archived InfoServer software and documentation for convenience [here](https://gitlab.com/NF6X_VAXen/InfoServer-docs).


## System Configuration

Description            | Default setting
-----------------------|------------------
Server name            | IS_VAXVMS54
Administrator password | ess
Network interface      | vde:/var/run/vde2/tap1.ctl


## Using the Simulation

Before you can run this simulation instance, you will need to download and install the [SIMH](https://github.com/simh/simh) software, particularly the `infoserver150vxt` simulator. You will probably need to edit the initialization file provided here to configure networking for your host system. The initialization file assumes that you will be using a [Virtual Distributed Ethernet](https://github.com/virtualsquare/vde-2) switch with its control port located at `/var/run/vde2/tap1.ctl`. Depending on your host operating system, you may need to use a different network configuration. SIMH network configuration is described in the [0readme_ethernet.txt](https://github.com/simh/simh/blob/master/0readme_ethernet.txt) file provided with SIMH.

To launch the simulation:

   infoserver150vxt IS_VAXVMS54.ini

To shut it down, log in with the administrator password, either on the console or via [LAT](https://en.wikipedia.org/wiki/Local_Area_Transport). Then use the `shutdown` command.

You might find it helpful to run the simulation in a detached `screen` session. The included script `start-screen.sh` starts the simulator with its console in a `screen` session, with the `screen` escape key set to `^p` so that it does not conflict with SIMH's use of `^e` as its escape key.

An included DCL script shows how to mount disks from the InfoServer simulation under VMS.


## File Manifest

Filename                       | Description
-------------------------------|--------------------------------------------
IS_VAXVMS54.ini                | SIMH `infoserver150vxt` initialization file
IS_VAXVMS54.nvr                | InfoServer nonvolatile RAM image
IS_VAXVMS54_system_rz1.rz24    | InfoServer system disk image
MOUNT_VAXVMS54.COM             | Example DCL script to mount CD-ROMs from InfoServer
README.md                      | This file
start-screen.sh                | Example shell script to launch simulator in a detachable `screen` session
dist/                          | Subdirectory containing the distribution CD-ROM images and associated documentation as downloaded from The Internet Archive
LINE_DOCS/                     | Plain text documentation extracted from LINE_DOCS directories in the CD-ROM images
POST_DOCS/                     | Postscript documentation extracted from POST_DOCS directories in the CD-ROM images


