 






          DIGITAL

          Read Before Installing or Using ALL-IN-1 MAIL for Video
          Terminals (VT) Version 1.0

          AV-PCXBA-TE

          ALL-IN-1 MAIL for Video Terminals (VT) is an electronic mes-
          saging application that conforms to the international messaging
          standards set by the CCITT X.400 recommendations. This means
          ALL-IN-1 MAIL allows you to communicate with other computer
          users, even when your computer systems are made by different
          manufacturers or are otherwise incompatible, and even when your
          computer systems are installed in different countries.

          Please take time to read the following information about ALL-IN-
          1 MAIL for Video Terminals (VT).

          Installation Information

          ALL-IN-1 MAIL Version 1.0 requires VMS Version 5.2.

          Release Notes Information

          The release notes for ALL-IN-1 MAIL Version 1.0 contain impor-
          tant installation-related instructions and a summary of tech-
          nical changes, new features, differences, known problems, cor-
          rected errors, performance enhancements, documentation errors,
          restrictions, and incompatibilities.

          The release notes for ALL-IN-1 MAIL for Video Terminals (VT) can
          be found in A1MAIL010.RELEASE_NOTES in SYS$HELP.

          If you try to use the command MODIFY ATTACHMENT with a message
          that has no attachments, you will abort your ALL-IN-1 MAIL
          session. Do not use this command with a message that has no
          attachments.

 






          If you extract non-text bodyparts or bodyparts to be used as
          distribution lists, be sure NOT to include any header in-
          formation or the recipient list in the extraction. Including
          such extraneous information may cause unpredictable results.
          To correctly extract such a bodypart using the Video Terminal
          interface, use the EXTRACT/BRIEF command.

          Contents of This Kit

          The ALL-IN-1 MAIL kit contains:

          o  Indented Bill Report (BIL) and Bill of Materials (BOM)

             Please read the BIL and BOM enclosed in this kit and check to
             see that all items listed are actually in your kit. If your
             kit is damaged or any items are missing, call your Digital
             representative.

             ©Digital Equipment Corporation. 1990. All rights reserved.




















                                          2

 






          o  Documentation

             Depending on your order, this kit may include copies of the
             following ALL-IN-1 MAIL documentation:

             o  The ALL-IN-1 MAIL for Video Terminals (VT) User's Guide
                This guide provides both basic and advanced user informa-
                tion for all users of ALL-IN-1 MAIL for Video Terminals
                (VT).

             o  Software Product Description (SPD)
                The SPD provides an overview of the ALL-IN-1 MAIL kit and
                its features.

             o  System Support Addendum (SSA)
                The SSA describes the technical environment in which the
                product is supported.

             o  Software Performance Report (SPR)
                The SPR is provided for you to report any problems with
                ALL-IN-1 MAIL, if you have purchased warranty services.

             o  Software Update Survey Card
                This card is provided for you to provide valuable feedback
                to Digital on the quality of our installation. By complet-
                ing this card after you install ALL-IN-1 MAIL, you help us
                understand our customers' needs and improve our products.

          A Final Note

          Digital prides itself on responding to customer needs. In order
          to continue serving you, we need your comments. Each manual
          contains preaddressed, postage-paid Reader's Comments forms
          at the back. If you find errors in a manual or want to make
          comments about it, please fill out one of these forms and send
          it to us.



                                          3
