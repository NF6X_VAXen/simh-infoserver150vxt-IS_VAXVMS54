 






          DIGITAL

          Read Before Installing or Using VAX DEC/Test Manager Version 3.2

           AV-DH56K-TE

          Whether you are a new or a continuing VAX DEC/Test Manager
          customer, please take time to read the following information
          about your product.

          Installation Information

          VAX DEC/Test Manager Version 3.2 requires VMS Version 5.3 or
          later.

          Before you install VAX DEC/Test Manager Version 3.2, you must
          register the product using the Product Authorization Key (PAK)
          provided in the kit. See the VMS License Management Utility
          Manual for registration instructions. Previous versions of
          VAX DEC/Test Manager do not need to be installed before this
          version.

          Release Notes Information

          The release notes for VAX DEC/Test Manager Version 3.2 con-
          tain important installation-related instructions and a summary
          of technical changes, new features, differences, known prob-
          lems, corrected errors, performance enhancements, documentation
          errors, restrictions, and incompatibilities.

          Contents of This Kit

          o  Indented Bill Report (BIL) and Bill of Materials (BOM)

             Please read the BIL and BOM enclosed in this kit and check to
             see that all items listed are actually in your kit. If your
             kit is damaged or any items are missing, call your Digital
             representative.

          o  Media

 






             If you ordered media, you will find the media and the VAX
             DEC/Test Manager Installation Guide in this kit. Consult the
             VAX DEC/Test Manager Installation Guide for information about
             installing VAX DEC/Test Manager on your system.

          o  Documentation

             Depending on your order, this kit may include copies of the
             following VAX DEC/Test Manager documentation:

                Guide to VAX DEC/Test Manager
                VAX DEC/Test Manager Quick Reference Guide

             Note that the VAX DEC/Test Manager Installation Guide is
             included with the media.

             ©Digital Equipment Corporation. 1990. All rights reserved.






















                                          2

 






          o  Product Authorization Key (PAK)

             The PAK contains information necessary to register VAX
             DEC/Test Manager before you install it.

          o  Software Product Description (SPD)

             The SPD provides an overview of the VAX DEC/Test Manager kit
             and its features.

          o  Software Performance Report (SPR)

             Use this form to report any problems with VAX DEC/Test Man-
             ager, provided you have purchased warranty services.

          o  Software Update Survey Card

             By completing this card after you install VAX DEC/Test Man-
             ager, you provide valuable feedback to Digital on installa-
             tion quality. Your feedback helps us to understand customer
             needs and improve our products.

          A Final Note

          Digital prides itself on responding to customer needs. In order
          to continue serving you, we need your comments. Each manual
          contains preaddressed, postage-paid Reader's Comments forms
          at the back. If you find errors in a manual or want to make
          comments about it, please fill out one of these forms and send
          it to us.









                                          3
