 






     System
     Support
     Addendum

     ________________________________________________________________
     ________________________________________________________________

     PRODUCT NAME:  DEC ODA Compound Document Architecture (CDA)  SSA
     32.10.00-A
                    Gateway for VMS, Version 1.0

     HARDWARE REQUIREMENTS

     Processors Supported

     VAX:        VAXft 3000-310

                 VAX 4000 Model 200,
                 VAX 4000 Model 300

                 VAX 6000 Model 200 Series,
                 VAX 6000 Model 300 Series,
                 VAX 6000 Model 400 Series,
                 VAX 6000 Model 500 Series

                 VAX 8200, VAX 8250, VAX 8300, VAX 8350, VAX 8500,
                 VAX 8530, VAX 8550, VAX 8600, VAX 8650, VAX 8700,
                 VAX 8800, VAX 8810, VAX 8820, VAX 8830, VAX 8840

                 VAX 9000-210, VAX 9000 Model 400 Series

                 VAX-11/780, VAX-11/785

     MicroVAX:   MicroVAX 2000,
                 MicroVAX 3100, MicroVAX 3300,
                 MicroVAX 3400, MicroVAX 3500,
                 MicroVAX 3600, MicroVAX 3800,
                 MicroVAX 3900

                                  DIGITAL                  March 1991

                                                          AE-PB6RA-TE

 


     DEC ODA Compound Document Architecture (CDA)      SSA 32.10.00-A
     Gateway for VMS, Version 1.0



     VAXstation: VAXstation II, VAXstation 2000,
                 VAXstation 3100 Series, VAXstation 3200,
                 VAXstation 3500, VAXstation 3520,
                 VAXstation 3540

     VAXserver:  VAXserver 3100, VAXserver 3300, VAXserver 3400,
                 VAXserver 3500, VAXserver 3600, VAXserver 3602,
                 VAXserver 3800, VAXserver 3900

                 VAXserver 4000 Model 300

                 VAXserver 6000-210, VAXserver 6000-220, VAXserver
                 6000-310, VAXserver 6000-320, VAXserver 6000-410,
                 VAXserver 6000-420, VAXserver 6000-510, VAXserver
                 6000-520

     Processors Not Supported

     VAX:        VAX-11/725, VAX-11/730, VAX-11/750, VAX-11/782


     MicroVAX:   MicroVAX I, MicroVAX II

     VAXstation: VAXstation I, VAXstation 8000

     Processor Restrictions

     A TK50 Tape Drive is required for standalone MicroVax 2000 and
     VAXstation 2000 systems.

     Suggested Configurations

     o  VAX 11/785 with a minimum of 12MB main memory

     o  VAXstation 2000 running DECwindows with a minimum of 12MB
        main memory installed on a VAXcluster


                                     2

 


     DEC ODA Compound Document Architecture (CDA)      SSA 32.10.00-A
     Gateway for VMS, Version 1.0


     o  VAXstation 3500 running DECwindows with a minimum of 32MB of
        main memory

     Disk Space Requirements (Block Cluster Size = 1):

     Disk space required for      1,600 blocks
     installation:
                                  (819.2 Kbytes)

     Disk space required for      1,500 blocks
     use (permanent):
                                  (768.0 Kbytes)

     These counts refer to the disk space required on the system
     disk. The sizes are approximate; actual sizes may vary depending
     on the user's system environment, configuration, and software
     options.

     Memory requirements for DECwindows Support

     Minimum supported memory for this application running in a
     standalone DECwindows environment with DECwrite is 12 MB of
     main memory.

     CLUSTER ENVIRONMENT

     This layered product is fully supported when installed on any
     valid and licensed VAXcluster* configuration without restric-
     tions. The HARDWARE REQUIREMENTS sections of this product's
     Software Product Description and System Support Addendum detail
     any special hardware required by this product.

     *  V5.x VAXcluster configurations are fully described in the
        VAXcluster Software Product Description (29.78.xx) and in-
        clude CI, Ethernet, and Mixed Interconnect configurations.




                                     3

 


     DEC ODA Compound Document Architecture (CDA)      SSA 32.10.00-A
     Gateway for VMS, Version 1.0


     SOFTWARE REQUIREMENTS

     For Systems Using Terminals (No DECwindows Interface):

     VMS Operating System V5.4 (and necessary components of VMS
     DECwindows)

     For Workstations Running VWS:

     VMS Operating System V5.4 (and necessary components of VMS
     DECwindows)
     VMS Workstation Software V4.3

     For Workstations Running DECwindows:

     VMS Operating System V5.4 (and necessary components of VMS
     DECwindows)

     VMS DECwindows is part of the VMS Operating System but must
     be installed separately. Installation of VMS DECwindows gives
     users the option to install any or all of the following three
     components:

     o  VMS DECwindows Compute Server (Base kit; includes runtime
        support)

     o  VMS DECwindows Device Support

     o  VMS DECwindows Programming Support

     For stand-alone execution, the following DECwindows components
     must be installed on the machine:

     o  VMS DECwindows Compute Server (Base kit; includes runtime
        support)

     For X.400 transmission, the Message Transport System used must
     be encoded to support ODA Body parts over a CCITT X.400-1984
     service.

     VMS Tailoring

                                     4

 


     DEC ODA Compound Document Architecture (CDA)      SSA 32.10.00-A
     Gateway for VMS, Version 1.0


     The following VMS class is required for full functionality of
     this layered product:

     o  VMS Required Save Set

     For more information on VMS classes and tailoring, refer to
     the VMS Operating System Software Product Description (SPD
     25.01.xx).

     OPTIONAL SOFTWARE

     DECwrite for VMS V1.1
     A Message Transport System that supports ODA Body
        parts over CCITT X.400-1984

     GROWTH CONSIDERATIONS

     The minimum hardware/software requirements for any future ver-
     sion of this product may be different from the requirements for
     the current version.

     DISTRIBUTION MEDIA

     Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

     This product is also available as part of the VMS Consolidated
     Software Distribution on CDROM.

     ORDERING INFORMATION

     Software Licenses: QL-YHNAA*-**
     Software Media: QA-YHN**-**
     Software Documentation: QA-YHNAA-GZ
     Software Product Services: QT-YHNA*-**





                                     5

 


     DEC ODA Compound Document Architecture (CDA)      SSA 32.10.00-A
     Gateway for VMS, Version 1.0


     *  Denotes variant fields. For additional information on avail-
        able licenses, services, and media, refer to the appropriate
        price book.

     The above information is valid at time of release. Please con-
     tact your local Digital office for the most up-to-date informa-
     tion.

     [TM] The DIGITAL Logo, CDA, DEC, DECwindows, DECwrite, LiveLink,
          MicroVAX, VAX, VAXcluster, VAXft, VAXserver, VAXstation,
          and VMS are trademarks of Digital Equipment Corporation.




























                                     6
