






         d i g it a lTM                                         AV-PDL5B-TE



                             F�lgebrev  -  L�s f�r installation


          Tillykke med erhvervelsen af Digital's WYSIWYG
          tekstbehandlingsprogram,  DECwrite/DANSK for VMS, V1.1. L�s
          venligst dette f�lgebrev f�r installation af DECwrite. Digital
          opfordrer endvidere til at gennemse Release Notes f�r brug af
          DECwrite. Release Notes kan udskrives under installationen.


          Installation

          H�ndbogen  DECwrite Installation Guide  for VMS Systems
          indeholder de n�dvendige informationer til installation af
          DECwrite. Navnet p� det danske software-kit er  DECWRITEDA011 og
          sprogkoden  er DA_DK. F�r installationen  b�r kommandoen:

          $ DEFINE/SYSTEM XNL$LANG  "DA_DK"  eksekveres.

          N�dvendig diskplads for installation:           29,600 blokke
                                   			  (14,8 Mb)

          N�dvendig diskplads for brug (permanent) :      26,400 blokke
                                   			  (13,2 Mb)


          Forskellige VMS versionsnumre

          DECwrite V1.0 dokumentationen refererer til VMS V5.1, mens
          dokumentationen til DECwrite V1.1  refererer til VMS V5.4.


          Dokumentation

          F�lgende manualer  udg�r dokumentationss�ttet til  DECwrite/DANSK
          for VMS V1.1 :

          �DECwrite Begynderbog
          �DECwrite Brugerh�ndbog
          �DECwrite Supplementsbind *
          �DECchart Brugerh�ndbog*
          �Using the DECwrite CDA Converters *
          �DECchart Getting Started
          �DECwrite Reference Manual
          �DECwrite Quick Lookup
          �DECwrite Sample Document



          � 1990 Digital Equipment Corporation. Alle rettigheder
          forbeholdes.
          Side 1








          Hvis du er ny DECwrite kunde indeholder kittet alle ovenn�vnte
          b�ger.
          Hvis du allerede er DECwrite kunde indeholder kittet de b�ger som
          er markeret med en stjerne (*) i listen ovenfor.


          Til st�tte for krydsreference mellem DECwrite's danske og
          amerikanske terminologi er udarbejdet da-eng og eng-da ordlister.
          Her st�r begreber og menuvalg, fx Style File = M�nsterdokument,
          Style = Elementtype, Link = Forbindelse, Letterspace =
          Spatiering. Ordlisten omfatter ogs� felter og knapper i
          dialogbokse, fx Load From Current Paragraph = Overf�r kendetegn
          fra aktuelt afsnit. Ordlisten findes i DECwrite Brugerh�ndbog,
          till�g B.

          Der er endvidere udarbejdet en oversigt over menu'er i DECwrite
          med tilh�rende accelerator-tastfunktioner. Den findes i
          SYS$COMMON:[SYSHLP.EXAMPLES.DA_DK.DECWRITE]V11_MENUOVERSIGT.PS.


          DECwrite Software Product Description (SPD)

          DECwrite SPD (med tilh�rende System Software Addendum, SSA) giver
          en beskrivelse af funktionaliteten i DECwrite/DANSK for VMS V1.1.
          Digital indest�r for, at produktet er i overensstemmelse hermed.


          Software Performance Report (SPR)

          Benyt denne formular til at rapportere eventuelle konstaterede
          fejl  opst�et under normal brug af programmelproduktet  som
          foreskrevet i SPD'en.


          Grafisk bibliotek

          DECwrite V1.1 indeholder et bibliotek med grafik i Encapsulated
          Postscript format. Grafikken er licensieret til Digital Equipment
          Corporation, Maynard, Massachusetts.  Copyright (c) Studio
          Advertising Art 1988. Alle rettigheder forbeholdes.   �nskes
          yderligere oplysninger om grafikken  kan Studio Art Advertising,
          P.O.Box 43912, Las Vegas, Nevada, U.S.A, kontaktes.














          � 1990 Digital Equipment Corporation. Alle rettigheder
          forbeholdes.
          Side 2
