

September 1990
BH-LG76D-TE

SOFTWARE PRODUCT DESCRIPTION

PRODUCT NAME: VAX/EDI (UK) Version 1.1                          SPD 29.45.03


This product is normally supplied in the U.K. only. For availability and
conditions of supply in other countries, please contact the local DIGITAL
office.

DESCRIPTION

VAX/EDI is a VMS layered application that provides the facilities 
necessary for Electronic Data Interchange (EDI). It enables end users to 
electronically exchange business documents such as 'Orders' and 
'Invoices' with their trading partners. It implements the TRADACOMS (TM) EDI 
standard (Update 4, 1st August 1987 and Update 5, December 1989) as defined by 
the Article Numbering Association (ANA). 

It also provides all the facilities necessary to interwork with the 
TRADANET (TM) EDI service to enable end users to communicate with their 
trading partners. The TRADANET EDI service is accessed via British 
Telecom's Packet Switchstream (PSS) (TM) through the use of VAX P.S.I.

VAX/EDI consists of the following components:


o A set of user-callable routines, to allow end users to link 
  existing or new applications to VAX/EDI. These routines 
  are used to:

  - Post data from the application to VAX/EDI for 
    translation to the TRADACOMS format prior to 
    transmission to the EDI service

  - Fetch data, received in TRADACOMS format from the 
    EDI network by VAX/EDI, for processing by the 
    application, once the format has been converted to 
    that required by the application.

o An implementation of the TRADACOMS EDI Standard
  (TRADACOMS Update 4 and update 5) to translate outgoing 
  documents into TRADACOMS format and to convert incoming 
  documents from the TRADACOMS format to that required by 
  the application.

o A communications component that provides the full set 
  of commands required to interwork with the TRADANET EDI
  service.      	
 	
o An administrator's interface which allows a designated user 
  to maintain the VAX/EDI system, adjust system parameters, and 
  perform any error recovery that may be necessary.

o A supervisor's interface which allows designated users to 
  monitor the status of the documents in the VAX/EDI system 
  via a menu driven interface.


In addition the following set of tools are provided:

o A syntax specific editor to enable the user to configure the 
  VAX/EDI formatting tables. The VAX/EDI formatting tables are 
  used to translate the application data to TRADACOMS format 
  and vice versa.

o A tool to carry out the certification tests specified by the 
  TRADANET EDI service for gaining approval to connect to the 
  Service and to check the operational status of VAX/EDI. The 
  tests are carried out in three stages and consist of : 
            
  - Stage-1, which tests the network access to the TRADANET EDI
    Service
	    
  - Stage-2, which exercises the full set of network commands to
    ensure correct inter-working with the TRADANET EDI Service

  - Stage-3, which performs full system verification by sending 
    and receiving test data in TRADACOMS format to
    and from the EDI Network
 

INSTALLATION

Only experienced customers should attempt installation of this product. 
Digital recommends that all other customers purchase DIGITAL's 
Installation Services. These services provide for installation of the 
product by an experienced DIGITAL Software Specialist.


HARDWARE REQUIREMENTS

VAX, VAXstation, VAXserver and MicroVAX configurations as specified in the 
System Support Addendum (SSA).


SOFTWARE REQUIREMENTS

VMS Operating System

VAX P.S.I. or P.S.I. ACCESS 

Refer to the System Support Addendum (29.45.**-A) for availability and required 
versions of prerequisite software.


ORDERING INFORMATION                                            

Software Licenses: QL-VHWA*-**
Software Media: QA-VHWAA-**
Software Documentation: QA-VHWAA-GZ
Software Product Services: QT-VHWA*-**

* Denotes variant fields. For additional information on available licenses,
services and media refer to the appropriate price book.


SOFTWARE LICENSING

This software is furnished under the licensing provisions of DIGITAL's 
Standard Terms and Conditions of Sale. For more information about 
DIGITAL's licensing terms and policies, contact your local DIGITAL 
office.


LICENSE MANAGEMENT FACILITY SUPPORT

This layered product supports the VMS License Management Facility.

License units for this product are allocated on a CPU-capacity basis.

For more information on the License Management Facility, refer to the
VMS Operating System Software Product Description (SPD 25.01.xx) or 
the License Management Facility manual of the VMS Operating System 
documentation set.

For more information about DIGITAL's licensing terms and policies, contact 
your local DIGITAL office.
 

SOFTWARE PRODUCT SERVICES

A variety of service options are available from DIGITAL. For more 
information contact your local DIGITAL office.


SOFTWARE WARRANTY

DIGITAL will not provide warranty for the formatting tables that are 
shipped to the customer on the distribution media containing the binaries
for this product.  

Warranty for this software product is provided by DIGITAL with the 
purchase of a license for the product as defined in the Software Warranty 
Addendum.


TRADANET is a trademark of International Network Services.
PSS and Packet Switchstream are trademarks of British Telecom.
TRADACOMS is a trademark of the Article Numbering Association.


