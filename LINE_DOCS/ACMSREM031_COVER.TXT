          DIGITAL                                     AV-U721H-TE


                  Before You Install VAX ACMS Version 3.1A

          This package contains the VAX ACMS Version 3.1A software,
          installation instructions, and user documentation.

          Follow these steps to install VAX ACMS Version 3.1A on your
          VMS operating system:

          1. Read the Bill of Materials enclosed in the transparent
             plastic envelope. Check to determine that all items listed
             there are in your kit. If any items are missing, con-
             tact your Digital representative.

          2. Make sure that you have Version 5.0 or higher of VMS in-
             stalled on your system.

             PLEASE NOTE: If you upgrade to VMS Version 5.4 or higher
             at some future time after you install VAX ACMS Version
             3.1A, you will then need to re-install VAX ACMS Version
             3.1A (you can use the VAX ACMS software contained in this
             kit for this re-installation). If you already have VMS
             Version 5.4 installed before you install VAX ACMS Ver-
             sion 3.1A, you are all set.

          3. If you are planning to install the ACMS run-time or de-
             velopment kit on your system, make sure that CDD/Plus
             Version 4.1 or higher is installed on your system.

          4. If you plan to use DECforms with VAX ACMS, make sure DEC-
             forms Version 1.1 or higher is installed on your sys-
             tem.

          5. If you plan to use LSE with VAX ACMS, make sure LSE Ver-
             sion 2.0 or higher is installed on your system.

          6. If you plan to use TDMS with VAX ACMS, make sure TDMS
             Version 1.7 or higher is installed on your system.

          7. If you plan to use TEAMDATA with VAX ACMS, make sure TEAM-
             DATA Version 1.3 or higher is installed on your system.

          The System Support Addendum (SSA) contains a complete list
          of the products that are compatible with this version of
          VAX ACMS. The SSA is part of the Software Product Descrip-
          tion (SPD).

           � Digital Equipment Corporation. 1990. All rights reserved.
