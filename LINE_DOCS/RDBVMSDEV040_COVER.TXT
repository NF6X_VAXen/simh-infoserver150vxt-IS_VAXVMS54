 






          DIGITAL                                              AV-N037M-TE


                         Before You Install VAX Rdb/VMS V4.0

          Enclosed is the kit for VAX Rdb/VMS Version 4.0 software (re-
          ferred to as Rdb/VMS V4.0), which requires VMS Version 5.3
          (V5.3) or higher.

                                        NOTE

             The two-phase commit protocol of Rdb/VMS V4.0 requires
             VMS V5.4, which includes DECdtm Services. Rdb/VMS returns
             the following error if you attempt to use DECdtm Services
             while running Rdb/VMS under VMS V5.3:

                  %RDB-F-NODECDTM, DETdtm is not installed on your system

             Rdb/VMS may attempt to invoke DECdtm Services implic-
             itly if you use the SQL interface and access two or more
             databases in a transaction. This causes the previous er-
             ror to be returned if you are running VMS Version 5.3.
             To avoid this error message, disable the two-phase commit
             protocol by defining the following logical name:

                  $ DEFINE SQL$DISABLE_CONTEXT TRUE

             However, be sure to deassign this logical name after you
             install VMS Version 5.4 to receive the benefit of the
             two-phase commit protocol. SQL evaluates whether the two-
             phase commit protocol is enabled or disabled at compile
             time. For more information, see the VAX Rdb/VMS Guide to
             Distributed Transactions.

          New customers receive a separate VMS License Management Facility
          (LMF) Product Authorization Key (PAK); service customers get a
          Service Update PAK (SUP) with their kit. Register the product
          with LMF before installation.

                                                (continued on other side)

 






          Rdb/VMS V4.0 includes a new database conversion utility that
          converts V3.0, V3.0A, V3.0B, V3.1, V3.1A, and V3.1B databases to
          V4.0 format. See Chapter 1, Section 1.22.4 of the VAX Rdb/VMS
          Release Notes for more information on the RMU/CONVERT command,
          syntax, and using this database conversion utility.

          The online (TXT) file supplied with the Rdb/VMS software kit
          and Bookreader documentation for the V4.0 VAX Rdb/VMS Release
          Notes are more recent than the printed version. Notes have been
          added, revised, and removed. For details, consult the online or
          Bookreader documentation version of the V4.0 VAX Rdb/VMS Release
          Notes.

          The following notes address issues of particular importance to
          system managers and users.

          1  Changes to Run-Unit Journal (RUJ) Files

          The following changes have been made to RUJ file default cre-
          ation:

          o  RUJ file naming convention changed

             The RUJ file naming convention is changed to:

                  <database_name>$<timestamp generated number>.RUJ

             This prevents multiple versions of RUJ files from using the
             same file name and the possibility of RUJ files being lost
             from a random purge of directories.









                                                  (continued on next page)


                                          2

 






          o  RUJ file placement changed

             RUJ file placement no longer defaults to SYS$LOGIN direc-
             tory. RUJ files now default to their own top level directory
             [RDM$RUJ] on the device in which SYS$LOGIN is defined. This
             avoids the problem of RUJ files being scattered around in
             users' directories. If a system manager wishes to change the
             default placement semantics, the RDMS$RUJ logical name can
             be defined systemwide to SYS$LOGIN and the "old" placement
             scheme of versions prior to Rdb/VMS V4.0 is used.

             The RDMS$RUJ logical name used for RUJ placement has not
             changed. The [RDM$RUJ] directory is created automatically
             by the first user on the disk who creates an RUJ file, and
             remains there indefinitely. It is owned by the [SYSTEM]
             identifier. The [SYSTEM] identifier must have write (W) and
             execute (E) privileges to create this top level directory
             [RDM$RUJ]. You can create ACLs on it if you wish, as the
             users are given privileges internal to Rdb/VMS to create or
             delete RUJ files. The RUJ files created in the directory are
             owned by the individual users.

                                       CAUTION

             The VAX Rdb/VMS Guide to Database Maintenance and Perfor-
             mance, Section 9.3, describes what to do if you have lost
             your AIJ file because of a disk drive problem. The man-
             ual suggests you create a dummy AIJ file in the directory
             where the root file says it should be located to get your
             database up and running again. Though not documented, this
             method can also be used for missing RUJ files in versions
             previous to Rdb/VMS V3.1; however, your database may be
             left in an inconsistent state and no error message warns
             you of this fact.

             This technique was not supported in Rdb/VMS V3.1 and is
             not supported for Rdb/VMS V4.0. Beginning with Rdb/VMS
             V4.0, RMU/ALTER includes syntax (see the Documentation
             Errors and Omissions section in the VAX Rdb/VMS Release
             Notes) to remove pointers to missing RUJ files. Because

                                                 (continued on other side)

                                         3

 






             using this technique will cause corruption, the database
             is marked as "eternally corrupt." To remove the "eternally
             corrupt" database status, restore and recover the database
             from a clean backup and AIJ file.


          2  Installing RMU with Privileges

          After the Rdb/VMS installation, install RMU.EXE with the VMS
          SYSPRV privilege. This enables the RMU image to check for appli-
          cable Rdb/VMS privileges when a user enters RMU commands. If you
          install RMU.EXE without SYSPRV, users must have VMS SYSPRV or
          BYPASS privilege to execute some of the RMU commands.

          To install RMU with the SYSPRV privilege, enter the following
          command:

               $ INSTALL
               INSTALL> ADD SYS$SYSTEM:RMU.EXE /OPEN/HEADER/PRIV=(SYSPRV)

          The VMS INSTALL command requires the VMS CMKRNL privilege be-
          cause the RMU image is being installed with privileges.

          If you previously used the VMS Install utility (INSTALL) to
          install the RMU.EXE image, enter the REPLACE command instead of
          the ADD command on the INSTALL command line.













                                                  (continued on next page)


                                          4

 






          RMU needs to reference each of the following images. Install the
          following images:

               $ INSTALL ADD SYS$COMMON:[SYSMSG]RDMSMSG.EXE/SHARE
               $ INSTALL ADD SYS$COMMON:[SYSMSG]RMUMSG.EXE/SHARE

          See the VAX Rdb/VMS Installation Guide or the VAX Rdb/VMS Guide
          to Database Design and Definition for information about the
          privileges required for RMU. See the VAX Rdb/VMS RDO and RMU
          Reference Manual for more information about the RMU utility. See
          the VMS Install Utility Manual in the VMS documentation set for
          more information about the VMS Install utility.

          3  System Management Information for the SQL/Services Server

          Sites planning to install either the Rdb/VMS V4.0 interactive
          or run-time kit exclusively (rather than the full development
          kit) can find documentation about managing a SQL/Services server
          in the appendix to the VAX Rdb/VMS Release Notes. Because the
          VAX Rdb/VMS Guide to Using SQL/Services, which contains the
          server system management information, is shipped only with the
          Rdb/VMS full development kit, the appendix has been added to the
          VAX Rdb/VMS Release Notes for Rdb/VMS V4.0 to provide essential
          server information otherwise not provided in the documentation
          kit supplied with the Rdb/VMS interactive and run-time kits.














             � Digital Equipment Corporation. 1990. All rights reserved.


                                          5
