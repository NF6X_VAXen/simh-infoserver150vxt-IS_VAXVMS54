

							 	     AV-PBGZB-TE
			� LIRE IMP�RATIVEMENT AVANT TOUTE 
				INSTALLATION

Le pr�sent kit de distribution comporte le logiciel DECdecision/FRAN�AIS
V1.1TM, les  instructions d'installation et la documentation utilisateur.

Contenu du kit de distribution

Le kit comporte les �l�ments suivants :

- Une nomenclature
V�rifiez que le kit comporte tous les �l�ments mentionn�s dans la nomenclature
fournie  sous film plastique. Si un des �l�ments fait d�faut, contacter
DIGITAL.
- Un support de distribution
Si vous avez command� le logiciel, vous trouverez dans le pr�sent kit un
support de distri bution et le manuel DECdecision Installation and System
Management Guide. Ce manuel  comporte toutes les instructions d'installation de
DECdecision V1.1 sous VMS Version 5.3- 1 ou ult�rieure.

-Un jeu de documentation
Si vous avez command� un kit complet ou uniquement un jeu de documentation (kit
GZ), vous trouverez dans le pr�sent kit de distribution un jeu de
documentation  DECdecision/FRAN�AIS  V1.1 qui comprend les manuels suivants :

  	DECdecision Analyse et gestion int�gr�es
 	DECdecision Installation and System Management Guide
 	DECdecision Overview
	DECdecision Calc User's Guide 
	DECdecision Calc Macro Guide 
	DECdecision Access User's Guide 
	DECdecision Builder User's Guide 
	DECchart User's Guide
	Converters Reference Guide

Description de produit logiciel (DPL)

La Description de produit logiciel de DECdecision donne une description du
produit DECdecision/FRAN�AIS  V1.1 et de ses fonctionnalit�s.

Software Performance Report (SPR)

Utiliser ce formulaire pour nous faire part des probl�mes de non conformit� �
la DPL rencontr�s avec DECdecision/FRAN�AIS V1.1.

 
		Sp�cificit�s de DECdecision/FRAN�AIS  V1.1

Nom des savesets

Le nom des savesets n'est pas DECISION011 mais DECISIONFR011. Le nom �
utiliser lors de l'installation sera donc DECISIONFR011.

Espace disque n�cessaire

Pour installer DECdecision/FRAN�AIS V1.1, vous avez besoin du nombre de blocs
suivant :

- 57 000 blocs pendant l'installation
- 42 500 blocs apr�s l'installation

Enregistrement des licences

Avant d'installer ou d'utiliser DECdecision/FRAN�AIS V1.1, vous devez
enregistrer et charger le PAK (Product Authorization Key) suivant :

- DECdecision-USER-UI-FRAN

Proc�dure d'installation

- Pour l'installation de DECdecision/FRAN�AIS V 1.1, l'utilisateur doit
r�pondre � une question suppl�mentaire :

     You can install the DECdecision kit with a French, a
     Belgian-French, a Canadian-French or a Swiss-French
     user-interface. By default the French variant will
     be installed.
  	   1   French variant
  	   2   Belgian-French variant
  	   3   Canadian-French variant
  	   4   Swiss-French variant

- Nous vous demandons d'ajouter la ligne de commande suivante au fichier 
SYSTARTUP_V5.COM :

          @SYS$STARTUP:DECW$STARTI18N 	FR_FR  (pour la France)	
          @SYS$STARTUP:DECW$STARTI18N 	FR_BE  (pour la Belgique)	    
          @SYS$STARTUP:DECW$STARTI18N 	FR_CA  (pour le Canada)	 
          @SYS$STARTUP:DECW$STARTI18N 	FR_CH  (pour la Suisse)

      Ce fichier doit �tre ex�cut� avant SYS$STARTUP:DECISION$STARTUP.

Nouveaux fichiers

	- BILAN_BILAN.TABLE
	- CONVERSION.TABLE
	- BILAN_ARDEMUZ.BLUEPRINT

Les deux premiers fichiers se trouvent dans le r�pertoire
DECISION$ECALC_LIBRARY:. Le troisi�me se trouve dans DECISION$BUILDER_LIBRARY:.
Ces fichiers sont utilis�s par l'exemple de d�monstration figurant dans le
manuel DECdecision Analyse et gestion int�gr�es. 

IVP

- Pour ex�cuter l' IVP apr�s l'installation, ex�cuter l'une des commandes
suivantes :

	@SYS$TEST:DECISION$IVP_FR_CA.COM (pour le Canada)
	@SYS$TEST:DECISION$IVP_FR_BE.COM (pour la Belgique) 
	@SYS$TEST:DECISION$IVP_FR_CH.COM (pour la Suisse)
	@SYS$TEST:DECISION$IVP_FR_FR.COM (pour la France)


- Dans le cas d'une installation en environnement multilingue, l'IVP �chouera
si vous ne d�finissez pas votre langue (Fran�ais, Fran�ais canadien ou Fran�ais
suisse) comme �tant la  langue de fonctionnement (Option Personnalisation
[Customize] du Gestionnaire de ses sion [Session Manager]).

DECchart

Lorsque vous installez DECdecision/Fran�ais alors que vous avez d�j� install�
une autre vari ante de DECdecision (francophone, am�ricaine ou autre), vous
devez r�pondre �Yes� � la question suivante pour afficher DECchart dans la
seconde langue install�e :

Do you want to reinstall DECchart ?[NO]

Affichage sous ULTRIX

Pour pouvoir afficher DECdecision V1.1/Fran�ais en environnement multilingue
sur une station de travail fonctionnant sous Ultrix, vous avez besoin de UWS
2.2. 


                Mises � jour du manuel �Analyse et gestion int�gr�es�

- Nous avons �t� amen�s � modifier le nom d'une des fonctions du module Access.
Dans la version 1.0 de DECdecision, on parlait de la fonction Intersection
(option du menu Donn�es). Dans la version 1.1, celle-ci s'appelle d�sormais la
fonction Fusion. Dans le manuel DECdecision Analyse et gestion int�gr�es,
veuillez remplacer le passage intitul� Intersection (page 3-32 et 3-33) par les
lignes suivantes : 

				******* 

Fusion La fonction Fusion permet � Access de cr�er une table de synth�se
r�sultant de la fusion de  deux tables dont certaines colonnes sont identiques. 
1 Ouvrez l'une des deux tables.  

2 S�lectionnez l'option Fusion du menu Donn�es :

Access affiche la bo�te de dialogue Fusion, qui contient le r�pertoire des
classeurs et des tables disponibles.S�lectionnez la  deuxi�me table en cliquant
deux fois rapidement sur le nom de celle-ci : Access modifie le contenu de la
bo�te de dialogue Fusion, qui pr�sente d�sormais le d�tail des colonnes 
disponibles dans chaque table et un tableau vide intitul� Colonnes communes.

3 Pour s�lectionner la ou les colonnes communes aux deux tables, cliquez sur
celle(s)-ci dans les deux listes intitul�es chacune Colonnes de la table.

Les paires de colonnes s�lectionn�es doivent �tre strictement identiques.

4 Cliquez sur OK : Access utilise les colonnes communes pour l'affichage de la
table r�sultante.

				*******

- De m�me, dans le chapitre 4, � la page 66, remplacer le paragraphe
concernant l'option D�faire par les lignes suivantes :

                                *******

Notez que, selon la fonction � annuler, Calc �actualise� l'intitul� D�faire en
le rempla�ant par D�faire Nom_fonction. Exemple : � l'aide de la fonction
Couper, vous supprimez une cellule, puis vous changez d'avis et d�cidez de la
r�afficher ; lorsque vous s�lectionnez la premi�re option du menu �dition
(anciennement D�faire), sont intitul� est devenu D�faire Couper.

                                ******* 

- Dans tout le manuel, l'extension par d�faut des rapports cr��s sous ACCESS et
sous CALC est .RAPPORT (et non plus .REPORT).


Listing d'installation de DECdecision/Francais for VMS V 1.1

	VAX/VMS Software Product Installation Procedure V5.3-1


It is 24-APR-1990 at 15:12.

Enter a question mark (?) at any time for help.

* Are you satisfied with the backup of your system disk [YES]? 

The following products will be processed:
  DECISIONFR V1.1

	Beginning installation of DECISIONFR V1.1 at 10:28

%VMSINSTAL-I-RESTORE, Restoring product save set A ...
%VMSINSTAL-I-RELMOVED , The product's release notes have been successfully 
moved to SYS$HELP.

   ************************************************************
   *                                                          *
   *                   DECdecision V1.1                       *
   *          Integrated Decision Support Package             *
   *                                                          *
   *                Installation Procedure                    *
   *                                                          *
   ************************************************************


     You can install the DECdecision kit with a French, a
     Belgian-French, a Canadian-french or a Swiss-French
     user-interface. By default the French variant will
     be installed.

     1   French          variant
     2   Belgian-French  variant
     3   Canadian-French variant
     4   Swiss-French    variant
* Enter the number of the variant to be installed [1]: 

        Product:      DECDECISION-USER-UI-FRAN
        Producer:     DEC
        Version:      1.1
        Release Date: 30-APR-1990

* Does this product have an authorization key registered and loaded? y

  Checking user parameters
  Checking system parameters
  Testing dependent product versions
%VMSINSTAL-I-RESTORE, Restoring product save set B ...
%VMSINSTAL-I-RESTORE, Restoring product save set C ...

    This kit requires DECchart V1.1 or later.

    *************************************************************

    DECchart V1.1 will be installed since it does 
    not exist on your system. 

    *************************************************************



The DECdecision product creates a tree of library directories.
A sub directory is created for each component.
* Where would you like to place the DECISION V1.1 library root
 [SYS$COMMON:[DECISION]]: 
%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[DECISION].
%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[DECISION.BUILDER].
%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[DECISION.ECALC].
%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[DECISION.ACCESS].
%VMSINSTAL-I-SYSDIR, This product creates system disk directory 
VMI$ROOT:[SYSTEST.DECISION$IVP].

    *************************************************************

    This installation requires the creation of the RDB$REMOTE
    account.  You MUST choose a UIC and password for this
    account.  PLEASE NOTE that your password for the RDB$REMOTE
    account and the network object RDBSERVER MUST be exactly the
    same.  The password you supply for the RDB$REMOTE account
    will be used for the network object as well IF the account is
    created by this installation.

    **************************************************************


    *************************************************************

    The installation procedure will not proceed until you enter a
    valid user identification code (UIC) for the RDB$REMOTE
    account.


    **************************************************************

* Enter UIC to be used for RDB$REMOTE account (e.g. [300,2]): 
* Enter UIC to be used for RDB$REMOTE account (e.g. [300,2]): [600,1]

    *************************************************************

    The entire installation will FAIL if you do not enter a valid
    password for the RDB$REMOTE account.  You will be given 
    3 chances to verify your password.  This installation
    procedure requires at least 6 characters for the
    RDB$REMOTE password.  Valid characters for a password are:

       A through Z
       a through z
       0 through 9
       $ (dollar sign)
       _ (underscore)

    As with the DCL SET PASSWORD command, your input will not
    appear on the terminal.  And to protect against typing errors
    that are not seen when entering the password, you must enter
    the password twice.

    *************************************************************


>
* Please enter PASSWORD to be used for RDB$REMOTE account: QSDFGHJKLM
>


>
* Please verify the PASSWORD entered for RDB$REMOTE: QSDFGHJKLM
>




* Do you want to run the IVP after the installation [YES]? 

    The DECdecision IVP requires that a display device be defined (using
    the SET DISPLAY command). Please enter the node name of the
    workstation on which to display the IVP. 

*      Enter node name (or Return to not run IVP): about

* Do you want to purge files replaced by this installation [YES]? 

    To complete the installation on a standalone VAX 11/780
    will take approximately:


        1 hour and 5 minutes to install
        16 minutes to run the IVP

    All required questions have been asked.  
    You can terminate the installation procedure at this time.

* Do you want to continue the installation [YES]? 
%VMSINSTAL-I-RESTORE, Restoring product save set D ...
%VMSINSTAL-I-RESTORE, Restoring product save set E ...
%VMSINSTAL-I-RESTORE, Restoring product save set F ...
%VMSINSTAL-I-RESTORE, Restoring product save set G ...
%VMSINSTAL-I-RESTORE, Restoring product save set H ...

  ***************************************************************************

   SYSTEM MANAGER


  The following command line MUST be added to the system startup
  command file SYS$MANAGER:SYSTARTUP_V5.COM for all nodes that will
  be running DECdecision

	@SYS$STARTUP:DECISION$STARTUP.COM 

  The following command line should be added to the system
  shutdown command file SYS$MANAGER:SYSHUTDWN.COM for all nodes
  that will be running DECdecision

	@SYS$MANAGER:DECISION$SHUTDOWN.COM

  ***************************************************************************

%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[RDB$REMOTE].

%DECISION-I-ACCOUNT, This installation creates an account named RDB$REMOTE.

RDBSERVER has been placed in the DECnet object database as number 35.

    *************************************************************

    SYSTEM MANAGER:

    If your DECnet object database is not configured to be in
    the cluster common directory, then you will need to perform
    the following:

    In order to have remote access on another node which shares
    this cluster common root directory, you must insert
    RDBSERVER.EXE into that node's DECnet object database by:


        a) Logging into that node, and
        b) Invoking SYS$COMMON:[SYSMGR]RDBSERVER_NCP.COM.

    This command procedure inserts RDBSERVER into the node's
    permanent DECnet object database.  This procedure only needs
    to be executed ONCE per node.

    This command procedure will prompt for a password for
    the object RDBSERVER. This password must match the 
    password established for the account.

    *************************************************************

 
    *************************************************************
 
    The RDBSERVER object automatically uses the RDB$REMOTE
    account created by this installation procedure.
 
    Invoking a database should look like: 
 
    $DEFINE MYDB "<node>::<dev>:<dir><db-name>"
    $RDO
    RDO> DATABASE FILE MYDB
 
    *************************************************************
 
%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[SYSHLP.EXAMPLES.RDBVMS].
%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[SYSTEST.RDBVMS].
 
    *************************************************************
 
    The Rdb/VMS Installation Verification Procedure (IVP) has
    been provided in SYS$COMMON:[SYSTEST].
 
    It is invoked using the commands:
 
        $ SET DEFAULT SYS$COMMON:[SYSTEST]
        $ @RDBIVP RTO
 
    *************************************************************
%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[SYSMGR.VAXINFO$PRODUCTS].

%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[SYSTEST.CHART$IVP].
%VMSINSTAL-I-SYSDIR, This product creates system disk directory  
VMI$ROOT:[SYSHLP.EXAMPLES.DECCHART].

Deleting obsolete files
%VMSINSTAL-I-MOVEFILES, Files will now be moved to their target 
directories...
Rdb/VMS monitor (RDMS_MONITOR) started
SQL: Assigning System-wide SQL Logicals

    *********************************************************************
    *                                                                   *
    *   Installing Sample Database...                                   *
    *                                                                   *
    *********************************************************************
    *                                                                   *
    *   Installing Default User Database...                             *
    *                                                                   *
    *********************************************************************
    *                                                                   *
    *   Installing Public Folder Database...                            *
    *                                                                   *
    *********************************************************************
Executing IVP for: DECdecision V1.1

    The IVP runs a Builder BLUEPRINT which will run each component and
    produce intermediate reports and a final chart of the results.    
    The reports and chart are compared with the expected values to determine
    if the IVP has completed successfully.

    Starting DECdecision...

Comparing the results of the BLUEPRINT with the expected results

        ********************************

        DECdecision V1.1

        IVP COMPLETED SUCCESSFULLY

        ********************************

IVP completed for: DECdecision V1.1

Executing IVP for: VAX Rdb/VMS V3.1A-0


Restoring the test database.

Running RDO tests
    Test completed successfully

Running the after-image journaling test.
    Test completed successfully


Running the interpreter test.
    Test completed successfully

Running the SQL/Services tests.
	You must have proxy enabled to run the sql services ivp program.
	See the Rdb/VMS installation guide for more information.
	All of the sql services files are in their proper directories.


 SQL/Services tests completed successfully.


    **************************************

    VAX Rdb/VMS V3.1A

    Run-Time Only

    IVP COMPLETED SUCCESSFULLY

    **************************************
    

IVP completed for: VAX Rdb/VMS V3.1A-0

	Installation of DECISIONFR V1.1 completed at 10:50

	VMSINSTAL procedure done at 10:50


                    Probl�mes rencontr�s avec la version 1.1



- Lorsque vous lancez DECdecision V1.1/Fran�ais � partir d'un FileView anglais
et que vous  n'avez pas install� la version anglaise de DECdecision, vous devez
copier les fichiers DECISION$VUE_PROFILE.VUE$DAT et DECISION$VUE.COM dans
VUE$LIBRARY. De m�me, lorsque vous lancez DECchart � partir d'un FileView
anglais, vous devez copier les fichiers CHART$VUE_PROFILE.VUE$DAT et
CHART$VUE.COM dans VUE$LIBRARY.

Module ACCESS

- Les symboles mon�taires s'affichent correctement dans les cellules . En
revanche, ils ne sont pas reproduits � l'impression.

- Le symbole Pourcentage, �%�, s'affiche correctement. � l'impression, l'espace
entre les  symbole �% � et le chiffre est supprim�.

- La date d'impression des rapports s'imprime selon le format am�ricain, avec
le nom du mois en fran�ais.

- Au niveau de l'exemple d'application pratique pr�sent� dans l'introduction du
manuel  DECdecision Analyse et gestion int�gr�es, les bases de donn�es ACCESS
n'autorisent pas une utilisation de DECdecision en environnement multilingue ;
c'est-�-dire que si, par exemple, vous installez la version am�ricaine de
DECdecision, et que vous installez par la suite DECdecision/Fran�ais, vous
devrez r�pondre �Yes� � la question suivante :

       Your system already has a default user database.
       * Do you want to replace it? [NO]

DECISION$ACCESS_DEFDB et DECISION$ACCESS_SAMPLE seront install�es. Mais 
puisque DECISION$ACCESS_SYSDB n'est pas r�install�e, le classeur BILAN ne sera
pas  pas accessible et l'utilisateur recevra le message suivant :

                 CLASSEUR NON DISPONIBLE
        Ce classeur n'est pas disponible pour le moment
                ou quelqu'un l'a supprim�.


- L'option Rendre public convertit le format �pourcentage� en format �d�cimal�.
Ouvrir la table et reformater la colonne en question au format �pourcentage�.


- Dans les tables du classeur BILAN (exemple d'application pratique), pour ne
pas afficher des ast�risques dans les cellules (**************), vous devez
modifier le format des tables de la fa�on suivante :

       -ZIZZZIZZZIZZZIZZZIZZ9O99" FB" pour la table BILAN_BELGIQUE 
       -ZIZZZIZZZIZZZIZZZIZZ9O99" �" pour la table BILAN_GB
       -ZIZZZIZZZIZZZIZZZIZZ9O99" F" pour la table BILAN_FRANCE 
       -ZIZZZIZZZIZZZIZZZIZZ9O99" $" pour la table BILAN_CANADA

Module CALC

- L'option  Remplir ne fonctionne pas avec les bases calendaire et horaire.

- La date d'impression des rapports s'imprime selon le format am�ricain, avec
le nom du mois en fran�ais.

- Les fonctions Date utilisent la syntaxe am�ricaine en entr�e. Ainsi, il
convient de taper :

   DATE("24-JAN-1989") pour obtenir 24-01-1989 ou 1989-01-24(1) 

   DATE("24-FEB-1989") pour obtenir 24-02-1989 ou 1989-02-24(1)

   DATE("24-MAR-1989") pour obtenir 24-03-1989 ou 1989-03-24(1)

   DATE("24-APR-1989") pour obtenir 24-04-1989 ou 1989-04-24(1)

   DATE("24-MAY-1989") pour obtenir 24-05-1989 ou 1989-05-24(1)

   DATE("24-JUN-1989") pour obtenir 24-06-1989 ou 1989-06-24(1)

   DATE("24-JUL-1989") pour obtenir 24-07-1989 ou 1989-07-24(1)

   DATE("24-AUG-1989") pour obtenir 24-08-1989 ou 1989-08-24(1)

   DATE("24-SEP-1989") pour obtenir 24-09-1989 ou 1989-09-24(1)

   DATE("24-OCT-1989") pour obtenir 24-10-1989 ou 1989-10-24(1)

   DATE("24-NOV-1989") pour obtenir 24-11-1989 ou 1989-11-24(1)

   DATE("24-DEC-1989") pour obtenir 24-12-1989 ou 1989-12-24(1)
--------------------------------------------------------------------------------
(1) Le deuxi�me format correspond � la variante canadienne. 



Module DECchart

Lorsque l'on demande une d�finition manuelle de l'�chelle (bo�te de dialogue
Axes), les valeurs minimale et maximale s'affichent selon le format am�ricain.   

Module Builder

Pour ex�cuter les exemples am�ricains dans DECdecision/Fran�ais, utiliser : 
DECISION$BUDGET_SUMMARY_FR.BLUEPRINT et 
DECISION$TUTORIAL_FR.BLUEPRINT
(au lieu de DECISION$BUDGET_SUMMARY.BLUEPRINT et 
DECISION$TUTORIAL.BLUEPRINT).


                                  NOTE FINALE

        Notre objectif principal �tant de satisfaire les besoins de nos
clients, n'h�sitez pas � nous faire part de vos commentaires. Vous pouvez nous
les faire parvenir aux adresses suivantes :

FRANCE :	
		Digital Equipment France
		SDES
		9/13 avenue du lac
		Bo�te postale 235
		91007 �vry Cedex

BELGIQUE :
		Digital Equipment  N.V./ S.A.
		Rue de l'A�ronef 1
		1140 Bruxelles

CANADA :
		Digital Equipment du Canada Limit�e
		Groupe d'ing�nierie local
		6505, autoroute Transcanadienne
		Saint-Laurent  (Qu�bec)
		H4T 2A8

SUISSE :
		Digital Equipment Corporation AG
		�berlandstrasse 1, Postfach
		CH-8600 D�bendorf 1

                         
