 






          DIGITAL

          Read Before Installing or Using VAX FORTRAN HPO Version 1.0A

          AV-PC39B-TE

          Please take time to read the following information about the VAX
          FORTRAN High-Performance Option (HPO) product.

          Installation Information

          This installation kit contains media and documentation for
          installing VAX FORTRAN HPO Version 1.0A on the VMS operating
          system.

          Prerequisite Software

          The following products are required for VAX FORTRAN HPO Version
          1.0A:

          o  VAX Language-Sensitive Editor (LSE) Version 3.0

          o  VAX FORTRAN Version 5.4

          o  VMS Version 5.4

          Digital also highly recommends using the VAX Performance and
          Coverage Analyzer (PCA) to optimize program performance.

          VMS Version 5.4 New Installation Problem

          If you have newly installed VMS Version 5.4 on your system and
          did not perform an upgrade from a previous version of VMS, you
          should execute the following command either before or after
          installing VAX FORTRAN HPO (the command requires the SYSPRV
          privilege):

               $ LIBRARY/SHARE/REPLACE SYS$LIBRARY:IMAGELIB SYS$LIBRARY:FORRTL,FORRTL2

          This command corrects a problem on newly installed VMS Ver-
          sion 5.4 systems that causes the linker to issue informational
          messages about a "link date-time mismatch" between the FORTRAN
          Run-Time Library shareable images and the system shareable image

 






          library. (The messages do not affect linking or running pro-
          grams, and can be ignored.) The command needs to be executed
          only once, and only on newly installed Version 5.4 systems. The
          problem will be corrected in future versions of VMS.

          Release Notes Information

          The release notes for VAX FORTRAN HPO Version 1.0A are included
          in the Installation Guide and are also available on line. They
          contain important installation-related instructions and sum-
          maries of new features, known problems, performance enhance-
          ments, restrictions, and incompatibilities with Version 5.n of
          VAX FORTRAN.

          You can read the online version of the release notes before in-
          stalling VAX FORTRAN HPO by invoking VMSINSTAL and following the
          instructions in the installation guide. To locate these release
          notes after installing VAX FORTRAN HPO, type the following DCL
          command:

               $ HELP FORTRAN RELEASE_NOTES



          ©Digital Equipment Corporation. 1990. All rights reserved.














                                          2

 






          Contents of This Kit

          o  Indented Bill Report (BIL) and Bill of Materials (BOM)

             Please read the BIL and BOM enclosed in this kit and check
             that all items listed are actually in your kit. If your
             kit is damaged or any items are missing, call your Digital
             representative.

          o  Media

             If you ordered media, you will find the media and the VAX
             FORTRAN HPO Installation Guide/Release Notes (AA-PC38A-TE) in
             this kit. For information about installing VAX FORTRAN HPO
             on your system, see the VAX FORTRAN HPO Installation Guide
             /Release Notes.

          o  Documentation

             Depending on your order, this kit may include the VAX FORTRAN
             Performance Guide.

             User documentation is always included for new customers.
             However, for continuing customers, user documentation is
             included only if the customer orders it separately or if the
             documetantion has been revised since the prior release.

             Note that the VAX FORTRAN HPO Installation Guide/Release
             Notes is included with the media.

          o  Software Product Description (SPD)

             The SPD provides an overview of the VAX FORTRAN HPO kit and
             its features.

          o  Software Performance Report (SPR)

             Use the SPR form to report any problems with VAX FORTRAN HPO,
             provided you have purchased warranty services.

                                          3

 






          o  SYS Support Addendum (SSA)

             The SSA describes the technical environment in which the
             product is supported.

          Restrictions

          Please note a restriction concerning the use of the /ANALYSIS_
          DATA and /DESIGN qualifiers with VAX FORTRAN HPO Version 1.0A.
          To load the analysis data files created by the /ANALYSIS_DATA
          qualifier into a VAX Source Code Analyzer (SCA) library, you
          must use SCA Version 2.0. (SCA Version 2.0 is available individ-
          ually or in VAXset Version 9.0 or later.)

          A Final Note

          Digital prides itself on responding to customer needs. To con-
          tinue serving you, we need your comments. Each manual contains
          pre-addressed, postage-paid Reader's Comments forms at the back.
          If you find errors in a manual or want to make comments about
          it, please fill out one of these forms and send it to us.


















                                          4
