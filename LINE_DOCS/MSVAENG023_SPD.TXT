MUXserver 100 Remote Terminal Server for VMS, V2.3		SPD 28.45.02

DESCRIPTION

The MUXserver 100 Remote Terminal Server is a statistical multiplexer 
server for connecting remote terminals to Ethernet Local Area Networks. The 
MUXserver 100 Remote Terminal Server provides a convenient method to 
logically connect up to sixteen remotely located Digital asynchronous 
terminals by means of  wide area telecommunications services to one or more 
service nodes (hosts) on an Ethernet.  

The MUXserver 100 Remote Terminal Server allows for VMS host initiated 
connections to asynchronous printers.  A special print symbiont on VMS 
service nodes can initiate connections to asynchronous printers connected 
to DECmux IIs located at remote sites.  This allows certain asynchronous 
printers to be distributed throughout the LAN/WAN network and accessed 
transparently by service node users.  Incoming host-initiated connect 
requests are queued in a First-in-First-Out (FIFO) priority scheme. 

The MUXserver 100 Remote Terminal Server implements the Local Area 
Transport (LAT) protocol for communication with service nodes that 
implement this protocol on the same Ethernet.  Remote terminal data streams 
are concentrated at a DECmux II statistical multiplexer device and 
connected back to the LAN-site MUXserver 100 on a single leased phone line 
using an HDLC based, synchronous data transmission, statistical multiplexer 
protocol. Response time at the remote terminal is a function of the 
Ethernet loading, the LAT  protocol performance, the synchronous link speed 
and the I/O loading requirements of the remote terminals.

Software that runs on the MUXserver 100 Remote Terminal Server is downline 
loaded over the Ethernet network from a Phase IV DECnet load host.

Terminal access using the MUXserver 100 Remote Terminal Server does not 
require DECnet running in the same service node.  LAT uses the Ethernet 
addressing mechanism to transport terminal messages.  

Features 

oo   Login load balancing

oo   Multiple terminal sessions
           
oo   Automatic login failover

oo   Remote printer support 

Terminal Connection Management

Through the use of a simple terminal command, users can:  

oo   Establish a logical connection, called a session, to any local service 
    node that implements the LAT protocol on the same Ethernet. 

oo   Connect to the same or a different service node on the Ethernet.   

oo   Several servers can be used to connect many terminals to one or more 
    service nodes.

A service node can have one or more services that are offered to MUXserver 
100 Remote Terminal Server users.  Services and nodes are identified by 
name.  Users always connect to services, not to nodes, although often one 
of the service names will be the node name.  

In a VAXcluster environment, each VAXcluster on the Ethernet is seen as a 
collection of service nodes offering a common service.

Load Balancing

When a connection is made to a service, the actual node for the connection 
is determined by load balancing.  Load balancing is a process the server 
uses when more than one node offers the same service. Service nodes do not 
have to be configured as a VAXcluster in order for load balancing to be 
used.  Service nodes with the same names may be running different operating 
systems.  Using the load balancing process, the server connects to the node 
with the highest rating for the service desired. This rating is based on 
the current loading, memory available, CPU class, and the number of users 
on the nodes that offer the service.

        
Multiple Sessions

oo   Allows each user to establish and maintain up to six sessions to one or 
    more service nodes.

oo   Up to a maximum of 28 sessions per MUXserver 100 depending on the 
    number of nodes in the server database.

oo   Only one session can be active at a time.

oo   Through simple switching commands, the user can access the different 
    sessions without repeating a login dialogue each time.  Some operating 
    systems may impose limits on the number of LAT sessions such a host 
    will support.

Local Mode and Service Mode

For the most part, the environment provided by the MUXserver 100 Remote 
Terminal Server is identical to the environment the user would experience 
if attached through stand-alone multiplexers to the service node.  When 
operating in this mode, the user is in Service Mode.  Occasionally, during 
connection establishment, the user interacts directly with the MUXserver 
100.  When operating in this mode, the user is in Local Mode.

Local Mode - The terminal input is interpreted directly by the MUXserver 
100 as commands to be performed by the server. 

oo   Two different levels of operation:  nonprivileged and privileged.

oo   Nonprivileged commands allow the terminal users to control their 
    service sessions, set terminal characteristics, and show server 
    information.
          
oo   Privileged mode is provided for the Server Manager to control the 
    environment of the server and of the terminal users. Access to this 
    mode is password protected.

oo   The Terminal Server Manager environment is a logical extension of the 
    user environment.  

oo   The Server Manager is treated as a server user with a privileged 
    status. 

oo   The Server Manager sets a terminal to this status using a command which 
    requires a password. This privileged status allows the Server Manager 
    to enter commands not usually available to server users. These commands 
    set server characteristics,  provide control over server port usage, 
    and provide the ability to  control  the user's access to the server 
    and network services.

    The MUXserver 100 also has a dedicated supervisory port that allows the 
    Server Manager to control the environment of the server and of the 
    terminal users.  Access to this mode is password protected.

Service Mode - The terminal input is passed directly to the connected 
service node with several exceptions.  One exception, the Local Switch 
Character, allows the user to enter Local Mode from Service Mode.

Note:    The BREAK Key can also be used for this function.  

Another exception, the Forward and Backward Switch Characters, allows the 
user to switch between sessions without entering Local Mode.  The Switch 
Characters are disabled by default but can be enabled by command. Both 
CTRL/S and CTRL/Q are usually interpreted locally, however flow control 
using these characters can be disabled.

Autoconnection

oo   Automatically connects a user terminal to a service node when 
    connection failures occur or upon user login to the server.  

oo   A dedicated or preferred service can be specified for each terminal 
    user.

Dedicated Service 

oo   The MUXserver 100 Remote Terminal Server will attempt to connect to 
    that service when a character is typed on the terminal keyboard or when 
    an existing connection fails.

oo   Only one session is available.   

oo   No Local Mode commands or messages are available to the terminal user. 

oo   Ports with dedicated service can be automatically logged out of the 
    server when the user logs out of the service node.

Preferred Service 

oo   The MUXserver 100 Remote Terminal Server will attempt to connect to 
    that service the same as with the dedicated service mode of operation.
  
oo   The terminal user can also enter Local Mode and establish other 
    sessions.

Automatic Session Failover

If a service is available on two or more service nodes and a connection to 
a service fails, the server will attempt to connect the user to another 
service node offering the same service.  Autoconnection must be enabled for 
this feature to work.  The user does not have to be already connected to 
that service node.  Furthermore, the user's context at the time of failure 
is not automatically restored and login to the new service is required.

Groups

Every terminal and service node in a LAT network is a member of one or more 
groups. These groups are identified by numbers called group codes. Group 
codes are an easy means of subdividing the network into what appears to be 
many smaller networks.  A terminal user is only aware of the services that 
are offered by nodes in the same group(s).

oo   A privileged user can change the group(s) in which a terminal is a 
    member.
   
oo   Group codes provide a restrictive view of the network.


This restricted view is mainly for user convenience and, although it also 
provides a form of security, it is not intended to be the primary form of 
access authorization or system security for a node.

Security

MUXserver 100 Remote Terminal Server security includes:

oo   The ability to lock a terminal's keyboard from another user's keyboard.  
    
oo   Optional login protection and nonprivileged Local Mode of operation as 
    a default.

Terminal Locking

A user can lock the terminal using a lock  password.  A lock password 
allows the user to leave sessions running on the terminal without fear of 
security violations.  When a terminal is locked, all input from the 
terminal is ignored until the lock password is re-entered.

Password

Login passwords can be enabled on a per-line basis by the privileged user.  
If enabled, the terminal user must enter a login password to access server 
functions.  If a dedicated service is specified, login protection is not 
available.

Terminal users usually have access to the non-privileged Local Mode.  In 
Local Mode, users can issue commands that affect their own terminal 
environment.  The server itself has a privileged mode for Server Manager's 
use. The mode is password protected.

On-line HELP Facility

A limited on-line HELP facility is available to remind users of command 
syntax. The HELP facility is not intended as a replacement for the 
documentation set.

Directory Service

Any MUXserver 100 Remote Terminal Server user can obtain a directory of 
services available with a SHOW SERVICES command.  Unauthorized Services 
will not be displayed.

Port Parameter Configuration

Parameters governing the operation of an individual terminal can be 
modified and displayed by a nonprivileged terminal user interactively from 
their terminal.  In the privileged mode, a Server Manager can modify 
parameters for other users' ports or for the server as a whole, and can 
also specify the composite, synchronous link parameters.

Permanent characteristics are maintained in the MUXserver 100 Remote 
Terminal Server memory (even maintained if the  power is disconnected).  
Permanent characteristics are maintained for server-wide parameters as well 
as per-terminal parameters.  Permanent characteristics can be reset to 
factory defaults by pressing the software reset button on the MUXserver 100 
hardware unit while  plugging in the power cord.  Terminal parameters that 
can be set and displayed include:  speed, character size, group codes, 
parity, and terminal type.

Port Access Characteristics

A port on a MUXserver 100 Remote Terminal Server, Version 2.3 can be set up 
in different ways  depending on the device attached to the port and its 
intended use. The MUXserver 100 Remote Terminal Server supports RS-232-C 
asynchronous devices operating at speeds up to 9.6K bps (19.2K bps for 
interactive terminals) and supports XON/XOFF flow control.

Port access is the characteristic which determines how a port can access or 
be accessed by interactive users and service nodes.

 1.   Access Local - Designed for interactive terminals.  This allows the 
      device (typically an interactive terminal) attached to the port to 
      CONNECT to LAT services.

 2.   Access Remote - Designed for application driven devices such as 
      asynchronous printers which are allocated by a service node process 
      by request.  This allows the implementation of certain shared, remote 
      printers by multiple service nodes.

 3.   Access Dynamic - Designed for devices (such as personal computers or 
      printers with keyboards) which require both Local and Remote access.

 4.   Access None - Designed to allow the Server Manager to disable the use 
      of a port.

Printers

With printer support capabilities, the setup procedure of remote printers 
needs to be performed once and is automatically reconfigured on system 
startup.  The particular server port must be configured for remote access 
and set up to match the characteristics of the printer.

The system startup command file must be modified to call the two command 
files provided with the service node software.  Finally, the command files 
themselves must be customized to reflect the environment of their node.  
The server can optionally queue remote connects if these connects cannot be 
satisfied immediately.  This queue management can be enabled for the server 
by the server manager.  Note that this is a connection queue only.

Network Configuration

The MUXserver 100 Remote Terminal Server is connected to an Ethernet using 
a DELNI, DECOM, DESTA, or H4000 Transceiver and a transceiver cable.  Two 
synchronous, composite data link connections are supported, each of which 
can connect to a remote, eight line DECmux II concentrator.  Two DECmux II 
concentrators can also be connected together using the route-through mode 
of operation.  In this case, only a single MUXserver 100 composite link 
connection can be utilized. 


Four LAN/WAN configurations are supported with the default, factory-set, 
MUXserver 100 parameters.  Default MUXserver Network configurations are:

oo   Eight users (one DECmux) at ONE Remote Site

oo   Sixteen users (two DECmuxes) at ONE Remote Site

oo   Sixteen users, with eight users (one DECmux) at each of TWO Remote 
    Sites. Two composite links are run from the MUXserver.

oo   Sixteen users, with eight users (one DECmux) at each of TWO Remote 
    Sites. Only ONE composite link is run from the MUXserver to the closest 
    Remote Site. The link is then ``Routed-through'' on a second composite 
    link from the first DECmux to the DECmux at the farther Remote Site.

The composite link is HDLC based with remote data link transfer rates of 
2400, 4800, 9600, or 19.2K bps for RS-232 connections, with up to 38.4K for 
RS-422.  The rate can be selected by the server manager in privileged mode.

Both RS-232-C/CCITT V.24 modem connections or RS-422-A long line (up to one 
kilometer) are supported  on the two MUXserver 100 composite link 
interfaces.  The same connectors are used for both RS-232 and RS-422 Long 
Line Drive.  The MUXserver 100 has a default setting for RS-232 and 
requires internal DIP switches and program settings to change to RS-422.  
Any change in switch setting requires access to the inside of the MUXserver 
100 and, as such, should be performed by Digital Field Service.

Terminal Operation

The MUXserver 100 Remote Terminal Server supports the simultaneous 
operation up to sixteen terminals connected over two DECmux II modules at 
speeds from 75 bps to 19.2K bps.  The DECmux II modules also support:

oo   Split speed (transmit and receive) terminal operation

oo   Block Mode transfers

oo   Automatic line speed detection

oo   Digital Personal Computer file transfer

oo   XON/XOFF handling

oo   Data Transparency mode

oo   Ability to pass BREAK character and/or error notification

Server Management

Several facilities exist for managing and trouble-shooting MUXserver 
operation.  A Server Manager connected in privileged mode by means of the 
supervisory port can establish the composite link parameters, set up server 
identification information, change port parameters, or fine tune the 
operating characteristics of the server.  Trouble-shooting facilities 
include diagnostic tests and on-line statistics.

A privileged user can diagnose Ethernet communications problems by looping 
messages to an Ethernet host and through the Ethernet hardware interface at 
the server.  Connections to remote DECmux II modules can also be made to 
test the composite link integrity.  To diagnose terminal problems, users 
can execute a command to transmit test data to their terminal, or the 
Server Manager can send test data to any terminal.

The server maintains the following variety of statistics and counters: 
Ethernet data link statistics, LAT protocol statistics, and composite link 
statistics.   The Ethernet data can be displayed and zeroed by a privileged 
terminal user.  Server parameters that can be modified and displayed 
include the server identification, circuit timer, session limits, and login 
limits.
                                               
Remote Server Management

The MUXserver 100 Remote Terminal Server implements the console carrier 
feature which enables access to the MUXserver 100 Local Mode from a Phase 
IV DECnet host on the same LAN.  A restricted set of Local Mode functions 
are available to a console carrier user.  This capability provides 
centralized server management and remote server diagnosis.

Communications Supported on the MUXserver 100

MUXserver 100 Remote Terminal Server software is designed to run on 
MUXserver hardware exclusively.  This hardware includes an Ethernet 
interface for connection to an Ethernet transceiver cable, two composite 
link ports, and a single supervisory terminal port.

Ethernet-connection alternatives:

oo   H4000	    Baseband Ethernet Transceiver                          

oo   DESTA	    ThinWire Ethernet Station Adapter                      

oo   DECOM	    Broadband Ethernet Transceiver                         

oo   DELNI	    Ethernet Local Area Interconnect                       

Composite Link-connection alternatives include the following with 1200, 
2400, 4800, 9600, 19200  and 38400 being the only available transfer rates 
on the interfaces:

oo   Leased Line Modem	       Non-dialup synchronous, full-duplex 
                                       via BC22F RS232 cable (non-satellite 
                                       links). Bell (TM) compatibility: 202 
                                       FDX, 201B, 208B, 209. CCITT 
                                       compatibility: V.23, V.26, V.27bis, 
                                       V.29. (up to the highest speed 
                                       supported by the modem used).

oo   Direct RS-422-A Cable		Customer-supplied RS-422-A cable up 
                                       to 1 kilometer or 3280 feet in 
                                       length.

oo   Digital Transmission		Full duplex, Synchronous RS232 
                                       interface (non-switched).

Supervisory Terminal port can accept directly connected asynchronous 
terminals with the following Comm. characteristics:

 1.    Asynchronous RS-232-C start/stop transmission, having eight data 
       bits, 1 stop bit, no parity.

 2.    Full duplex with XON/XOFF flow control.

 3.    Speeds selected from one of the following: 300, 600, 1200, 2400, 
       4800, 9600 baud, or AUTOBAUD.

 4.    Physical connections includes data lines only on a DB25-P connector.

At least one dedicated, hardcopy, Supervisory Terminal is recommended per 
MUXserver 100 site for use in diagnostic trouble-shooting, 
re-configuration, or performance monitoring.

Devices Supported on the DECmux II

The DECmux II has four types of interfaces to which external equipment can 
be connected:

 1.    Two RS-232-C Modem Composite Link Ports

 2.    Two RS-422-A Composite Link Ports

 3.    A single Supervisory Terminal Port

 4.    Eight Asynchronous RS-232-C Device Ports

The RS-232-C Composite Link-connection alternatives include the following 
with 1200, 2400, 4800, 9600, and 19200 bps being the only available 
transfer rates on the interfaces:

oo   Leased Line Modem	  Non-dialup synchronous, full-duplex via 
                                  BC22F RS232 cable (non-satellite links). 
                                  Bell compatibility: 202 FDX, 201B, 208B, 
                                  209. CCITT compatibility: V.23, V.26, 
                                  V.27bis, V.29. (up to the highest speed 
                                  supported by the modem used).

oo   Digital Transmission	   Full duplex, Synchronous RS232 interface 
                                  (non-switched).

The only RS-422-A Composite Link-connection alternative is direct wire 
connection (customer-supplied cable) with 1200, 2400, 4800, 9600, 19200 and 
38400 bps being the only available transfer rates.

Supervisory Terminal port can accept directly connected asynchronous 
terminals with the following communication characteristics:

oo   Asynchronous RS-232-C start/stop, having 8 data bits, 1 stop bit, no 
    parity.

oo   Full duplex with XON/XOFF flow control.

oo   Speeds selected from one of the following: 300, 600, 1200, 2400, 4800, 
    9600 baud, or AUTOBAUD.

oo   Physical connections include data lines only on a DB25-P connector.

MUXserver 100 Remote Terminal Server Operation

The MUXserver 100 ROM-based firmware provides the necessary maintenance 
operation protocols for downline loading MUXserver 100 software from a 
Phase IV DECnet load host over the Ethernet into MUXserver memory.   All 
self-test diagnostics are in MUXserver ROM, therefore, downline loading is 
not a precondition for MUXserver self-test.  In the event of a bug check 
caused by a fatal error, the unit will usually attempt to upline dump 
server memory to a DECnet Phase IV host.  Following this, the unit will 
automatically initialize itself and invoke a downline load.

Restrictions on MUXserver 100 Remote Terminal Server Usage

While terminal connections using the MUXserver 100 Remote Terminal Server  
have been designed to simulate direct terminal connections as much as 
possible, a few differences exist because of the nature of the product.

Under most circumstances, these differences are not noticed by terminal 
users or service node application programs.  However, applications which 
are directly dependent on the following functions may not operate in the 
same manner as a direct connection:

 1.    Applications that depend on reading or setting the terminal speed, 
       character size and parity by manipulating system data structures.

 2.    Applications that depend on an extremely fast response time 
       (typically less than 200 ms) to operate.

 3.    Applications that utilize an alternate terminal driver in the 
       service node.

 4.    Applications that expect incoming connections to have fixed device 
       names.

 5.    Applications that directly receive and send BREAK signals and/or 
       XON/XOFF flow control characters (since these characters are 
       usually intercepted and processed locally by the MUXserver 100 and 
       DECmux II).

 6.    Applications that require dial-up modems connected to the DECmux II. 
       (Unauthorized connection could occur if a call is disconnected and 
       the terminal session is NOT logged out first).

 7.    Applications that utilize asynchronous DECnet software or protocols 
       are not supported on the MUXserver 100.

HARDWARE REQUIREMENTS

VAX, MicroVAX or VAXstation configuration as specified in the System 
Support Addendum (SSA 28.45.02-x).


SOFTWARE REQUIREMENTS

VMS Operating System 

DECnet-VAX

Host initiated connect capability and shared printer support are only 
available on VMS service nodes running LATplus/VMS.  

A DECnet Load Host is required. 

Refer to the System Support Addendum  (SSA 28.45.02-x) for availability and 
required versions of prerequisite software.

ORDERING INFORMATION
 
Software Licenses: QL-VE4A*-**
Software Media: QA-VE4A*-**
Software Documentation: QA-VE4AA-GZ
Software Product Services: QT-VE4A*-**

*   Denotes variant fields.  For additional information on licenses, 
    services and media, refer to the appropriate price book.

SOFTWARE LICENSING     

The MUXserver 100 Remote Terminal Server software license applies to the 
MUXserver 100 hardware on which the MUXserver software runs, NOT to the 
host node CPUs which downline load the software image, or service node CPUs 
in the network.

The user will need a separate license for each MUXserver 100 CPU on which 
the user is using the software product (except as otherwise specified by 
Digital).  Materials and Service Options are selected to utilize the 
product effectively.  

This product does not provide support for the VMS License Management 
Facility. A Product Authorization Key (PAK) is not required for 
installation or use of this version of the product.

This software is furnished under the licensing provisions of Digital 
Equipment Corporation's Standard Terms and Conditions.  For more 
information about Digital's licensing terms and policies, contact your 
local Digital office.

SOFTWARE PRODUCT SERVICES 

A variety of service options are available from Digital.  For more 
information, contact your local Digital office.

SOFTWARE WARRANTY

Warranty for this software product is provided by Digital with the purchase 
of a license for the product as defined in the Software Warranty Addendum 
of this SPD.

(TM) Bell is a trademark of Bell Telephone Companies.

(R)  The DIGITAL Logo is a registered trademark of Digital Equipment 
     Corporation.
(TM) DEC, DECnet, DECmux, Ethernet, LA50, LA100, LA120, LA210, LN01S, LQP02, 
     LG01, LG02, LCG01, LJ250, MicroVAX, VAX, VAXstation, VAXserver, and VT 
     are trademarks of Digital Equipment Corporation.

