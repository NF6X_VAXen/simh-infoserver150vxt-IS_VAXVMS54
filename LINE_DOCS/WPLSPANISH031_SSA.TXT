 
Nombre del producto: WPS-PLUS(tm)/VMS/Espa�ol, versi�n 3.1 	SSA 27.90.02-A
 
Requisitos de soporte f�sico 
 
Procesadores:

VAX :

VAX 6000 Model 200 Series, VAX 6000 Model 300 Series, VAX 6000 Model 400 Series

VAX 8200, VAX 8250, VAX 8300, VAX 8350, VAX 8500, VAX 8530, VAX 8550, VAX 8600, 
VAX 8650, VAX 8700, VAX 8800, VAX 8810, VAX 8820, VAX 8830, VAX 8840, VAX 8842, 
VAX 8974, VAX 8978

VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

MicroVAX :

MicroVAX II, MicroVAX 2000, MicroVAX 3100, MicroVAX 3300, MicroVAX 3400, 
MicroVAX 3500, MicroVAX 3600, MicroVAX 3800, MicroVAX 3900

VAXstation :

VAXstation II, VAXstation 2000, VAXstation 3100 Series, VAXstation 3200, 
VAXstation 3500, VAXstation 3520, VAXstation 3540 
                                                      
VAXserver :

VAXserver 3100, VAXserver 3300, VAXserver 3400, VAXserver 3500, VAXserver 3600, 
VAXserver 3602, VAXserver 3800, VAXserver 3900, VAXserver 6000-210, 
VAXserver 6000-310, VAXserver 6000-410, VAXserver 6000-420


Sin servicio: MicroVAX I, VAXstation I, VAX-11/725, VAX-11/782,
VAXstation 8000

Limitaciones:

MicroVAX II y VAXstation II requieren o un RD53 � RD52 con una
configuraci�n de al menos otro disco duro. 
 
Este producto requiere tres megabytes de memoria por encima de la memoria 
m�nima que se precisa para ejecutar el sistema operativo VMS y otras
aplicaciones ejecut�ndose en el sistema. 

Se requiere un dispositivo capaz de leer una TK50 para instalar
este producto en un MicroVAX 2000 y VAXstation 2000.
 
Los terminales admitidos incluyen (texto solamente): 
 
* terminal de la serie VT100W con AVO^ (opci�n v�deo avanzado)
 
* VT100 (AVO)^
 
* Serie VT200, VT300 
 
* DECmate II o DECmate III, con DECmate/WPS (al menos versi�n 2.1) 
(en modo CX)^^
 
* Professional 300 en modo de emulaci�n de terminal 
 
* Rainbow 100 en modo de emulaci�n de terminal 
 
* Serie VAXstation 
 
^ WPS-PLUS admite solamente dichos terminales si tienen la opci�n 
de video avanzado. No admite el VT100 sin AVO, VT131, DECmate I o WS78. 
 
^^ consulte la documentaci�n del DECmate para instrucciones adecuadas.
 
Juego de caracteres t�cnicos: 
 
* serie VT200, VT300 
 
* VAXstation II con emulador de VT220 
 
* DECmate II o DECmate III, con DECmate/WPS (versi�n m�nima 2.1) (en modo CX) 
 
* LA100 con la TCS ROM adecuada instalada. 

NOTA: solamente se admite el modelo LA100-PC. 
 
* LA210 con TCS ROM instalado 
 
* LN03 y LN03-Plus 
 
* LN03R (ScriptPrinter) y LPS40 (PrintServer 40) con el int�rprete ANSI 
que se proporciona como requisito de soporte l�gico.
 
Espacio en bloques (tama�o del bloque en cluster = 1): 
 
espacio en disco requerido para instalaci�n sobre el disco del sistema: 
25.000 bloques (12.8 Mbytes) 

espacio en disco requerido para uso (permanente) despu�s de la
instalaci�n sobre disco del sistema: 
21.000 bloques (10.7 Mbytes) 
 
espacio en disco requerido sobre disco del sistema cuando la instalaci�n no
se realiza sobre el disco del sistema: 
25.000 bloques (12.8 Mbytes) 
 
espacio en disco requerido para uso sobre el sistema (permanente) cuando
la instalaci�n no se realiza sobre el disco del sistema: 
6.000 bloques (3.1 Mbytes) 
 
espacio en disco requerido para instalaci�n de diccionarios ingleses:
1.800 bloques (0.92M bytes) 
 
espacio en disco requerido para uso (permanente) de los diccionarios 
ingleses: 1.700 bloques (0.87M bytes) 
 
Nota: los tama�os son aproximados; los tama�os reales pueden variar 
dependiendo del entorno del usuario, la configuraci�n y las opciones
seleccionadas.

Equipo opcional: 
 
* LQP02 con alimentador opcional de hojas

* LQP03 con alimentador opcional de hojas
 
* LQP45 con alimentador opcional de hojas
 
* LA50 
 
* LA75 
 
* LA100 con alimentador opcional de hojas (1)
 
* LA210 
 
* LN03 
 
* LN03R (2)
 
* PrintServer40 (LPS40) (3)
 
* LP11 
 
* LJ250 
 
* QUME (4)


Nota 1: solamente el modelo LA100-PC se admite. 
Nota 2: Se admite la LN03R via el int�rprete de ANSI
Nota 3: El PrintServer 40 se admite v�a el int�rprete ANSI
Nota 4: La impresora QUME Sprint 11/40-130 WT PLUS Wide Track
        Letter Quality est� disponible v�a QUME y admite ancho de
	256 columnas.
 
NOTA FINAL SOBRE IMPRESORAS:

WPS-PLUS/VMS entiende que siempre habr� papel disponible en el mecanismo
de arrastre mediante el alimentador autom�tico de hojas o en el mecanismo
de arrastre.


Entorno "cluster"
 
Este producto asociado funciona enteramente cuando se instala sobre
cualquier configuraci�n VAXcluster^ v�lida y con licencia sin limitaciones. 
Los requisitos de soporte f�sico especial se incluyen en la descripci�n y 
en el anexo de este producto.
 
^ Las configuraciones VAXcluster V5.x se describen enteramente 
en la descripci�n del producto VAXcluster (29.78.xx) e incluyen
CI, Ethernet y configuraciones mixtas de Interconnect. 
 
Requisitos del soporte l�gico
 
Sistema operativo: 
 
Sistema operativo VMS V5.0-V5.2
 
Productos asociados:
 
VAX FMS Runtime Only System V2.3 - V2.4
VMS Workstation Software V4.1

Se precisa VAX ScriptPrinter V2.0 para imprimir ficheros con formato
distinto a PostScript en una impresora ScriptPrinter 
 
*NOTA: VAX FMS Runtime Only System es un requisito previo para ejecutar 
WPS-PLUS/VMS. Los clientes tienen que haber comprado la licencia VAX FMS 
Runtime Only System as� como el juego de soporte magn�tico del producto.
 
Personalizaci�n VMS 
 
Para sistemas VMS V5.x se precisa el sistema operativo VMS de las
siguientes clases:

^ VMS Required Saveset
^ Network Support
^ Programming Support
^ Secure User's Environment
^ Utilities

Para m�s informaci�n sobre clases de VMS consulte 
la descripci�n del producto VMS (SPD 25.01.xx). 
 
Soporte L�gico Adicional
 
DECpage, versi�n 3.0 
 

CONSIDERACIONES DE CRECIMIENTO 
 
Los requisitos m�nimos de soporte f�sico y l�gico para cualquier versi�n 
futura de este producto pueden ser diferentes a los requisitos m�nimos 
de soporte f�sico de la versi�n en curso.
 
MEDIOS MAGNETICOS DE DISTRIBUCION 
 
Cinta: 9-track 1600 BPI Magtape (PE), cinta TK50 Streaming 

Este producto est� disponible como parte del VMS Consolidated Software
Distribution sobre CDROM.
 
Informaci�n de pedidos:
 
Licencias del soporte l�gico: QL-AAMS*-** 
Soporte magn�tico: QA-AAMS*-** 
Documentaci�n: QA-AAMSA-GZ 
Servicios: QT-AAMS*-** 
 
* denota campos variables. Para informaci�n adicional sobre licencias
disponibles, servicios y soporte magn�tico, consulte el cat�logo
de precios correspondiente.
 
La informaci�n anterior es v�lida a la hora de lanzar este producto. 
P�ngase en contacto con la oficina local de DIGITAL para obtener
la informaci�n m�s actualizada. 
 
 
Enero 1990
BH-MW81B-TE
