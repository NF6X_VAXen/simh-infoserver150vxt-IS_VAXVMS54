

     Software
     Product
     Description

     ________________________________________________________________

     PRODUCT NAME:  DECbridge 500 Software Microcode, Version 2.2  SPD 32.28.03

     DESCRIPTION

     DECbridge 500 Software Microcode is dedicated microcode which
     runs in the DECbridge 500 FDDI to 802.3/Ethernet hardware bridge
     platform. The DECbridge 500 is an ISO model Level 2, protocol
     independent bridge that allows customers to connect an existing
     802.3/Ethernet LAN to a high speed FDDI network.

     DECbridge 500 hardware is shipped from the factory with the
     DECbridge 500 Software Microcode preloaded. The microcode re-
     sides in the bridge's electronically alterable memory, thus
     allowing subsequent versions of the microcode to be downline
     loaded. The Software Microcode is maintained in the hardware
     base even during power-off states.

     Local area network bridges are the building blocks of the ex-
     tended LAN. An extended LAN is a collection of LANs that are
     interconnected to logically appear as one large local area net-
     work. The bridge interconnects an 802.3/Ethernet LAN to an FDDI
     LAN allowing only data which is destined for an adjacent LAN to
     pass through the device. By performing this traffic control and
     direction, network bandwidth is maximized since only traffic
     which is required on the extended LAN is allowed to traverse the
     bridge. Additionally, the DECbridge 500 performs all the neces-
     sary data packet conversion to allow FDDI and 802.3/Ethernet to
     interoperate on the extended LAN.

     The general characteristics of DECbridge 500 are:

     o  Performs initialization and bootstrapping.

                                  DIGITAL                October 1990

                                                          AE-PBPYD-TE

 






     o  Performs Diagnostic Selftest automatically at power-up and
        also when initiated by DECelms network management software.

     o  Performs filter and forward decisions on incoming data pack-
        ets.

     o  Performs bi-directional translation between FDDI and
        802.3/Ethernet data formats, providing direct transparent
        communications between 802.3/Ethernet and FDDI LANs.

     o  Performs IP (TCP, UDP, ICMP, and EGP) data packet fragmenta-
        tion per RFC 791. Allows transparent forwarding of large FDDI
        IP packets to 802.3/Ethernet LANs.

     o  Performs spanning tree loop detection in both the IEEE 802.1d
        implementation as well as in Digital's LAN Bridge implemen-
        tation mode; automatically configures to Digital LAN Bridge
        and 802.1d Spanning Tree implementations for backwards com-
        patibility with the installed base of Digital LAN Bridge
        users.

     o  Provides flexible filtering (destination, address, protocol)
        and password access protection for greater network control,
        increased security and bandwidth utilization, and reduced
        propagation of network problems.

     o  Performs FDDI station management as specified by the ANSI
        X3T9 SMT V6.2 standard.

     o  Performs Ring Map functions as per SMT V6.2.

     o  Updates all the DECbridge 500 counters and settable parame-
        ters.

     o  Performs operating system support scheduling, maintains
        timers and receive requests.

     HARDWARE REQUIREMENTS

     The DECbridge 500 hardware is required to run DECbridge 500
     Software Microcode.

     SOFTWARE LICENSE

                                     2

 






     The Software License required to run DECbridge 500 Software
     Microcode is included with the hardware.

     This software microcode is furnished under the licensing pro-
     visions of Digital Equipment Corporation's Standard Terms and
     Conditions. For more information about Digitals licensing terms
     and policies, contact your local Digital office.
































                                     3

 






     SOFTWARE WARRANTY

     Warranty of this software microcode product is provided by Digi-
     tal with the purchase of a license for the product as defined in
     the Software Warranty Addendum of this SPD.

     [R]  The DIGITAL Logo is a registered trademark of Digital
          Equipment Corporation.

     [TM] DECbridge and DECelms are trademarks of Digital Equipment
          Corporation.




























                                     4
