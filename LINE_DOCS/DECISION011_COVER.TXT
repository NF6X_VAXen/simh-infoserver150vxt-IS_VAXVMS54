




          DIGITAL                                              AV-MN87B-TE


                       DECdecision Version 1.1 Read This First

          This package contains the DECdecision[TM] Version 1.1 software,
          installation instructions, and user documentation.

          Contents of This Kit

          This package contains the following:

          o  Bill of Materials

             Please read the Bill of Materials enclosed in the transparent
             plastic envelope. Check to see that all items listed there
             are in your kit. If any items are missing, contact your
             Digital representative.

          o  Media

             If you ordered media, you will find the media and the DECde-
             cision Installation and System Management Guide in this kit.
             This guide contains complete instructions for installing
             DECdecision Version 1.1 on a system running VMS Version 5.3-1
             or higher.

          o  Documentation

             If you ordered a full kit or a documentation-only kit, you
             will find a DECdecision documentation set, which includes the
             following manuals:

                DECdecision Overview
                DECdecision Access User's Guide
                DECdecision Calc User's Guide
                DECchart User's Guide
                DECdecision Builder User's Guide
                DECdecision Calc Macro Guide
                Converters Reference Guide



                                                (continued on other side)

 



          o  Software Product Description (SPD)

             The SPD provides an overview of DECdecision and its features.

          o  Software Performance Report (SPR)

             Use this form to report any problems with DECdecision (pro-
             vided you have purchased warranty services).


          Installation and Release Notes Information

          You should read the DECdecision Installation and System Manage-
          ment Guide before installing DECdecision.

          The DECdecision Release Notes contain information such as re-
          strictions, known problems, and documentation errors or addi-
          tions. You can read the Release Notes before installing DECdeci-
          sion by invoking VMSINSTAL and following the instructions in the
          DECdecision Installation and System Management Guide. You can
          read the Release Notes after installing DECdecision by printing
          the file SYS$HELP:DECISION011.RELEASE_NOTES.



















           � Digital Equipment Corporation. 1989, 1990. All rights reserved.


                                          2
