 






          ________________________________________________________________

          VMS/SNA, Version 2.0
                                                                   DIGITAL
          AV-JQ86D-TE

          April_1989______________________________________________________

          Dear Customer:

          Your VMS/SNA Version 2.0 software and user documentation, in-
          cluding installation instructions, are included in this package.

          o  The Bill of Materials specifies the number and contents of
             your distribution kit. Check to see that all items are in
             your kit. If any items are missing or damaged, contact your
             Digital Equipment Corporation representative.

          o  Make sure you have installed Version 5.0 or 5.1 of the VMS
             operating system.

          o  If you are planning to use X.25 switched virtual circuits for
             communication between your VMS/SNA system and the IBM host
             system, install and configure VAX Packetnet System Interface
             (P.S.I.) software, Version 4.2 (or later).

          o  Contact your Digital field representative for the installa-
             tion of the VAX communications hardware device you intend to
             use with VMS/SNA (if not already installed). See the Software
             Product Description for a list of devices supported on your
             VAX processor.

          o  Read VMS/SNA Installation before installing the software.
             VMS/SNA configuration is dependent upon your site-specific
             IBM configuration; you should have access to the IBM configu-
             ration details before you begin VMS/SNA installation.



 �1989        by Digital Equipment Corporationo All rights reserved o Printed in U.S.A.

 


          Page 2                                               AV-JQ86D-TE



          o  VMS/SNA requires the use of specific external communications
             hardware (such as modems) that should already be in place.
             In addition, the installer needs to know the operational
             configuration of the communications line (for example, full-
             duplex or half-duplex). These requirements are explained in
             VMS/SNA Installation.

          The VMS/SNA release notes for Version 2.0 are on line as part of
          the software distribution kit. These release notes inform you
          of software updates not included in the manual set. To access
          the release notes before completing the installation, specify
          OPTIONS N to the VMSINSTAL procedure:

                  $ @SYS$UPDATE:VMSINSTAL SNAVMS020 device OPTIONS N

          The OPTIONS N keyword allows you to indicate whether you want
          the release notes displayed or printed, or both, and the print
          queue name. Whether or not you specify OPTIONS N, the release
          notes are copied to the file SYS$HELP:SNAVMS$020.RELEASE_NOTES.
          Digital recommends that you do not delete the release notes for
          the previous version of VMS/SNA, if they are present.
