 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   3270 TERMINAL OPTION SOFTWARE    1.1     VV9AA           2  [CXM011]
 
   AAF01/VMS SUBROUTINE LIBRARY     2.0     EFRAA           2  [AAF01020]
 
   ADF01/VMS SUBROUTINE LIBRARY     4.0     735AA           2  [ADF01040]
 
   ALL-IN-1 MAIL                    1.0     YCZAA           1  [A1MAIL010]
 
   ALL-IN-1 MAIL/DEUTSCH            1.0     VZ7GA           3  [A1MAILDE010]
 
   ALL-IN-1 MAIL/DEUTSCH            1.0     YFTGA           3  [A1MAILDE010]
 
   ALL-IN-1 MAIL/DEUTSCH            1.0     YFUGA           3  [A1MAILDE010]
 
   ALL-IN-1 MAIL/FRANCAIS           1.0     VZ7PA           3  [A1MAILFR010]
 
   ALL-IN-1 MAIL/ITALIANO           1.0     VZ7UA           3  [A1MAILIT010]
 
   ALL-IN-1 PC SERVER FOR VMS       1.0     YFFAA           1  [A1SVC010]
 
   ALL-IN-1 SERVICES FOR            1.0     YG4AA           1  [ASD010]
   DECWINDOWS
 
   ALL-IN-1                         2.4     AAAAA    U      1  [A1ENGLISH024]
 
   ALL-IN-1 STARTER                 2.4     VNNAA           1  [ASENGLISH024]
 
   BASESTAR FOR VMS (Development)   3.0     YU9AA    N      2  [BCC030]
 
   BASESTAR FOR VMS (Runtime)       3.0     YUAAA    N      2  [BCCLIB030]
 
   CDA CONVERTER LIBRARY FOR VMS    1.0     VZAAA           1  [CDACVTLIB010]
 
   CDA CONVERTER LIBRARY FOR VMS    1.1     VZAAA           1  [CDACVTLIB011]
 
   DEC COMPUTER INTEGRATED          2.1     VGXAA           2  [CIT021]
   TELEPHONY APPLIC. INTERFACE
   FOR VMS
 
   DEC COMPUTER INTEGRATED          2.1     VGYAA           2  [CITSR021]
   TELEPHONY SERVER FOR VMS
 
   DEC GKS FOR VMS                  4.1     810AA           4  [DECGKS041]
 
   DEC GKS FOR VMS (Runtime)        4.1     811AA           4  [DECGKSRTO041]
 
   DEC GKS-3D FOR VMS               1.1     VFXEA           4  [DECGKS3D011]
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   DEC GKS-3D FOR VMS (Runtime)     1.1     VFYEA           4  [DECGKS3DRT011]
 
   DEC GKS-3D/JAPANESE FOR VMS      1.1     VFXJA           4  [DECGKS3DJ011]
 
   DEC GKS-3D/JAPANESE FOR VMS      1.1     VFYJA           4  [DECGKS3DRTJ011]
   (Runtime)
 
   DEC GKS/JAPANESE FOR             4.1     810JA           4  [DECGKSJ041]
   VMS/JAPANESE
 
   DEC GKS/JAPANESE FOR             4.1     811JA           4  [DECGKSJRTO041]
   VMS/JAPANESE (Runtime)
 
   DEC IEZ11 CLASS DRIVER FOR VMS   1.0     YEHAA           4  [IEZ11010]
 
   DEC INFOSERVER CLIENT FOR VMS    1.0     GGWAA           1  [ESS010]
 
   DEC ODA CDA GATEWAY FOR VMS      1.0     YHNAA    N      2  [ODA010]
 
   DEC PHIGS FOR VMS                2.2     0KBAA           4  [DECPHIGS022]
 
   DEC PHIGS FOR VMS (Runtime)      2.2     VK1AA           4  [DECPHIGSRTO022]
 
   DEC PHIGS/JAPANESE FOR           2.2     0KBJA           4  [DECPHIGSJ022]
   VMS/JAPANESE
 
   DEC PHIGS/JAPANESE FOR           2.2     VK1JA           4  [DECPHIGSJTRO022]
   VMS/JAPANESE (Runtime)
 
   DEC PRINTSERVER SUPPORTING       3.2     798AA           2  [LPSVS032]
   HOST SOFTWARE FOR VMS
 
   DEC RDBACCESS FOR ORACLE ON      1.0     YQVAA           4  [VIDAO010]
   VMS
 
   DEC RDBACESS FOR VAX RMS ON      1.0     YQZAA    N      4  [NSDS010]
   VMS
 
   DEC RDBEXPERT FOR VMS            1.0     VFJAA           4  [RDBX010]
 
   DEC REALTIME TEST INTEGRATOR     2.0     YWQAA    N      4  [RTI020]
   FOR VMS (Development)
 
   DEC REALTIME TEST INTEGRATOR     2.0     B15AA    N      4  [RTIRTO020]
   FOR VMS (Runtime)
 
   DEC SOFTPC FOR VMS               2.2     YNWAA           4  [SOFTPC022]
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   DEC TRELLIS OBJECT SYSTEM FOR    1.0     YLVAA           4  [TRELLIS010]
   VMS
 
   DEC VUIT (VISUAL USER            1.0     YHCAA           4  [VUIT010]
   INTERFACE TOOL) FOR VMS
 
   DEC/EDI                          1.0     YM2AA           2  [DECEDI010]
 
   DECBRIDGE 500 SOFTWARE           2.2     YLYAA           2  [DEFEB022]
   MICROCODE FOR VMS
 
   DECCONCENTRATOR 500 SOFTWARE     2.3     YLZAA           2  [DEFCN023]
   MICROCODE FOR VMS
 
   DECDECISION                      1.1     VW3AA           2  [DECISION011]
 
   DECDECISION/DEUTSCH              1.1     VW3GA           3  [DECISIONDE011]
 
   DECDECISION/ESPA�OL              1.1     VW3SA           3  [DECISIONES011]
 
   DECDECISION/FRANCAIS             1.1     VW3PA           3  [DECISIONFR011]
 
   DECDECISION/NEDERLANDS           1.1     VW3HA           3  [DECISIONNL011]
 
   DECDECISION/SVENSKA FOR VMS      1.1     VW3MA           3  [DECISIONSE011]
 
   DECDESIGN                        1.0     YFDAA           2  [DES010]
 
   DECDX/VMS                        1.2     708AA           2  [DDX012]
 
   DECELMS (DEC EXTENDED LAN        1.1     YFPAA           2  [ELMS011]
   MANAGEMENT SOFTWARE)
 
   DECFORMS                         1.3     VCHAA    U      1  [FORMS013]
 
   DECFORMS (Runtime)               1.3     VNSAA    U      1  [FORMSRT013]
 
   DECFORMS/JAPANESE                1.2     VCHJA    N      4  [FORMSJ012]
 
   DECFORMS/JAPANESE (Runtime)      1.2     VNSJA    N      4  [FORMSRTJ012]
 
   DECIMAGE APPLICATION SERVICES    2.0     892AA           2  [IMG020]
   FOR VMS
 
   DECIMAGE SCAN SOFTWARE FOR VMS   2.0     VPFAA           2  [ISA020]
 
   DECIMAGE STORAGE MANAGER FOR     1.0     893AA           2  [ISM010]
   VMS
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   DECINTACT                        1.1     VF1AA           4  [DECINTACT011]
 
   DECINTACT (Runtime)              1.1     VF2AA           4  [DECINTACT011]
 
   DECINTACT (Remote)               1.1     VF3AA           4  [DECINTACT011]
 
   DECISION EXPERT FOR VMS          1.0B    VI2AA           4  [DEEX010]
 
   DECMCC BASIC MANAGEMENT SYSTEM   1.1     YSUAA    U      4  [MCCBMS011]
 
   DECMCC DEVELOPER'S TOOLKIT       1.1     YSWAA    U      4  [MCCTK011]
 
   DECMCC DIRECTOR                  1.1     VM9AA    U      4  [MCCDIR011]
 
   DECMCC ENTERPRISE MANAGEMENT     2.0     YFVAA           4  [MCCEMS020]
   STATION
 
   DECMCC SITE MANAGEMENT STATION   2.0     YGLAA           4  [MCCSMS020]
 
   DECMCC TCP/IP SNMP ACCESS        1.0     YSVAA           4  [MCCTCPIP010]
   MODULE FOR VMS
 
   DECMCC WANDESIGNER               1.0     YMGAA           4  [NDS010]
 
   DECMESSAGEQ FOR VMS              1.0     GKPAA    N      2  [DMQ010]
 
   DECNDU (NETWORK DEVICE           1.0     YWWAA           2  [NDU010]
   UPGRADE) UTILITY FOR VMS
 
   DECNET-VAX                       5.4     D05AA           1  [NETVAX054]
 
   DECNET/SNA DATA TRANSFER         2.0     VEBAA           2  [SNADTFS020]
   FACILITY
 
   DECNET/SNA DATA TRANSFER         2.0     VEKAA           2  [SNADTFU020]
   FACILITY
 
   DECNET/SNA GATEWAY FOR CHANNEL   2.0     VC9AA           2  [SNACSA020]
   TRANSPORT
 
   DECNET/SNA GATEWAY FOR           1.1     S01AA           2  [SNACST011]
   SYNCHRONOUS TRANSPORT
 
   DECNET/SNA VMS 3270 DATA         1.4     363AA           2  [SNA3270014]
   STREAM PROGRAMMING INTERFACE
 
   DECNET/SNA VMS 3270 TERMINAL     1.5     454AA           2  [SNATE015]
   EMULATOR
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   DECNET/SNA VMS APPC/LU6.2        2.2     022AA           2  [SNALU62022]
   PROGRAMMING INTERFACE
 
   DECNET/SNA VMS APPLICATION       2.3     455AA           2  [SNALU0023]
   PROGRAMMING INTERFACE
 
   DECNET/SNA VMS DISOSS DOCUMENT   1.4     042AA           2  [SNADDXF014]
   EXCHANGE FACILITY
 
   DECNET/SNA VMS DISTRIBUTED       1.2     043AA           2  [SNADHCF012]
   HOST COMMAND FACILITY
 
   DECNET/SNA VMS GATEWAY           2.1     VCKAA           2  [SNAGM021]
   MANAGEMENT
 
   DECNET/SNA VMS PRINTER           1.2     044AA           2  [SNAPRE012]
   EMULATOR
 
   DECNET/SNA VMS REMOTE JOB        1.4     453AA           2  [SNARJE014]
   ENTRY
 
   DECOMNI/VMS                      1.0     YMEAA           2  [OMNI010]
 
   DECPAGE                          3.1     AANAA           4  [PAGE031]
 
   DECPAGE/DEUTSCH                  3.1     AANGA           3  [DECPAGEGERM031]
 
   DECPAGE/ESPA�OL                  3.1     AANSA           3  [DECPAGESPAN031]
 
   DECPAGE/ITALIANO                 3.1     AANUA           3  [DECPAGEITAL031]
 
   DECPRESENT FOR VMS               1.0     YHEAA           2  [PRESENT010]
 
   DECPRINT PRINTING SERVICES FOR   4.0     YNCAA    N      4  [CPS040]
   VMS
 
   DECPRINT PRINTING                4.0     YNCJA    N      4  [CPSJ040]
   SERVICES/JAPANESE FOR
   VMS/JAPANESE
 
   DECPRINT UTILITY FOR             1.0     VZPAA           4  [PSPRINT010]
   POSTSCRIPT TO SIXEL PRINTING
   FOR VMS
 
   DECROUTER 2000                   1.2     VI8AA           2  [ROU012]
 
   DECROUTER 250                    1.0     YG6AA           2  [RT2010]
 
   DECSCHEDULER FOR VMS             1.1     YLLAA           4  [SCHEDULER011]
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   DECSERVER 200 FOR VMS            3.1     VCBAA    U      2  [DS2031]
 
   DECSERVER 250 FOR VMS            1.0     VTMAA           2  [DS25010]
 
   DECSERVER 300 FOR VMS            2.0     VTUAA    U      2  [DS3020]
 
   DECSERVER 500 FOR VMS            2.1     03KAA           2  [DS5021]
 
   DECTRACE FOR VMS                 1.0A    VW2AA           4  [EPC010]
 
   DECVIEW3D FOR VMS                2.0     7969A           4  [DECVIEW3D020]
 
   DECVOICE SOFTWARE                1.1     VFUAA           4  [VOX011]
 
   DECWINDOWS DECNET/SNA 3270       1.0     VXBAA           2  [SNADWTE010]
   TERMINAL EMULATOR FOR VMS
 
   DECWRITE FOR VMS                 1.1     VVFAA           1  [DECWRITE011]
 
   DECWRITE/BRITISH FOR VMS         1.1     VVFEA    U      3  [DECWRITEBR011]
 
   DECWRITE/DANSK FOR VMS           1.1     VVFDA    U      3  [DECWRITEDA011]
 
   DECWRITE/DEUTSCH FOR VMS         1.1     VVFGA           3  [DECWRITEDE011]
 
   DECWRITE/ESPA�OL FOR VMS         1.1     VVFSA    U      3  [DECWRITEES011]
 
   DECWRITE/FRANCAIS FOR VMS        1.0     VVFCA           3  [DECWRITEFR010]
 
   DECWRITE/ITALIANO FOR VMS        1.0     VVFUA           3  [DECWRITEIT010]
 
   DECWRITE/JAPANESE FOR            1.0     VVFJA           4  [DECWRITEJA010]
   VMS/JAPANESE
 
   DECWRITE/NEDERLANDS FOR VMS      1.1     VVFHA    U      3  [DECWRITENL011]
 
   DECWRITE/NORSK FOR VMS           1.0     VVFNA           3  [DECWRITENO010]
 
   DECWRITE/SUOMI FOR VMS           1.1     VVFFA    U      3  [DECWRITEFI011]
 
   DECWRITE/SVENSKA FOR VMS         1.1     VVFMA           3  [DECWRITESE011]
 
   DIGITAL EXTENDED MATH LIBRARY    1.0A    YEZAA    U      4  [DXML010]
   FOR VMS
 
   DIGITAL CARTRIDGE SERVER         1.0     YWNAA    N      2  [DCSC010]
   COMPONENT FOR VMS
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   DECWINDOWS 4125 EMULATOR FOR     1.0     VZQAA    N      1  [DW4125010]
   VMS
 
   DECWINDOWS DECNET/SNA 3270       1.0     VXBJA    N      4  [SNADWTEJA010]
   TE/JAPANESE
 
   DRB32 VMS DRIVERS                3.0     VF5AA           2  [DRB32030]
 
   DRX11-C/VMS DRIVER               6.0     S36AA           2  [DRX11C060]
 
   EXTERNAL DOCUMENT EXCHANGE       2.1     761AA           2  [EDEDIS021]
   WITH IBM DISOSS
 
   FORTRAN IV/VAX TO RSX            2.8     107AA           4  [FORIV028]
 
   IEX-VMS-DRIVER                   4.2     519AA           2  [IEX042]
 
   INFOSERVER 100 SOFTWARE          1.1     YSHAA           1  [INFOSERV011]
 
   IXV/VAXELN DRIVER                2.0     VG3AA           2  [IXVELN020]
 
   IXV11/VMS DRIVER                 2.0     VHZAA           2  [IXV11020]
 
   LAN TRAFFIC MONITOR              1.2     VEHAA           2  [LTM012]
 
   MICROVAX MIRA SWITCH CONTROL     2.2     09TAA           2  [MRA022]
 
   MICROVAX/DRQ3B DEVICE DRIVER     1.3     0APAA           2  [HX013]
 
   MIRA HIGH AVAILABILITY           1.0     YHMAA           2  [HAMS010]
   MANAGEMENT SOFTWARE FOR VMS
 
   MUXSERVER 100 REMOTE TERMINAL    2.3     VE4AA           2  [MSVAENG023]
   SERVER
 
   MUXSERVER 300 REMOTE TERMINAL    1.2     VT7AA           2  [MS3012]
   SERVER FOR VMS
 
   NMCC/DECNET MONITOR              2.3     VTGAA           2  [NMCC023]
 
   NMCC/VAX ETHERNIM                2.3     514AA           2  [ENIM023]
 
   PATHWORKS FOR VMS                4.0     A93AA    N      2  [PCSA040]
 
   PBXSERVER                        2.1     VCCEA           2  [PBXSERVER021]
 
   PDP-11 C FOR VMS                 1.1     YEJAA           4  [PDP11C011]
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   PDP-11 DATATRIEVE/VAX            3.3A    105AA           4  [DTR11033]
 
   PDP-11 FORTRAN-77/VAX TO RSX     5.4     138AA           4  [FOR77PDP054]
 
   PDP-11 SYMBOLIC DEBUGGER/VAX     2.2     139AA           4  [PDPDEBUG022]
   TO RSX
 
   REMOTE SYSTEM MANAGER            2.2     B13AA           2  [RSMSRV022]
 
   SESSION SUPPORT UTILITY          1.1     VE3AA           2  [SSU011]
 
   TERMINAL SERVER MANAGER          1.3     VDHAA           2  [TSM013]
 
   VAX 2780/3780 PROTOCOL           1.7     111AA           2  [BSCPTP017]
   EMULATOR
 
   VAX 3271 PROTOCOL EMULATOR       2.5     112AA           2  [V3271E025]
 
   VAX ACMS (Runtime)               3.1A    076AA           1  [ACMSRTO031]
 
   VAX ACMS (Development)           3.1A    079AA           1  [ACMSDEV031]
 
   VAX ACMS (Remote)                3.1A    Y30AA           1  [ACMSREM031]
 
   VAX ADA                          2.2     056AA           1  [ADA022]
 
   VAX ADE                          2.5     425AA           4  [ADE025]
 
   VAX APL                          3.2     020AA           1  [APL032]
 
   VAX BASIC                        3.4     095AA           1  [BASIC034]
 
   VAX BLISS-32 IMPLEMENTATION      4.6     106AA           1  [BLS32046]
   LANGUAGE
 
   VAX C                            3.2     015AA           1  [VAXC032]
 
   VAX CDD/PLUS                     4.3     897AA    U      1  [CDD043]
 
   VAX COBOL GENERATOR              1.3     365AA           1  [GEN013]
 
   VAX COBOL GENERATOR/JAPANESE     1.3     365JA           4  [JGEN013]
 
   VAX COBOL                        4.4     099AA    U      1  [COBOL044]
 
   VAX COBOL/JAPANESE               4.3     099JA           4  [JCOBOL043]
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   VAX COMPUTER INTEGRATED          1.0     YG8AA           2  [CITMD010]
   TELEPHONY MESSAGE DESK
 
   VAX COPROCESSOR/RSX              1.1     VFEAA           4  [CPRSX011]
 
   VAX DATA DISTRIBUTOR             2.2     VDRAA           1  [DDAL022]
 
   VAX DATATRIEVE                   5.1     898AA           1  [DTR051]
 
   VAX DATATRIEVE/JAPANESE          4.2     898JA           4  [JDTR042]
 
   VAX DBMS                         4.2     899AA           1  [DBMDEV042]
 
   VAX DBMS (Runtime)               4.2     915AA           1  [DBMRTO042]
 
   VAX DEC/CMS                      3.4     007AA           1  [CMS034]
 
   VAX DEC/MAP                      3.0     VFZAA           1  [MAP030]
 
   VAX DEC/MMS                      2.6     VADAA           1  [MMS026]
 
   VAX DEC/TEST MANAGER             3.2     927AA           1  [DTM032]
 
   VAX DECALC                       3.1     310AA           1  [CALC031]
 
   VAX DECALC-PLUS                  3.1     A98AA           1  [PLUS031]
 
   VAX DECGRAPH                     1.6     360AA           1  [GRAPH016]
 
   VAX DECSCAN VMS AND ELN BITBUS   2.0     VCJAA           2  [IBQ020]
   DRIVERS
 
   VAX DECSCAN VMS SOFTWARE         2.1     VCSAA           2  [IOTK021]
   TOOLKIT
 
   VAX DECSLIDE                     1.4     361AA           4  [SLIDE014]
 
   VAX DIBOL                        4.1A    018AA           1  [DIBOL041]
 
   VAX DISK STRIPING DRIVER FOR     1.0     YELAA           2  [STRIPE010]
   VMS
 
   VAX DISTRIBUTED FILE SERVICE     1.2     VEQAA           2  [DFS012]
 
   VAX DISTRIBUTED NAME SERVICE     1.1A    VERAA           2  [DNS011]
 
   VAX DISTRIBUTED QUEUING          1.1     VENAA           2  [DQS011]
   SERVICE
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   VAX DOCUMENT                     2.0     VEEAA           1  [DOC020]
 
   VAX DSM                          6.0     130AA           2  [DSM060]
 
   VAX FMS                          2.4     VD7AA           1  [FMSFDV024]
 
   VAX FMS (Runtime)                2.4     VD8AA           1  [VAXFMS024]
 
   VAX FMS/JAPANESE                 2.4     VD7JA           4  [FMSFDVJ024]
 
   VAX FMS/JAPANESE (Runtime)       2.4     VD8JA           4  [VAXFMSJ024]
 
   VAX FORTRAN HIGH PERFORMANCE     1.1     YHBAA           1  [FORTHPO011]
   OPTION
 
   VAX FORTRAN                      5.6     100AA           1  [FORT056]
 
   VAX ISDN ACCESS                  1.1     VZCAA           2  [ISDN-ACCESS011]
 
   VAX ISDN                         1.1     VZ9AA           2  [ISDN011]
 
   VAX LANGUAGE SENSITIVE           3.1     057AA           1  [LSE031]
   EDITOR/SOURCE CODE ANALYZER
 
   VAX LISP/VMS                     3.1     917AA           2  [LISP031]
 
   VAX LN03 IMAGE SUPPORT           1.0     VZRAA           2  [LN03IMAGE010]
   SOFTWARE
 
   VAX NOTES                        2.0     960AA           1  [NOTES020]
 
   VAX NOTES                        2.1     960AA           1  [NOTES021]
 
   VAX OPS5                         3.0A    913AA           1  [OPS030]
 
   VAX PACKETNET SYSTEM INTERFACE   4.3     061AA           2  [PSI_AC043]
   ACCESS
 
   VAX PACKETNET SYSTEM INTERFACE   4.3     071AA           2  [PSI043]
 
   VAX PASCAL                       4.2     126AA    U      1  [PASCAL042]
 
   VAX PERFORMANCE ADVISOR          2.1     VE5AA           1  [VPA021]
 
   VAX PERFORMANCE AND COVERAGE     3.0     119AA           1  [PCA030]
   ANALYZER
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   VAX PL/I                         3.4     114AA           1  [PLI034]
 
   VAX PRINTSERVER CLIENT           3.0     797AA           2  [LPSVC030]
   SOFTWARE
 
   VAX PRINTSERVER SUPPORTING       3.2     798JA           4  [LPSVSJ032]
   HOST SW/JAPANESE FOR
   VMS/JAPANESE
 
   VAX PUBLIC ACCESS                1.3     VFHAA    U      1  [VAXPAC013]
   COMMUNICATIONS
 
   VAX RALLY                        2.2     A86AA           1  [RALDO022]
 
   VAX RALLY (Runtime)              2.2     VF4AA           1  [RALRTO022]
 
   VAX RDB/ELN                      2.2     D07AA           4  [RDBB022]
 
   VAX RDB/VMS                      4.0     358AA           1  [RDBVMSRTO040]
 
   VAX RDB/VMS (Interactive)        4.0     VCLAA           1  [RDBVMSINT040]
 
   VAX RDB/VMS (Full Development)   4.0     VD2AA           1  [RDBVMSDEV040]
 
   VAX RDB/VMS/JAPANESE (Full       4.0     VD2JA           4  [JRDBVMSDEV040]
   Development)
 
   VAX REAL-TIME ACCELERATOR        2.0A    VJNAA           2  [VRTA020]
   SOFTWARE
 
   VAX REMOTE ENVIRONMENTAL         1.2     VI5AA           2  [REMS012]
   MONITORING SOFTWARE
 
   VAX RMS JOURNALING               5.4     VDVAA           1  [RMSJNL054]
 
   VAX SCAN                         1.2     495AA           1  [SCAN012]
 
   VAX SOFTWARE PERFORMANCE         3.4     850AA           1  [SPM034]
   MONITOR
 
   VAX SOFTWARE PERFORMANCE         3.4     VUPAA           1  [SPMC034]
   MONITOR (Collect)
 
   VAX SOFTWARE PROJECT MANAGER     1.2     A82AA           1  [PM012]
 
   VAX STORAGE LIBRARY SYSTEM       2.0A    0L7AA    U      1  [SLS020]
 
   VAX STORAGE LIBRARY SYSTEM       2.0A    YE8AA    U      1  [SLSRMT020]
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   VAX TDMS                         1.9A    706AA           1  [TDMSDEV019]
 
   VAX TDMS (Runtime)               1.9A    711AA           1  [TDMSRTO019]
 
   VAX TEAMDATA                     1.4     741AA           1  [TDA014]
 
   VAX TEAMDATA/DANSK               1.3     741DA           3  [TDADA013]
 
   VAX TEAMDATA/DEUTSCH             1.3     741GA           3  [TDADE013]
 
   VAX TEAMDATA/ESPANOL             1.3     741SA           3  [TDAES013]
 
   VAX TEAMDATA/FRANCAIS            1.3     741PA           3  [TDAFR013]
 
   VAX TEAMDATA/NORSK               1.3     741NA           3  [TDANO013]
 
   VAX TEAMINFO/SVENSKA             1.3     741MA           3  [TDASE013]
 
   VAX VERTICAL FORMS PRINTING      1.0     VZEAA           1  [VFP010]
   FOR VMS
 
   VAX VTX                          4.1     031AA           1  [VTX041]
 
   VAX WIDE AREA NETWORK DEVICE     1.1A    VAWAA           2  [SYNC011]
   DRIVERS
 
   VAX XWAY                         1.2     729AA           2  [XWAY012]
 
   VAX-11 RSX                       2.5     382AA           2  [RSX025]
 
   VAX/EDI (UK)                     1.1     VHWAA           2  [VAXEDI011]
 
   VAXCLUSTER CONSOLE SYSTEM        1.3     V01AA           1  [VCS013]
 
   VAXCLUSTER SOFTWARE              5.4     VBRAA           1  [VAXCLU054]
 
   VAXELN ADA                       2.2     A97AA           4  [VAXELN_ADA022]
 
   VAXELN KMV1A TOOL KIT            1.1     0JPAA           4  [KMV_ELNTOOL011]
 
   VAXELN TOOLKIT                   4.2     375AA           4  [ELN042]
 
   VAXFT SYSTEM SERVICES            1.0     YEAAA           4  [FTSS010]
 
   VAXSET                           10.0    965AA           1  [VAXSET100]
 
   VIDA FOR DB2                     1.0A    VTWAA           1  [VIDA2010]
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   VIDA/JAPANESE FOR DB2            1.0     VTWJA    N      4  [VIDAJ2010]
 
   VMS                              2.1     YFSPB           3  [DECWFR_CA021]
   DECWINDOWS/CANADIEN-FRANCAIS
 
   VMS DECWINDOWS/DEUTSCH           2.1     YFSGA           3  [DECWDE_DE021]
 
   VMS DECWINDOWS/DEUTSCH/CH        2.1     YFSGB           3  [DECWDE_CH021]
 
   VMS DECWINDOWS/FRANCAIS          2.1     YFSPA           3  [DECWFR_FR021]
 
   VMS DECWINDOWS/FRANCAIS/CH       2.1     YFSPC           3  [DECWFR_CH021]
 
   VMS DECWINDOWS/ITALIANO          2.1     YFSUA           3  [DECWIT_IT021]
 
   VMS DECWINDOWS/ITALIANO/CH       2.1     YFSUB           3  [DECWIT_CH021]
 
   VMS DECWINDOWS/NEDERLANDS        2.1     YFSHA           3  [DECWNL_NL021]
 
   VMS DECWINDOWS/NORSK             2.1     YFSNA           3  [DECWNO_NO021]
 
   VMS DECWINDOWS/SVENSKA           2.1     YFSMA           3  [DECWSV_SE021]
 
   VMS OPERATING SYSTEM             5.4     001AA           1  [VMS054]
 
   VMS OPERATING SYSTEM             5.4-2   001AA    U      1  [VMSU2054]
 
   VMS VOLUME SHADOWING             5.4     AB2AA           1  [VOLSHD054]
 
   VMS WORKSTATION SOFTWARE         4.3     A96AA           1  [VWS043]
 
   VMS WORKSTATION                  4.3     A96JA           4  [JVWS043]
   SOFTWARE/JAPANESE
 
   VMS/JAPANESE                     5.4     001JA           4  [JVMS054]
 
   VMS/JAPANESE                     5.4-1   001JA           4  [JVMSU1054]
 
   VMS/JAPANESE                     5.4-2   001JA    U      4  [JVMSU2054]
 
   VMS/SNA                          2.0     362AA           2  [SNAVMS020]
 
   VMS/ULTRIX CONNECTION            1.3A    VHRAA           1  [UCX013]
 
   WPS-PLUS/VMS                     4.0     AAMAA           1  [WPLENGLISH040]
 
   WPS-PLUS/VMS/ESPA�OL             3.1     AAMSA           3  [WPLSPANISH031]
 
 
 
 
   N = New Product    U = Updated Product

 
   May 1991 Master Index:
 
 
   Product Name                     Vers.   UPI     Stat   CD  Directory
   ------------------------------   -----   -----   ----   --  -----------------
 
   WPS-PLUS/VMS/FRANCAIS            3.1     AAMPA           3  [WPLFRENCH031]
 
   WPS-PLUS/VMS/SVENSKA             3.1     AAMMA           3  [WPLSWEDISH031]
 
   X25ROUTER 2000                   1.1     S02AA           2  [X25ROU011]
 
