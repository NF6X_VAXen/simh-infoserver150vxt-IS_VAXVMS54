 






          System
          Support
          Addendum

          ________________________________________________________________

          PRODUCT NAME:   VAX DEC/CMS, Version 3.4          SSA 25.52.15-A

          HARDWARE REQUIREMENTS

          Processors Supported

          VAX:      VAX 4000 Model 300,
                    VAXft 3000 Model 310,
                    VAX 6000 Model 200 Series,
                    VAX 6000 Model 300 Series,
                    VAX 6000 Model 400 Series

                    VAX 8200, VAX 8250, VAX 8300, VAX 8350,
                    VAX 8500, VAX 8530, VAX 8550, VAX 8600,
                    VAX 8650, VAX 8700, VAX 8800, VAX 8810,
                    VAX 8820, VAX 8830, VAX 8840, VAX 8842,
                    VAX 8974, VAX 8978

                    VAX 9000-210, VAX 9000-410

                    VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

          MicroVAX: MicroVAX II, MicroVAX 2000,
                    MicroVAX 3100, MicroVAX 3300,
                    MicroVAX 3400, MicroVAX 3500,
                    MicroVAX 3600, MicroVAX 3800,
                    MicroVAX 3900






                                       DIGITAL              September 1990

                                                               AE-LT63E-TE

 


           VAX DEC/CMS, Version 3.4                         SSA 25.52.15-A




          VAXstationVAXstation II, VAXstation 2000,
                    VAXstation 3100 Series, VAXstation 3200,
                    VAXstation 3500, VAXstation 3520,
                    VAXstation 3540

          VAXserver:VAXserver 3100, VAXserver 3300,
                    VAXserver 3400, VAXserver 3500,
                    VAXserver 3600, VAXserver 3602,
                    VAXserver 3800, VAXserver 3900,

                    VAXserver 6000-210, VAXserver 6000-310,
                    VAXserver 6000-410, VAXserver 6000-420

          Processors Not Supported

          VAX-11/725, VAX-11/782, MicroVAX I, VAXstation I, VAXstation
          8000

          System configuration must have sufficient on-line mass storage
          to accommodate VAX DEC/CMS and the project libraries. Most
          projects will require no more storage than they are now using.

          Processor Restrictions

          A TK50 Tape Drive is required for standalone MicroVAX 2000 and
          VAXstation 2000 systems.

          Block Space Requirements (Block Cluster Size = 1):

          Disk space required for      7,000 blocks
          installation:
                                       (3.6M bytes)

          Disk space required for      3,900 blocks
          use (permanent):
                                       (2.0M bytes)


                                          2

 


           VAX DEC/CMS, Version 3.4                         SSA 25.52.15-A



          These counts refer to the disk space required on the system
          disk. The sizes are approximate; actual sizes may vary depending
          on the user's system environment, configuration, and software
          options.

          Upon installation, a conversion from Version 2 to Version 3 of
          VAX DEC/CMS will require additional disk space equivalent to the
          size of the user's existing Version 2.x VAX DEC/CMS library.

          The minimum supported memory for this application running in
          a standalone DECwindows environment with both the client and
          server executing on that same system is 8 MB.

          The minimum supported memory is the minimum that will allow
          the product to run adequately. For optimal performance of the
          product, the recommended memory is 12 MB.

          OPTIONAL HARDWARE

          None



















                                          3

 


           VAX DEC/CMS, Version 3.4                         SSA 25.52.15-A



          CLUSTER ENVIRONMENT

          This layered product is fully supported when installed on any
          valid and licensed VAXcluster* configuration without restric-
          tions. The HARDWARE REQUIREMENTS sections of this product's
          Software Product Description and System Support Addendum detail
          any special hardware required by this product.

          *  V5.x VAXcluster configurations are fully described in the
             VAXcluster Software Product Description (29.78.xx) and in-
             clude CI, Ethernet and Mixed Interconnect configurations.

          SOFTWARE REQUIREMENTS

          For Systems Using Terminals (No DECwindows Interface):

          o  VMS Operating System V5.3 - V5.4

          For Workstations Running VWS:

          o  VMS Operating System V5.3 - V5.4

          o  VMS Workstation Software V4.2

          For Workstations Running DECwindows:

          o  VMS Operating System V5.3 - V5.4 (and necessary components of
             VMS DECwindows)

          This product may run in either of the following ways:

          o  Stand-alone execution - running the X11 display server and
             the client application on the same machine.

          o  Remote execution - running the X11 display server and the
             client application on different machines.

          VMS DECwindows is part of the VMS Operating System but must
          be installed separately. Installation of VMS DECwindows gives
          you the option to install any or all of the following three
          components:

                                          4

 


           VAX DEC/CMS, Version 3.4                         SSA 25.52.15-A



          o  VMS DECwindows Compute Server (Base kit; includes runtime
             support)

          o  VMS DECwindows Device Support

          o  VMS DECwindows Programming Support

          For stand-alone execution, the following DECwindows components
          must be installed on the machine:

          o  VMS DECwindows Device Support

          o  VMS DECwindows Programming Support

          For remote execution, the following DECwindows components must
          be installed on the machine:

          Server Machine

          o  VMS DECwindows Compute Server (Run-time Support)

          Client Machine

          o  VMS DECwindows Compute Server (Run-time Support)

          VMS Tailoring

          For VMS V5.x, the following VMS classes are required for full
          functionality of this layered product:

          o  VMS Required Saveset

          o  Programming Support

          o  Utilities

          For more information on VMS classes and tailoring, refer to
          the VMS Operating System Software Product Description (SPD
          25.01.xx).

          OPTIONAL SOFTWARE

          None

                                          5

 


           VAX DEC/CMS, Version 3.4                         SSA 25.52.15-A



          GROWTH CONSIDERATIONS

          The minimum hardware/software requirements for any future ver-
          sion of this product may be different from the minimum require-
          ments for the current version.

          DISTRIBUTION MEDIA

          Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

          This product is also available as part of the VMS Consolidated
          Software Distribution on CDROM.

          ORDERING INFORMATION

          Software Licenses: QL-007A*-**
          Software Media: QA-007A*-**
          Software Documentation: QA-007AA-GZ
          Software Product Services: QT-007A*-**

          *  Denotes variant fields. For additional information on avail-
             able licenses, services and media refer to the appropriate
             price book.

          The above information is valid at time of release. Please con-
          tact your local Digital office for the most up-to-date informa-
          tion.

          [TM]  The DIGITAL Logo, VMS, DECwindows, VAX, MicroVAX, VAXs-
                tation, VAXserver and VAXset are trademarks of Digital
                Equipment Corporation.








                                          6
