 










                                             Terry Bass
                                             DEComni/VMS Product Manager
                                             Digital Equipment Corporation
                                             600 Nickerson Road
                                             Marlboro, Massachusetts 01752

          Dear DEComni/VMS V1.0 Customer:

          On behalf of the DEComni/VMS development team, I would
          like to thank you for purchasing DEComni/VMS Version
          1.0.

          Enclosed is the DEComni/VMS Version 1.0 kit. It con-
          sists of the appropriate software distribution media
          (including online release notes) and software documen-
          tation.

          Release Notes Information

          The online release notes for DEComni/VMS Version 1.0
          contain information such as restrictions, incompatibil-
          ities, and changes to documentation.

          See the DEComni/VMS Software Installation Guide on how
          to access and read these release notes.

          DEComni/VMS V1.0 Documentation

          The following documentation is included in your DEComni
          /VMS Version 1.0 kit.

          o  DEComni/VMS Software Installation Manual

          o  DEComni/VMS Application Programmer's Guide

          o  DEComni/VMS Guide to Using OmniView

 


                                                           Page 2



          o  DEComni/VMS Network Manager's Guide


                                             Regards,



                                             Terry Bass
                                             Product Manager
