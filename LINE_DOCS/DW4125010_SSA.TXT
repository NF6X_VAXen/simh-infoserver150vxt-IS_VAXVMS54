 






     System
     Support
     Addendum

     ________________________________________________________________
     ________________________________________________________________

     PRODUCT NAME:  DECwindows 4125 Emulator for VMS, Version 1.0 SSA
     31.60.00-A

     HARDWARE REQUIREMENTS

     Processors Supported

     VAX:      VAXft 3000-310,
               VAX 6000 Model 200 Series,
               VAX 6000 Model 300 Series,
               VAX 6000 Model 400 Series,
               VAX 6000 Model 500 Series

               VAX 8200, VAX 8250, VAX 8300, VAX 8350,
               VAX 8500, VAX 8530, VAX 8550, VAX 8600,
               VAX 8650, VAX 8700, VAX 8800, VAX 8810,
               VAX 8820, VAX 8830, VAX 8840

               VAX 9000-210, VAX 9000 Model 400 Series

               VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

     MicroVAX: MicroVAX II, MicroVAX 2000,
               MicroVAX 3100, MicroVAX 3300,
               MicroVAX 3400, MicroVAX 3500,
               MicroVAX 3600, MicroVAX 3800,
               MicroVAX 3900





                                  DIGITAL                  April 1991

                                                          AE-PBD8A-TE

 


     DECwindows 4125 Emulator for VMS, Version 1.0     SSA 31.60.00-A




     VAXstationVAXstation II, VAXstation 2000,
               VAXstation 3100, VAXstation 3200,
               VAXstation 3500, VAXstation 3520,
               VAXstation 3540

     VAXserver:VAXserver 3100, VAXserver 3300,
               VAXserver 3400, VAXserver 3500,
               VAXserver 3600, VAXserver 3602,
               VAXserver 3800, VAXserver 3900

               VAXserver 6000-210, VAXserver 6000-220,
               VAXserver 6000-310, VAXserver 6000-320,
               VAXserver 6000-410, VAXserver 6000-420,
               VAXserver 6000-510, VAXserver 6000-520

     Processors Not Supported

     MicroVAX I, VAXstation I, VAX-11/725, VAX-11/782, VAXstation
     8000

     Other Hardware Required

     A terminal with the ability to display DECwindows.

     Disk Space Requirements (Block Cluster Size = 1):

     Disk space required for      4,000 blocks
     installation:
                                  (2,048K bytes)

     Disk space required for      4,000 blocks
     use (permanent):
                                  (2,048K bytes)

     These counts refer to the disk space required on the system
     disk. The sizes are approximate; actual sizes may vary depending
     on the user's system environment, configuration, and software
     options.

                                     2

 


     DECwindows 4125 Emulator for VMS, Version 1.0     SSA 31.60.00-A



     Memory Requirements for DECwindows Support:

     The minimum supported memory for this application running in
     a standalone DECwindows environment with both the client and
     server executing on that same system is 6MB.

     OPTIONAL HARDWARE

     Eight Plane Graphics Board is required for full color display
     (optional if just black and white display is desired).

     CLUSTER ENVIRONMENT

     This layered product is fully supported when installed on any
     valid and licensed VAXcluster* configuration without restric-
     tions. The HARDWARE REQUIREMENTS sections of this product's
     Software Product Description and System Support Addendum detail
     any special hardware required by this product.

     *  V5.x VAXcluster configurations are fully described in the
        VAXcluster Software Product Description (29.78.xx) and in-
        clude CI, Ethernet and Mixed Interconnect configurations.

     SOFTWARE REQUIREMENTS

     For Workstations Running DECwindows:

     VMS Operating System V5.2 - V5.4

     This product may run in either of the following ways:

     o  Stand-alone execution - running the X11 display server and
        the client application on the same machine.

     o  Remote execution - running the X11 display server and the
        client application on different machines.

     VMS Tailoring

     For VMS V5.x systems, the following VMS classes are required for
     full functionality of this layered product:

     o  VMS Required Saveset

                                     3

 


     DECwindows 4125 Emulator for VMS, Version 1.0     SSA 31.60.00-A



     o  Network Support

     o  Programming Support

     o  System Programming Support

     o  Secure User's Environment

     o  Utilities

     o  RMS Journaling Files

     o  Miscellaneous Files

     o  BLISS Require Files

     o  Example Files

     o  User Environment Test Package

     o  VMS Workstation Support

     For more information on VMS classes and tailoring, refer to
     the VMS Operating System Software Product Description (SPD
     25.01.xx).

     OPTIONAL SOFTWARE

     None

     GROWTH CONSIDERATIONS

     The minimum hardware/software requirements for any future ver-
     sion of this product may be different from the minimum require-
     ments for the current version.

     DISTRIBUTION MEDIA

     Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

     ORDERING INFORMATION

                                     4

 


     DECwindows 4125 Emulator for VMS, Version 1.0     SSA 31.60.00-A



     Software Licenses: QL-VZQA*-**
     Software Media: QA-VZQA*-**
     Software Documentation: QA-VZQAA-GZ
     Software Product Services: QT-VZQA*-**

     *  Denotes variant fields. For additional information on avail-
        able licenses, services and media, refer to the appropriate
        price book.

     The above information is valid at time of release. Please con-
     tact your local Digital office for the most up-to-date informa-
     tion.

     [R]   Tektronix is a registered trademark of Tektronix, Inc.

     [TM]  The DIGITAL Logo, DECwindows, VAX, VAXstation, VAXserver,
           VAXcluster and VMS are trademarks of Digital Equipment
           Corporation.





















                                     5
