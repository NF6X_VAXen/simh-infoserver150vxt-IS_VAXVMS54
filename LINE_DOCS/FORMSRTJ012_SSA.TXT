     System
     Support
     Addendum
     ================================================================

     日本語DECforms Version 1.2 システム・サポート付加情報  SSA 26.K4.00-A

     必要なハードウェアの最小構成

     適用プロセッサ

     VAX:    VAX 4000 モデル300,
             VAX 6000 モデル200シリーズ,
             VAX 6000 モデル300シリーズ,
             VAX 6000 モデル400シリーズ,
             VAX 6000 モデル500シリーズ,
             VAX 8200, VAX 8250, VAX 8300, VAX 8350,
             VAX 85xx, VAX 8600, VAX 8650, VAX 8700,
             VAX 8800, VAX 8810, VAX 8820, VAX 8830,
             VAX 8840,
             VAX 9000モデル210, VAX 9000モデル410,
             VAXft 3000モデル310,
             VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

     MicroVAX:   MicroVAX II, MicroVAX 2000,
                 MicroVAX 3100, MicroVAX 3300,
                 MicroVAX 3400, MicroVAX 3500,
                 MicroVAX 3600, MicroVAX 3800,
                 MicroVAX 3900

     VAXstation: VAXstation II, VAXstation 2000,
                 VAXstation 3100シリーズ,
                 VAXstation 3200, VAXstation 3500,
                 VAXstation 3520, VAXstation 3540

     VAXserver:  VAXserver 3100, VAXserver 3300,
                 VAXserver 3400, VAXserver 3500,
                 VAXserver 3600, VAXserver 3602,
                 VAXserver 3800, VAXserver 3900,
                 VAXserver 4000 モデル300,
                 VAXserver 6000-210/220,
                 VAXserver 6000-310/320,
                 VAXserver 6000-410/420,
                 VAXserver 6000-510/520

     適用されないプロセッサ

     o  VAX-11/725

     o  VAX-11/782

     o  MicroVAX I

     o  VAXstation I

     o  VAXstation 8000

     プロセッサ制限事項

     スタンドアロンの MicroVAX 2000 および VAXstation 2000 では，
     TK50 テープ駆動装置が必要です。


     端末装置[1]

     VT80               VT100

     AVO付きVT100       VT101

     VT102              VT103

     VT125              VT131[2]

     VT220              VT240

     VT241              VT282

     VT320[3]           VT330[3]

     VT340[3]           VT382[3]

     IBM[TM]3270コンパチブル[4]

     [1]日本語DECformsはVT100, VT200およびVT300シリーズのヘブライ語の
        端末装置もサポートします。

     [2]VT131はVT102モードでのみサポートされます。

     [3]日本語DECformsでは25行目のステータス行の使用，およびVT300シ
        リーズ端末のロケータ装置の使用をサポートしていません。

     [4]日本語DECformsでは，IBM 3270クラスの端末装置が DECserver 550
        のCXM04 3270 Terminal Option Cardに接続されている場合のみ，そ
        の端末装置上へのフォームの表示をサポートします。このオプショ
        ン・カードによって，アプリケーションに対して，IBM 3270コンパ
        チブル装置をVT220であるかのように見せることができます。

     端末エミュレータ

     VAXstationやパーソナル・コンピュータ上の端末エミュレータは，その
     エミュレータが VT100, VT220 あるいは VT320環境に従ってエミュレー
     トするという程度にのみサポートします。


     プリンタ

     日本語DECformsのパネルやフォームは，当社のプリンタで印刷すること
     ができます。ビデオ属性は印刷されません。また，線は線画用文字セッ
     トではなく"-", "+", "|"を使って描かれます。

     必要なディスク容量
     (Block Cluster Size=1 の場合)

     インストール時
          フル開発システム:        約 34,300ブロック
                                   (17Mバイト)
          ランタイム・システム:    約 4,500ブロック
                                   (2.3Mバイト)
     運用時
          フル開発システム:
               サンプルを含む      約 21,000ブロック
                                   (11Mバイト)
               サンプルを含まな    約 15,000ブロック
               い
                                   (7.5Mバイト)
          ランタイム・システム:    約 4,000ブロック
                                   (2Mバイト)

     これらのディスク容量は，システム・ディスク上で必要となるディスク
     領域を表したものであり，概数です。実際の値は，システムの環境，構
     成，ソフトウェア・オプションに応じて異なります。

     オプション・ハードウェア

     必須/オプション・ソフトウェアでサポートされる装置。


     クラスタ環境

     本レイヤード・プロダクトは，正規のライセンスを持つVAXcluster構
     成*の上にインストールされている場合，機能上の制限は受けません。
     本ソフトウェアで必要とされるハードウェアについては「必要なハード
     ウェアの最小構成」の項に記述されているとおりです。

     *  バージョン5.xのVAXcluster構成については「VAXcluster Software
        Product Description (SPD 29.78.xx)」に詳しく説明されていま
        す。 また，この構成にはCI, NI および Mixed Interconnect構成が
        含まれます。

     必須ソフトウェア

     o  日本語VMS オペレーティング・システム V5.1〜V5.4

     VMS テーラリング

     VMS バージョン5.0以降のシステムでは，本レイヤード・プロダクトの
     全機能を得るには次のVMS構成要素が必要です。

     o  VMS Required Saveset

     o  Programming Support

     o  Utilities

     VMSのクラスおよびテーラリングについては「VMS Operating System
     Software Product Description (SPD 25.01.xx)」および「日本語VMS
     オペレーティング・システム ソフトウェア仕様書 (SPD 25.C4.xx)」を
     参照してください。

     オプション・ソフトウェア

     以下の製品のバージョンは，オペレーティング・システムのバージョン
     に依存しています。必要なバージョンについては，各製品のシステム・
     サポート付加情報を参照してください。

     o  VAX CDD/Plus V4.1〜V4.1A


     バージョンアップの考慮

     本製品の将来のバージョンでは，必要なハードウェアおよびソフトウェ
     アの最小構成が変更されることがあります。

     提供媒体

     テープ:     9トラック 1600BPI 磁気テープ

                 TK50 ストリーム・テープ


     注文情報

     最寄りの日本DECの各支店/営業所にお問い合わせください。


     ___________________

  [TM] IBM は米国International Business Machines社の省略です。

                                                            1991年1月
                                                       AE-B584A-TE-JO
