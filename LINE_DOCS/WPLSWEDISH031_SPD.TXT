


PRODUKTNAMN: WPS-PLUS/VMS/SVENSKA v3.1          SPD 27-94-01

ALLM�N BESKRIVNING

WPS-PLUS/VMS/SVENSKA �r ett menystyrt textbehandlingssystem f�r 
VMS-anv�ndare. Det installeras som till�ggsprogramvara i ett 
VMS-system.

Textbehandlingssystemet WPS-PLUS/VMS best�r av:

^   redigering med "GULD-tangent"

^   multinationell teckenupps�ttning

^   teknisk teckenupps�ttning

^   tv�dimensionell grafik

^   listbehandling, f�r skapande av standardiserade brev

^   redigeringsmatematik

^   sortering

^   anv�ndardefinierade funktioner (ADF:er)

^   filkonvertering (.WPL, .DX, ASCII)

^   hj�lpfunktion

^   datorst�dd utbildning

^   dokumentation


Med WPS-PLUS/VMS kan anv�ndaren:

^   skapa, redigera och skriva ut dokument

^   infoga numrerade fotnoter eller slutnoter i dokument

^   numrera avsnitt i upp till nio niv�er

^   skapa inneh�llsf�rteckningar

^   skapa sakregister och k�llf�rteckningar

^   anv�nda �ndringsmarkering och bakgrundsmarkering

^   klippa ut och klistra in kolumner

^   definiera omr�den d�r sidbrytning inte f�r g�ras

^   kontrollera enstaka rader �verst och nederst p� sidor

^   v�lja att f� automatisk avstavning vid sidindelning

^   skapa brevlistor och standardiserade brev 

^   s�ka efter dokument utifr�n deras titel, nummer eller nyckelord

^   h�mta in ett dokument i ett annat genom att anv�nda 
    tangentkombinationen <GULD> <H�MTA DOKUMENT>

^   konvertera ett WPS-PLUS-dokument till en VMS-fil och s�nda det med 
    VMSmail (textframh�vningar f�rsvinner dock)

^   skriva tekniska tecken och vetenskapliga ekvationer i dokument

^   infoga diagram, matriser och VT100:s teckenupps�ttning

^   s�ka efter framh�vningar, som fetstil och understrykning

^   �verf�ra dokument till och fr�n DECmate II och DECmate III

^   �verf�ra dokument till och fr�n underst�dda persondatorer som har 
    WPS-PLUS/DOS

^   anv�nda skrivare fr�n andra leverant�rer �n Digital, med hj�lp av 
    skrivartabeller (Printer Tables Utility, PTU)

^   anv�nda olika programvaror fr�n Digital med hj�lp av External 
    Application Link (XAL)

^   l�ra sig WPS-PLUS genom att anv�nda datorst�dd utbildning


Best�ndsdelar

Textbehandling

Textbehandlingssystemet WPS-PLUS/VMS omfattar en m�ngd olika
redigeringsfunktioner. Exempel p� vad anv�ndaren kan g�ra vid
redigering �r:
    
^   anv�nda fullsk�rmsredigering och flytta mark�ren i fyra riktningar

^   reglera marginaler och tabulatorstopp, inklusive v�nsterjusterade, 
    h�gerjusterade och decimaljusterade tabulatorstopp

^   skapa och visa dokument p� upp till 256 teckens bredd

^   lagra upp till tio linjaler f�r inst�llning av marginaler och 
    tabulatorstopp; dessa linjaler kan sedan snabbt tas fram

^   anv�nda automatisk raddelning, halvautomatisk avstavning och 
    utskrift med rak eller oj�mn h�germarginal

^   skriva text en halvrad upp eller ner (exponent och index)

^   anv�nda kombinerade och multinationella tecken

^   anv�nda tekniska och vetenskapliga tecken

^   l�gga in datum och tid i dokumentet

^   anv�nda klipp-och-klistra-funktionen f�r att kopiera och flytta 
    textstycken eller kolumner inom ett dokument eller fr�n ett 
    dokument till ett annat

^   flytta mark�ren och v�lja ut text tecken-, ord-, rad-, menings-, 
    stycke- eller sidvis

^   s�ka och ers�tta text

^   anv�nda genomg�ende s�kning och ers�ttning, som s�ker upp och 
    ers�tter en angiven fras genom ett helt dokument

^   s�ka efter en speciell sida i ett sidindelat dokument

^   sidindela ett dokument manuellt

^   skapa fotnoter automatiskt, s�ka efter fotnoterna samt redigera 
    dem (�ven utf�ra stavningskontroll)

^   skapa kommandoblock varifr�n nyckelord skickas till skrivaren via 
    skrivartabellen; skrivaren f�ljer sedan de speciella 
    instruktioner som nyckelorden st�r f�r

^   skapa ett indrag vid radbrytning, utan att beh�va ta fram linjalen


Textbehandlingssystemet WPS-PLUS/VMS har en redigeringsmeny varifr�n
anv�ndaren kan:

^   l�ta sidindela ett dokument automatiskt

^   v�lja bred sk�rm, f�r att l�ttare kunna arbeta med bred linjal

^   v�lja frasminnes- och standarddokument, varifr�n ord, fraser och 
    textstycken kan h�mtas med ett f�tal tangenttryckningar

^   anv�nda bakgrundsmarkering och �ndringsmarkering f�r att markera 
    text som tagits bort respektive lagts till i ett dokument


I WPS-PLUS/VMS finns ocks� kommandon som g�r det m�jligt att:

^   placera sidhuvuden och sidf�tter �verst respektive nederst p� 
    varje sida i ett dokument

^   skriva ut tabeller med flera kolumner

^   numrera sidorna automatiskt

^   g�ra ber�kningar med addition, subtraktion, multiplikation och 
    division

^   l�gga in kommentarer som visas p� sk�rmen men inte p� utskriften


Spr�khj�lp (engelska, franska, tyska)

Som till�ggsprogramvara finns betydelseordlista och synonymordlista p� 
engelska samt stavningskontroll p� engelska, franska eller tyska. F�r 
mer information om spr�khj�lp, se under TILL�GGSPROGRAMVARA i denna 
produkts System Support Addendum (SSA 27.94.01A)


Listbehandling, sortering och listmatematik

Anv�ndaren kan skapa standardiserade brev eller rapporter genom att
f�rst skapa f�ljande:

^   ett malldokument, t ex ett brev eller en rapport

^   ett registerdokument som inneh�ller poster med variabel text, t ex 
    namn och adresser

^   ett urvalsdokument som anger vilka poster som ska behandlas

^   ett slutdokument som ska inneh�lla resultatet av listbehandlingen, 
    dvs de standardiserade breven eller rapporterna, klara att skrivas 
    ut


Listbehandlingsfunktionens egenskaper

^   Ett registerdokument inneh�ller flera poster, och en post kan i 
    sin tur ha flera f�lt av varierande l�ngd.

^   Anv�ndaren kan sortera p� ett eller flera f�lt i ett 
    registerdokument, i stigande eller fallande ordning.

^   Med hj�lp av listmatematiken kan anv�ndaren f� ber�kningar utf�rda 
    automatiskt under listbehandlingen.

^   Kolumnrubriker och undertexter (med t ex slutsummeringar) kan 
    skrivas ut i tabeller.

^   Under en listbehandling eller sortering visas l�pande information 
    p� sk�rmen om hur m�nga poster som har behandlats.


Tv�dimensionell grafik

I den tv�dimensionella grafiken (TDG) kan anv�ndaren skapa diagram, 
ekvationer, streckteckningar, tabeller, matriser och VT100:s 
teckenupps�ttning. Grafikfunktionen g�r det m�jligt att:

^   anv�nda den tv�dimensionella grafiken under redigering, genom att 
    trycka p� <GULD> <GRAFIK>

^   skapa och skriva ut diagram som kan vara upp till 132 tecken breda 
    och 22 rader l�nga; diagrammen skrivs ut med den inst�llning som 
    finns p� linjalen ovanf�r dem

^   placera in ett diagram var som helst i ett dokument, �ven p� en 
    sida d�r det finns text, och skriva ut dokumentet p� en underst�dd 
    skrivare

^   placera in en ekvation i flera niv�er var som helst i ett 
    dokument, �ven p� en sida d�r det finns text, och skriva ut 
    dokumentet p� en underst�dd skrivare

^   flytta mark�ren f�rbi ett diagram vid redigering

^   se till att sidbrytning inte g�rs i diagrammen vid automatisk 
    sidindelning

^   rita vertikala och horisontella linjer

^   skriva in text vertikalt eller horisontellt

^   skapa boxar

^   v�lja ut ett rektangul�rt arbetsomr�de, som sedan kan kopieras, 
    flyttas, raderas eller framh�vas


Integrerad arkivering

I WPS-PLUS/VMS finns ett integrerat arkiveringssystem med ett arkiv
f�r varje anv�ndare. Arkivets egenskaper:

^   Dokumentnamnen kan best� av upp till 70 tecken och mappnamnen av 
    upp till 30 tecken.

^   Anv�ndaren kan ge varje dokument ett eller flera nyckelord.

^   Dokumenten kan s�kas utifr�n mappnamn, dokumentnamn, nyckelord 
    eller dokumentnummer.

^   Dokumenten kan v�ljas ut fr�n en inneh�llsf�rteckning som visar 
    dokument- och mappnamn.

^   Dokument kan tas bort, kopieras och l�ggas i flera mappar.

^   Dokument som tagits bort f�rvaras i mappen PAPPERSKORG tills denna 
    mapp t�ms av anv�ndaren.


Hj�lpfunktionen

I WPS-PLUS/VMS finns en omfattande hj�lpfunktion d�r anv�ndaren kan f�
information om:

^   tangentbordets utseende och tangenternas funktion 

^   aktuell redigeringsfunktion

^   aktuellt f�lt i ett formul�r

^   ett angivet menyval


Anv�ndardefinierade funktioner

Med hj�lp av anv�ndardefinierade funktioner (ADF:er) kan anv�ndaren
automatisera tangentsekvenser som anv�nds ofta. Anv�ndaren kan:

^   anropa en ADF fr�n vilken meny eller vilket formul�r som helst 
    eller vid redigering

^   ta fram en lista �ver sina ADF:er genom att anv�nda en 
    GULD-funktion

^   ge ADF:en ett namn p� upp till nio tecken

^   spara ett obegr�nsat antal ADF:er

^   spara ett obegr�nsat antal tangenttryckningar i varje ADF

^   skapa, redigera, skriva ut och ta bort ADF:er


Utskrift

I utskriftsfunktionen finns m�jlighet f�r anv�ndaren att spara
utskriftsinst�llningar och best�mma:

^   sidstorlek och sidans utseende (t ex v�nster- och h�germarginal, 
    �vre och undre marginal)

^   spaltavst�nd vid utskrift i spalter

^   antal tecken per tum

^   val av papperskassett f�r pappersinmatning f�r skrivare som 
    underst�der detta


Datorst�dd utbildning

I WPS-PLUS/VMS finns omfattande datorst�dd utbildning, som passar f�r 
nyb�rjaren s�v�l som den n�got mer vana anv�ndaren.


MASKINVARUKRAV

Konfiguration med VAX, MicroVAX eller VAXstation s�som anges i System 
Support Addendum (SSA BH-MX15A-TE).


PROGRAMVARUKRAV*

Operativsystemet VMS

VAX FMS Runtime Only System 

Programvaran VAX Scriptprinter fordras f�r att filer av annat format 
�n Postscript skall kunna skrivas ut p� en Scriptprinter.

Observera: VAX FMS Runtime Only System fordras f�r att WPS-PLUS/VMS 
skall kunna anv�ndas. Kunden m�ste k�pa en programvarulicens f�r VAX 
FMS Runtime Only System s�v�l som en upps�ttning VAX FMS Runtime Only 
media.

* Se System Support Addendum (SSA BH-MX15A-TE) f�r information om 
programvaruversioner, programvarukrav och till�ggsprogramvara.


ORDERINFORMATION

Programvarulicenser: QL-AAMM*-**
Programvarumedia: QA-AAMM*-**
Programvarudokumentation: QA-AAMMA-GZ
Programvaruservice: QT-AAMM*-**

* Asterisken betecknar varierande enheter. F�r ytterligare information 
om tillg�ngliga licenser, media och service, se g�llande prislista.


LICENSVILLKOR

Denna programvara levereras under de licensvillkor som anges i 
Digitals Allm�nna leveransvillkor. F�r ytterligare information om 
Digitals licensvillkor, kontakta n�rmaste Digitalkontor.


LICENSE MANAGEMENT FACILITY (LMF)

Denna programvara underst�der VMS License Management Facility. 
Licensenheter f�r denna produkt k�ps per centralenhet.

F�r ytterligare information om License Management Facility, se  
Software Product Description f�r operativsystemet VMS (SPD 25.01.xx) 
eller handboken f�r License Management Facility i 
dokumentationsupps�ttningen f�r operativsystemet VMS.

F�r ytterligare information om Digitals licensvillkor, kontakta 
n�rmaste Digitalkontor.


PROGRAMVARUSERVICE

Ett flertal servicealternativ finns tillg�ngliga hos Digital. Kontakta 
Digital f�r n�rmare information.


PROGRAMVARUGARANTI

Denna programvaruprodukt omfattas av en garanti enligt vad som anges i 
garantibilagan till denna SPD. Garantin l�mnas av Digital vid k�p av 
en licens f�r produkten.


WPS-PLUS �r ett varum�rke tillh�rande Digital Equipment Corporation. 
Postscript �r ett varum�rke tillh�rande Adobe Systems Inc.


September 1989
BH-JP72B-TE
