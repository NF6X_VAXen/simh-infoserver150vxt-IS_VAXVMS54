 

















             Installation Guide


             Order No. AA-LN26B-TE


             SUPERSESSION/UPDATE INFORMATION:  This is a revised man-
                                               ual


             OPERATING SYSTEM AND VERSION:     VAX/VMS V5.0


             SOFTWARE VERSION:                 VAX Wide Area Network

                                               Device Drivers V1.1












             DIGITAL             DIGITAL             DIGITAL

 






                                       First Printing, July 1988
                                            Revised, January 1989




             The information in this document is subject to change
             without notice and should not be construed as a com-
             mitment by Digital Equipment Corporation. Digital Equipment
             Corporation assumes no responsibility for any errors
             that may appear in this document.

             The software described in this document is furnished
             under a license and may be used or copied only in ac-
             cordance with the terms of such license.

             No responsibility is assumed for the use or reliabil-
             ity of software on equipment that is not supplied by
             Digital Equipment Corporation or its affiliated com-
             panies.

             Copyright � 1989 by Digital Equipment Corporation
                            All Rights Reserved
                               Printed in UK

             The READER'S COMMENTS form on the last page of this doc-
             ument requests the user's critical evaluation to as-
             sist in preparing future documentation.


             The following are trademarks of Digital Equipment Corporation:

             DEC             MASSBUS       UNIBUS
             DEC/MMS         MicroVAX      VAX
             DECnet          Packetnet     VAXcluster
             DECsystem-10    PDP           VMS
             DECSYSTEM-20    Q-bus         VT
             DECUS           Q22-bus
             DECwriter       RSTS
             DIBOL           RSX                                           DIGITAL                                           DIGITAL                                           DIGITAL


             This manual was produced by the Wide Area Communications
             Environment group in Reading, England.

 















          _______________________________________________________

                                                         Contents                                                         Contents                                                         Contents



          How to Use This Manual


          1   Installation Preparations

              1.1      Introduction ......................... 1-1

              1.2      Preparation .......................... 1-2

          2   How to Install the VAX WAN Device Drivers V1.1 Kit



          A  Example Installation Dialog



          B  Installed Device Driver Files









                                                              iii

 






          Tables

              B-1    Installed Device Driver Files .......... B-1






































          iv

 













          _______________________________________________________

                                           How to Use This Manual                                           How to Use This Manual                                           How to Use This Manual






          Manual Objectives          Manual Objectives          Manual Objectives

             This manual describes how to install VAX WAN Device Drivers
             V1.1 on a VAX/VMS system, and is intended for anyone
             conducting the installation.


          Associated Documents          Associated Documents          Associated Documents

             For reference information, see the following documents:

              _                The two companion volumes contained in this binder:

                 -                   ___ ____ ____ _______ ______ _______ ____ ______________                   VAX Wide Area Network Device Drivers V1.1 Specifications

                 -                   ___ ____ ____ _______ ______ _______ ____ ____________                   VAX Wide Area Network Device Drivers V1.1 Programmer's
                   _____                   Guide

              _                ___ _______ _______ ______                VMS Install Utility Manual

              _                ___ _______ __________ _______ ______                VMS License Management Utility Manual




                                                                v

 






          Conventions Used in this Manual          Conventions Used in this Manual          Conventions Used in this Manual


             ____________________________________________________
             ____________________________________________________             ConventionMeaning             ConventionMeaning             ConventionMeaning

             Red       Indicates text that you enter.
             print

                       Indicates what appears on the screen.             Plain
             Text

                       Indicates variable information.             ______             Italic

             <RET>     Indicates that you should press the
                       RETURN key.

             <CTRL/X>  Indicates that you should simultane-
                       ously press the CTRL key and the keyboard
             ____________________________________________________                       character shown.





















          vi

 













          _______________________________________________________                                                                1                                                                1                                                                1

                                        Installation Preparations                                        Installation Preparations                                        Installation Preparations







          1.1 Introduction          1.1 Introduction          1.1 Introduction

             This manual tells you how to install drivers from the
             VAX Wide Area Network (WAN) Device Drivers V1.1 kit.
             You must install the VAX WAN Device Drivers kit before
             installing any of the layered products which use the
             device drivers.

             You can install the kit on any system running VAX/VMS
             V5.0 or later. If you want to install the kit on a clus-
             ter, you can work from any node in that cluster.

             The full installation procedure has 2 steps:

              1.Preparation

              2.Installation

             The rest of this manual tells you the commands required
             to begin these steps. You should then perform the ac-
             tions or enter the information requested by each prompt.
             This manual does not describe each prompt in turn, though
             Appendix A contains an example installation. You can
             get help at any time during the installation by enter-
             ing a question mark (?).

          Installation Preparations                           1-1

 









          1.2 Preparation          1.2 Preparation          1.2 Preparation

             The VAX WAN Device Drivers kit is supplied on the fol-
             lowing media:

              _                1 x RX33 diskette

              _                1 x TK50 tape cassette

              _                1 x 1600 bpi magnetic tape

             Before beginning the VAX WAN Device Drivers V1.1 kit
             installation, please complete all the steps described
             in this section.

              1.Log in to the SYSTEM account, or an account with sim-
                ilar privileges and quotas.

              2.Check that you have enough space available for the
                installation. Enter this command:

                  $ SHOW DEVICE SYS$SYSDEVICE

                Check the number of free blocks. A VAX WAN Device
                Drivers V1.1 kit needs a maximum of 2000 free blocks
                for successful installation, but the final amount
                of space used depends on the drivers you install.

             Installation takes up to 15 minutes depending on the
             number of drivers you need and the speed of the CPU on
             which you are performing the installation.

             During the installation, you will be asked whether you
             want to run the Installation Verification Procedure (IVP)
             or not. The IVP checks that the files have been copied
             correctly. If you want to check that the drivers and
             hardware are working properly, run the IVP for the lay-
             ered product.

          1-2                               Installation Guide

 













          _______________________________________________________                                                                2                                                                2                                                                2

               How to Install the VAX WAN Device Drivers V1.1 Kit               How to Install the VAX WAN Device Drivers V1.1 Kit               How to Install the VAX WAN Device Drivers V1.1 Kit




             The VAX WAN Device Drivers V1.1 kit supports the License
             Management Facility features provided by VMS. Before
             you attempt to install the kit, you must register a Product
             Authorization Key (PAK). To register the PAK (which is
             provided with the kit), enter the following command:

               $ @SYS$UPDATE:VMSLICENSE

             See the                                       for in-                     ___ _______ __________ _______ ______                     VMS License Management Utility Manual
             formation about this command procedure and the manage-
             ment features of LMF.

              1.Begin the installation by entering the command:

                                                  _________                                                  kitdevice                  $ @SYS$UPDATE:VMSINSTAL SYNC011          : OPTIONS N

                          is the device from which you are installing                _________                kitdevice
                the VAX WAN Device Drivers V1.1 kit, such as MUA1:.
                OPTIONS N lets you obtain the release notes, and you
                are given the option of stopping the installation
                to read them. If you omit this portion of the com-
                mand, the installation proceeds without reference
                to the release notes.

              2.                Read through the release notes before you continue                Read through the release notes before you continue                Read through the release notes before you continue
                with the installation.                with the installation.                with the installation.

          How to Install the VAX WAN Device Drivers V1.1 Kit  2-1

 






              3.Follow the directions shown on your terminal. For
                reference, Appendix A contains an example dialog.
                Type a question mark (?) at any prompt if you are
                unsure about the information required.

              4.The installation procedure asks you to select de-
                vices from a menu. Enter as many as you require, sep-
                arating the menu numbers with commas. When you have
                completed the entry, the menu is redisplayed and se-
                lected devices are marked with an asterisk. If you
                make a mistake, select the exit option and start again.

              5.After you have installed the drivers and any lay-
                ered products that use the drivers, you must reboot
                your system. On a VAXcluster, reboot each node in
                the cluster that has a synchronous communications
                device for which you have installed a driver.

          After the installation, SYS$TEST contains an Installation
          Verification Procedure (SYNC$IVP.COM), even if you did not
          choose to run the IVP during the installation. You may use
          SYNC$IVP.COM at any time to check that your drivers are
          still present on the system by entering the command:

               $ @SYS$TEST:SYNC$IVP
















          2-2                               Installation Guide

 













          _______________________________________________________                                                                A                                                                A                                                                A

                                      Example Installation Dialog                                      Example Installation Dialog                                      Example Installation Dialog




             The following is an example VAX WAN Device Drivers kit
             installation:



          $ @sys$update:vmsinstal sync011 jupitr$dua2: options n

           VAX/VMS Software Product Installation Procedure V5.0-2

          It is 11-OCT-1988 at 12:16.
          Enter a question mark (?) at any time for help.

          Please mount the first volume of the set on  JUPITR$DUA2:.
          * Are you ready? y
          %MOUNT-I-MOUNTED, SYNC01       mounted on _JUPITR$DUA2:
          The following products will be processed:
            SYNC V1.1

                 Beginning installation of SYNC V1.1 at 12:17

          %VMSINSTAL-I-RESTORE, Restoring product saveset A ...

              Release Notes Options:

                  1.  Display release notes
                  2.  Print release notes
                  3.  Both 1 and 2
                  4.  Copy release notes to SYS$HELP
                  5.  Do not display, print or copy release notes

          Example Installation Dialog                         A-1

 






          * Select option [2]: 4
          * Do you want to continue the installation? yes
          %VMSINSTAL-I-RELMOVED , The product's release notes have been successfully
            moved to SYS$HELP.

                 VAX WAN Device Drivers Kit Installation Procedure V1.1

                  Product:      WAN-DEVICE-DRIVERS
                  Producer:     DEC
                  Version:      1.1
                  Release Date: 30-JUN-1988

          * Does this product have an authorization key registered and loaded? yes
          * Do you want to run the IVP after the installation [YES]?

          *       Is this machine a member of a VAXcluster [YES]?

          *       Do you have any MicroVAXes in your VAXcluster [YES]?

                  Please select device type from menu. (1..13)
                  Enter numbers for devices singly or in a list separated by
                  commas. Finish with the number for EXIT and press <RET>.
                  You can correct mistakes at a later prompt.

          Valid devices for MicroVAX:     1)      DPV11
                                          2)      DMV11
                                          3)      KMV1A
                                          4)      DSV11
                                          5)      DST32

          Valid devices for VAXBI:        6)      DMB32
                                          7)      DSB32

          Valid devices for UNIBUS:       8)      DUP11
                                          9)      DMF32
                                          10)     KMS11-BD
                                          11)     KMS1P
                                          12)     DMC11/DMR11

          To install chosen drivers:      13)     EXIT

          * NUMBER(s): 4,5,6,7

                                          DSV11 selected.
                                          DST32 selected.
                                          DMB32 selected.
                                          DSB32 selected.

                  Please select device type from menu. (1..13)
                  Enter numbers for devices singly or in a list separated by
                  commas. Finish with the number for EXIT and press <RET>.
                  You can correct mistakes at a later prompt.

          A-2                                Installation Guide

 






          Valid devices for MicroVAX:     1)      DPV11
                                          2)      DMV11
                                          3)      KMV1A
                                          4)      DSV11 *
                                          5)      DST32 *

          Valid devices for VAXBI:        6)      DMB32 *
                                          7)      DSB32 *

          Valid devices for UNIBUS:       8)      DUP11
                                          9)      DMF32
                                          10)     KMS11-BD
                                          11)     KMS1P
                                          12)     DMC11/DMR11

          To install chosen drivers:      13)     EXIT

          * NUMBER(s): 9,13

                                          DMF32 selected.
                                          Exit  selected.

          *       Any changes required [NO]?

          * Do you want to purge files replaced by this installation [YES]? no

          *       To use the drivers you have installed, reboot the system.
                  Either VMS or the layered product will automatically load the
                  driver(s) at system boot-time.

          *       In a VAXcluster environment, the driver image is installed
                  into a cluster-wide directory.  You must reboot all nodes in
                  the VAXcluster with these devices.

          *       No more questions will be asked during this installation.
                  Depending on the number of drivers selected, the installation
                  can take up to 15 minutes.

          *       Installed device(s) are:    DSV11
                                              DST32
                                              DMB32
                                              DSB32
                                              DMF32

          %VMSINSTAL-I-RESTORE, Restoring product saveset B ...
          %VMSINSTAL-I-MOVEFILES, Files will now be moved to their target directories...

          Beginning VAX WAN Device Drivers V1.1 Installation Verification Procedure

          Example Installation Dialog                         A-3

 






          %ANALYZE-I-ERRORS, $1$DUA1:[SYS2.SYSCOMMON.SYS$LDR]SJDRIVER.EXE;121  0 errors
          %ANALYZE-I-ERRORS, $1$DUA1:[SYS2.SYSCOMMON.SYS$LDR]ZSDRIVER.EXE;7    0 errors
          %ANALYZE-I-ERRORS, $1$DUA1:[SYS2.SYSCOMMON.SYS$LDR]SIDRIVER.EXE;18   0 errors
          %ANALYZE-I-ERRORS, $1$DUA1:[SYS2.SYSCOMMON.SYS$LDR]SLDRIVER.EXE;3    0 errors
          %ANALYZE-I-ERRORS, $1$DUA1:[SYS2.SYSCOMMON.SYS$LDR]XGDRIVER.EXE;2    0 errors

          The IVP for VAX WAN Device Drivers V1.1 was successful

                  Installation of SYNC V1.1 completed at 12:28

                  VMSINSTAL procedure done at 12:29

          $ logout
            SYSTEM       logged out at 11-OCT-1988 13:31:48.26



























          A-4                                Installation Guide

 













          _______________________________________________________                                                                B                                                                B                                                                B

                                    Installed Device Driver Files                                    Installed Device Driver Files                                    Installed Device Driver Files





             ____________________________________________________             Table B-1  Installed Device Driver Files             Table B-1  Installed Device Driver Files             Table B-1  Installed Device Driver Files

             ____________________________________________________             Filename         Location              Description             Filename         Location              Description             Filename         Location              Description

             PSI$KMVPRO.EXE   SYS$LOADABLE_IMAGES   KMV1A soft-
                                                    load mi-
                                                    crocode
                                                    image

             PSI$KMXDIA.EXE   SYS$LOADABLE_IMAGES   KMS11-BD
                                                    diagnostic
                                                    microcode
                                                    image

             PSI$KMXPDM.EXE   SYS$LOADABLE_IMAGES   KMS11-BD
                                                    softloaded
                                                    microcode
                                                    image

             PSI$KMXPRO.EXE   SYS$LOADABLE_IMAGES   KMS11-BD
                                                    softloaded
                                                    microcode
                                                    image



          Installed Device Driver Files                       B-1

 






             ____________________________________________________             Table B-1 (Cont.)  Installed Device Driver Files             Table B-1 (Cont.)  Installed Device Driver Files             Table B-1 (Cont.)  Installed Device Driver Files

             ____________________________________________________             Filename         Location              Description             Filename         Location              Description             Filename         Location              Description

             PSI$KMYDIA.EXE   SYS$LOADABLE_IMAGES   KMS1P di-
                                                    agnostic
                                                    microcode
                                                    image

             PSI$KMYPDM.EXE   SYS$LOADABLE_IMAGES   KMS1P soft-
                                                    loaded mi-
                                                    crocode
                                                    image

             PSI$KMYPRO.EXE   SYS$LOADABLE_IMAGES   KMS1P soft-
                                                    loaded mi-
                                                    crocode
                                                    image

             SIDRIVER.EXE     SYS$LOADABLE_IMAGES   DMB32 sync.
                                                    port driver
                                                    image

             SIDRIVER.STB     SYS$LOADABLE_IMAGES   DMB32 sync.
                                                    port driver
                                                    symbol table

             SJDRIVER.EXE     SYS$LOADABLE_IMAGES   DSV11 driver
                                                    image

             SJDRIVER.ULD     SYS$LOADABLE_IMAGES   DSV11 soft-
                                                    loaded mi-
                                                    crocode
                                                    image

             SLDRIVER.EXE     SYS$LOADABLE_IMAGES   DSB32 driver
                                                    image




          B-2                                Installation Guide

 






             ____________________________________________________             Table B-1 (Cont.)  Installed Device Driver Files             Table B-1 (Cont.)  Installed Device Driver Files             Table B-1 (Cont.)  Installed Device Driver Files

             ____________________________________________________             Filename         Location              Description             Filename         Location              Description             Filename         Location              Description

             SLDRIVER.ULD     SYS$LOADABLE_IMAGES   DSB32 soft-
                                                    loaded mi-
                                                    crocode
                                                    image

             UEDRIVER.EXE     SYS$LOADABLE_IMAGES   DPV11 driver
                                                    image for
                                                    VAX PSI

             UFDRIVER.EXE     SYS$LOADABLE_IMAGES   DPV11 driver
                                                    image
                                                    for VAX
                                                    2780/3780

             UGDRIVER.EXE     SYS$LOADABLE_IMAGES   DPV11 driver
                                                    image for
                                                    VAX 3271

             UHDRIVER.EXE     SYS$LOADABLE_IMAGES   DPV11 driver
                                                    image for
                                                    VMS/SNA

             UVDRIVER.EXE     SYS$LOADABLE_IMAGES   KMV1A driver
                                                    image

             XDDRIVER.EXE     SYS$LOADABLE_IMAGES   DMV11 driver
                                                    image

             XGDRIVER.EXE     SYS$LOADABLE_IMAGES   DMF32 sync.
                                                    port driver
                                                    image






          Installed Device Driver Files                       B-3

 






             ____________________________________________________             Table B-1 (Cont.)  Installed Device Driver Files             Table B-1 (Cont.)  Installed Device Driver Files             Table B-1 (Cont.)  Installed Device Driver Files

             ____________________________________________________             Filename         Location              Description             Filename         Location              Description             Filename         Location              Description

             XJDRIVER.EXE     SYS$LOADABLE_IMAGES   DUP11 driver
                                                    image
                                                    for VAX
                                                    2780/3780

             XKDRIVER.EXE     SYS$LOADABLE_IMAGES   DUP11 driver
                                                    image for
                                                    VAX 3271

             XMDRIVER.EXE     SYS$LOADABLE_IMAGES   DMC11/DMR11
                                                    driver image

             XNDRIVER.EXE     SYS$LOADABLE_IMAGES   KMS1P driver
                                                    image

             XSDRIVER.EXE     SYS$LOADABLE_IMAGES   KMS11-BD
                                                    driver image

             XWDRIVER.EXE     SYS$LOADABLE_IMAGES   DUP11 driver
                                                    image for
                                                    VAX PSI

             ZSDRIVER.EXE     SYS$LOADABLE_IMAGES   DST32 driver
                                                    image

             SYNC$IVP.COM     SYS$TEST              Installation
                                                    verification
                                                    procedure

             UETDMBS00.EXE    SYS$TEST              UETP for
                                                    DMB32 sync.
                                                    port





          B-4                                Installation Guide

 






             ____________________________________________________             Table B-1 (Cont.)  Installed Device Driver Files             Table B-1 (Cont.)  Installed Device Driver Files             Table B-1 (Cont.)  Installed Device Driver Files

             ____________________________________________________             Filename         Location              Description             Filename         Location              Description             Filename         Location              Description

             UETDSBS00.EXE    SYS$TEST              UETP for
                                                    DSB32

             UETDSVS00.EXE    SYS$TEST              UETP for
                                                    DSV11

             SI$LIB.MLB       SYS$LIBRARY           MACRO li-
                                                    brary file
                                                    containing
                                                    the $SIDEF
                                                    macro

             SI$DEF.SDL       SYS$LIBRARY           Software
                                                    design lan-
                                                    guage defi-
                                                    nitions of
                                                    SI$ symbols

             SI$DEF.R32       SYS$LIBRARY           BLISS li-
                                                    brary file
                                                    gener-
                                                    ated from
                                                    SI$LIB.R32
             ____________________________________________________                                                    file













          Installed Device Driver Files                       B-5
