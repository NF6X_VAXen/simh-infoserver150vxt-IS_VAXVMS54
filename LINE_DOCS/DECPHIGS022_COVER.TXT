 






          Read Before Installing or Using DEC PHIGS for VMS

          AV-PBHKB-TE

          Whether you are a new or continuing DEC PHIGS customer, please
          take time to read the following information about your product.

          Operating Systems

          The installation guide for this product contains information
          about installing DEC PHIGS Version 2.2 on a VMS system. The
          other documentation for this product contains ULTRIX and VMS
          operating system information.

          Installation and Release Notes Information

          The release notes for the latest installed version of DEC
          PHIGS contain useful information such as a summary of tech-
          nical changes, new features, differences, known problems, cor-
          rected errors, performance enhancements, documentation errors,
          restrictions, and incompatibilities.

          VMS System Information

          Before you install DEC PHIGS for VMS Version 2.2, you must
          register the product using the Product Authorization Key (PAK)
          provided in the kit. See the VMS License Management Utility
          Manual for registration instructions.

          You can read the release notes following the installation of DEC
          PHIGS on a VMS system by typing:

               $ HELP PHIGS VMS RELEASE_NOTES

          Note that you must have VMS Version 5.3-1 or higher for DEC
          PHIGS Version 2.2. Previous versions of DEC PHIGS do not need to
          be installed before this version.

 






          Contents of This Kit

          o  Indented Bill Report (BIL) and Bill of Materials (BOM)

             Please read the BIL and BOM enclosed in this kit, and check
             to see that all items listed there are actually in your kit.
             If your kit is damaged, or if any items are missing, call
             your Digital representative.

              � Digital Equipment Corporation. 1990. All Rights Reserved.





























                                          2

 






          o  Media

             If you ordered media, you will find the media and the In-
             stalling DEC PHIGS for VMS manual in this kit. Consult the
             Installing DEC PHIGS for VMS manual for information about
             installing DEC PHIGS for ULTRIX on your system.

          o  Documentation

             New DEC PHIGS customers receive all of the following documen-
             tation. Update customers receive only new documents. Run-time
             only customers receive only the Installing DEC PHIGS for VMS
             manual.

                DEC PHIGS Reference Manual
                DEC PHIGS C Binding Reference Manual
                DEC PHIGS FORTRAN Binding Reference Manual
                DEC PHIGS PHIGS$ Binding Reference Manual
                DEC PHIGS Device Specifics Reference Manual
                Installing DEC PHIGS for VMS

          o  Product Authorization Key (PAK) for VMS

             The PAK contains information necessary to register DEC PHIGS
             before you install it on a VMS system.

          o  Software Product Description (SPD)

             Your SPD provides an overview of the DEC PHIGS kit and its
             features.

          o  Software Performance Report (SPR)

             Use this form to report any problems with DEC PHIGS, provided
             you have purchased warranty services.

          o  Software Update Survey Card

             By completing this card after you install DEC PHIGS, you
             provide valuable feedback to Digital on installation quality.
             Your feedback helps us to understand customer needs and
             improve our products.

                                          3

 






          A Final Note

          Digital prides itself on responding to customer needs. In order
          to continue serving you, we need your comments. Each manual
          contains preaddressed, postage-paid Reader's Comments forms
          at the back. If you find errors in a manual or want to make
          comments about it, please fill out one of these forms and send
          it to us.































                                          4

 






          DECUS - The Benefits of Belonging

          DECUS, the Digital Equipment Computer Users Society, is one
          of the largest and most active user groups in the computer
          industry. Since it was founded in 1961, DECUS has grown in-
          ternationally, with more than 70,000 members worldwide.

          DECUS, a not-for-profit organization, is actively directed by
          individuals like yourself who have purchased, leased, or ordered
          a Digital computer, who currently use a Digital computer, or who
          have a genuine interest in DECUS.

          Membership is free. Benefits of belonging to DECUS include the
          following:

          Interchange of information among users and with Digital person-
          nel

          DECUS maintains contact between Digital and users primarily
          through Special Interest Groups (SIGs). SIGs are formed around
          areas of common interest, such as hardware, operating systems,
          languages, applications, and marketing. SIGs participate in
          a newsletter and they offer effective and focused exchanges
          of information between Digital and users. In addition, SIGs
          represent users on ANSI committees responsible for standards.

          Local area activities

          DECUS members participate on a local level through one of many
          Local User Groups (LUGs). Digital often assigns representatives
          to keep LUGs informed of new products and available services.

          Symposia

          DECUS holds symposia around the world several times each year.
          Intensive days of meetings consist of workshops, panels, tutori-
          als, and formal paper presentations.

          SIGs also sponsor seminars at which both Digital and users give
          presentations on topics such as strategies, products, problems,
          and solutions that are of interest to Digital users.

                                          5

 






          Software at nominal charge

          DECUS maintains a library of approximately 2000 active pro-
          grams written and submitted by users. These programs include
          compilers, editors, numerical functions, utilities, and games.
          Individual SIGs also maintain separate libraries of programs
          specific to each Digital software product. Programs from the li-
          braries are available to any DECUS member. You can get ordering
          information from your DECUS office.

          Feedback to Digital

          DECUS members have significant input into Digital's hardware
          and software designs. Digital managers and developers frequently
          participate in symposia and SIG/LUG meetings to obtain feedback
          on current products and to get direction for future products.























                                          6

 






          Publications

          DECUS produces a newsletter, symposia proceedings, SIG newslet-
          ters, and other publications containing useful technical ideas
          and information about industry trends. (Please note that there
          are charges for some of these services.)

          The following is a list of the DECUS SIGs:

          APL                               MUMPS

          BUSINESS APPLICATIONS             NETWORKS

          DAARC                             THE NATIONAL LUG ORGANIZATION
                                            (NLO)

          DATATRIEVE                        OFFICE AUTOMATION

          DATA MANAGEMENT SYSTEMS           PERSONAL COMPUTER

          DATABASE MANAGEMENT SYSTEMS       REFERRED PAPERS JOURNAL

          EDUCATIONAL                       RSTS

          GRAPHICS APPLICATIONS             RSX

          HARDWARE AND MICRO                RT-11

          INTERACTIVE APPLICATIONS          SITE MANAGEMENT AND TRAINING
          SYSTEM

          LANGUAGES AND TOOLS               UNISIG

          LARGE SYSTEMS                     VAX SYSTEMS





                                          7

 






          To obtain a DECUS membership application form, write to the
          address closest to you:

          DECUS U.S.                        DECUS CANADA
          249 NORTHBORO ROAD                P.O. BOX 13000
          MARLBORO, MA 01752                KANATA, ONTARIO K2K2A6
          UNITED STATES                     CANADA

          DECUS EUROPE                      DECUS JAPAN
          12 AVENUE DES MORGINES            SUNSHINE 60, P.O. BOX 1135
          CASE POSTALE 510                  1-1 HIGASHI IKEBUKURO-CHOME
          CH-1213 PETIT-LANCY 1 GENEVA      TOSHIMA-KU
          SWITZERLAND                       TOKYO 170 JAPAN

          DECUS AUSTRALIA P/L               DECUS (for all other nations):
          (including New Zealand)           DIGITAL EQUIPMENT CORPORATION
          DIGITAL EQUIPMENT CORPORATION     NAGOG PARK
          PTY. LIMITED 100                  AK01
          P.O. BOX 384                      ACTON, MA 01720
          CHATSWOOD, NEW SOUTH WALES        UNITED STATES
          2067
          AUSTRALIA

          DECUS is supported and administered by Digital.















                                          8
