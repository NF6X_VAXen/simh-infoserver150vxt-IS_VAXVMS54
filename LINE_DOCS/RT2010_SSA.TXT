PRODUCT NAME:  DECrouter 250, Version 1.0              SSA 32.15.00-A

HARDWARE REQUIREMENTS 

Processors Supported (Load Host System)

For VMS Load Host Systems:

VAX:	  VAX 6000 Model 200 Series
	  VAX 6000 Model 300 Series
	  VAX 6000 Model 400 Series

	  VAX 8200, VAX 8250, VAX 8300, VAX 8350, VAX 8500, VAX 8530, VAX 8550, 
          VAX 8600, VAX 8650, VAX 8700, VAX 8800, VAX 8810, VAX 8820, VAX 8830, 
          VAX 8840, VAX 8842, VAX 8974, VAX 8978

	  VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

MicroVAX:	   MicroVAX II, MicroVAX 2000, MicroVAX 3100, MicroVAX 3300, 
                   MicroVAX 3400, MicroVAX 3500, MicroVAX 3600, MicroVAX 3800, 
                   MicroVAX 3900

VAXstation:	   VAXstation II, VAXstation 2000, VAXstation 3100 Series, 
                   VAXstation 3200, VAXstation 3500, VAXstation 3520, 
                   VAXstation 3540 
                                                      
VAXserver:	   VAXserver 3100, VAXserver 3300, VAXserver 3400, VAXserver 
                   3500, VAXserver 3600, VAXserver 3602, VAXserver 3800, 
                   VAXserver 3900, VAXserver 6000-210, VAXserver 6000-310, 
                   VAXserver 6000-410, VAXserver 6000-420

Processors Not Supported 

MicroVAX I, VAXstation I, VAX-11/725, VAX-11/782, VAXstation 8000

For ULTRIX Load Host Systems:

VAX-Based Processors:

VAX:	  VAX 6000-410, VAX 6000-420
   
	  VAX 6210, VAX 6220, VAX 6310, VAX 6320, VAX 8200, VAX 8250, VAX 
          8300, VAX 8350, VAX 8500, VAX 8530, VAX 8550, VAX 8600, VAX 
          8650, VAX 8700, VAX 8800, VAX 8810, VAX 8820

	  VAX-11/750, VAX-11/780, VAX-11/785

MicroVAX:	   MicroVAX II, MicroVAX 2000, MicroVAX 3100, MicroVAX 
                   3300, MicroVAX 3400, MicroVAX 3500, MicroVAX 3600, 
                   MicroVAX 3800, MicroVAX 3900

VAXstation:	   VAXstation II, VAXstation 2000, VAXstation 3100, 
                   VAXstation 3200, VAXstation 3500, VAXstation 3520

VAXserver:	   VAXserver 100, VAXserver 3100, VAXserver 3300, 
                   VAXserver 3400, VAXserver 3500, VAXserver 3600, 
                   VAXserver 3602, VAXserver 3800, VAXserver 3900, 
                   VAXserver 6000-210, VAXserver 6000-310, VAXserver 
                   6000-410, VAXserver 6000-420

MS-DOS Host Systems:

Personal Computers

VAXmate, IBM 5150, IBM 5160, IBM 5162, IBM 5170, IBM 8530, IBM 8550,  
IBM 8560, COMPAQ DESKPRO, COMPAQ DESKPRO 286, Olivetti M24, Olivetti 
M28, Olivetti M240, Olivetti M280

For MS-DOS load host support, refer to the Configuration Chart for 
DECnet-DOS (SPD 50.15.xx) for a list of supported configurations.

Other Hardware Required

The DECrouter 250 software runs only on the DECrouter 250 (DSRVR) 
hardware available as follows:

^ DSRVR-AA for use with 120V AC power supplies

^ DSRVR-AB for use with 240V AC power supplies

It should be noted that two cables are shipped with the hardware units 
above.  The two cables are the BC23V-02 (allows for interchange circuits 
for the high speed port for V.28/EIA-232-D) and the BS23V-02 (allows for 
interchange circuits for the high speed port for EIA-232-C).

The DECrouter 250 has a selectable ThinWire/ThickWire connector.

The DECrouter 250 uses adapter cables to provide the necessary 
interchange circuits at the appropriate electrical interface types for 
the two high speed ports.  One of these cables is needed for each line 
that is used.  The following table shows the cable numbers for each of 
the electrical interface types:

                  Adapter Cables


Electrical Interface Type	      Adapter Cable  

V.11/EIA-422/V.36		      BC19B-02
V.10/EIA-423			      BC19E-02
V.35				      BC19F-02

Within the UK, an additional adapter cable is needed to allow connection 
to the British Telecom Kilostream as listed:

^ BC22X-02

Block/Disk Space Requirements - VMS

Block Space Requirements (Block Cluster Size = 1):

Disk space required for installation:			     1,700 blocks
							     (850K bytes)

Disk space required for use (permanent):		     1,630 blocks
							     (815K bytes)

Block Space Requirements  - ULTRIX

Disk Space Requirements for VAX-Based Systems:

Disk Space required for installation:

Root file system: / <0> Kbytes

Other file systems:
     /usr <610> Kbytes
     /var <0> Kbytes

Disk Space required for use (permanent):

Root file system: / <0> Kbytes

Other file systems:
     /usr <600> Kbytes
     /var <0> Kbytes

Disk Space Requirements  - DOS

Disk space required for installation:			     710,00 bytes

These counts refer to the disk space required on the system disk.  The 
sizes are approximate; actual sizes may vary depending on the user's 
system environment, configuration, and software options.

OPTIONAL HARDWARE

None

CLUSTER ENVIRONMENT (VMS only)

This layered product is fully supported when installed on any valid and 
licensed VAXcluster* configuration without restrictions.  The HARDWARE 
REQUIREMENTS sections of this product's Software Product Description and 
System Support Addendum detail any specific hardware required by this 
product. 

* V5.x VAXcluster configurations are fully described in the VAXcluster 
  Software Product Description (29.768.xx) and include CI, 
  Ethernet/802.3, and Mixed Interconnect configurations. 

SOFTWARE REQUIREMENTS 

For each VAX System acting as a load host or dump receiver: 

^ VMS Operating System V5.0 - V5.3 running DECnet-VAX V5.0 - V5.3 (either 
  end-node or full-function)

or

^ ULTRIX-32 Operating System V3.0 - V3.1 running DECnet-ULTRIX V3.0 - 
  V3.1 (including the ULTBASE, ULTMOP and DNUBASE subsets)

For each MS-DOS System acting as a load host or dump receiver:

^ MS-DOS Operating System V3.1 - V3.1  or 

^ IBM PC Disk Operating System V3.20 - V3.30 or 

^ COMPAQ MS-DOS V3.30 or 

^ Olivetti MS-DOS V3.2

^ DECnet-DOS V2.1 - V3.0

VMS Tailoring (VMS only)

For VMS V5.x systems, the following VMS classes are required for full 
functionality of this layered product: 

^ VMS Required Saveset

^ Network Support

For more information on VMS classes and tailoring, refer to the VMS 
Operating System Software Product Description (SPD 25.01.xx).

OPTIONAL SOFTWARE

None 

GROWTH CONSIDERATIONS

The minimum hardware/software requirements for future version of this 
product may differ from the minimum requirements for the current version.

DISTRIBUTION MEDIA

For VAX systems, running VMS or ULTRIX, acting as a load host:

Tape:	 TK50 Streaming Tape, 9-track 1600 BPI Magtape

For VMS Load Host Systems, this product is also available as part of the 
VMS Consolidated Software Distribution on CDROM.

For DOS systems, running MS-DOS, acting as a load host:

Disk:   RX24 Floppy Diskette, RX31 Floppy Diskette 

ORDERING INFORMATION

For VMS Operating Systems:

Software License: QL-YG6A9-**
Software Media:  QA-YG6AA-**
Software Documentation: QA-YG6AA-GZ
Software Product Services: QT-YG6*-**

For ULTRIX-32 Operating Systems on VAX or RISC:

Software License: QL-YG6A9-**
Software Media: QA-YG6AB-**
Software Documentation: QA-YG6AB-GZ
Software Product Services: QT-YG6*-**

For MS-DOS Operating Systems or PC-DOS Operating Systems:

Software License: QL-YG6A9-**
Software Media: QA-YG6AC-**
Software Documentation: QA-YG6AC-GZ
Software Product Services: QT-YG6*-**

* Denotes variant fields.  For additional information on available 
  licenses, services and media, refer to the appropriate price book.

The above information is valid at the time of release.  Please contact 
your local Digital office for the most up-to-date information.


IBM is a registered trademark of IBM Corporation.
COMPAQ is a registered trademark of COMPAQ Computer Corporation.
MS-DOS is a registered trademark of Microsoft Corporation.
Smart Modem 2400 is a trademark of Hayes Microcomputer Products.

The DIGITAL Logo is a registered trademark of Digital Equipment Corporation. 
DECnet, DECrouter, ULTRIX-32, VAX, VMS, MicroVAX, VAXstation and VAXserver 
are trademarks of Digital Equipment Corporation.

March 1990
AE-PBAHA-TK
