PRODUCT NAME:  VAX TDMS, Version 1.9                 SSA 25.71.15-A
               (Terminal Data Management System)

HARDWARE REQUIREMENTS

Processors Supported

VAX:	  VAX 6000-Model 200 Series
	  VAX 6000-Model 300 Series
	  VAX 6000-Model 400 Series

	  VAX 8200, VAX 8250, VAX 8300, VAX 8350, VAX 8500, VAX 8530, VAX 8550, 
          VAX 8600, VAX 8650, VAX 8700, VAX 8800, VAX 8810, VAX 8820, VAX 8830, 
          VAX 8840, VAX 8842, VAX 8974, VAX 8978       

	  VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

 
MicroVAX:	 MicroVAX II, MicroVAX 2000, MicroVAX 3100, MicroVAX 3300, 
                 MicroVAX 3400, MicroVAX 3500, MicroVAX 3600, MicroVAX 3800, 
                 MicroVAX 3900
 
VAXstation:	 VAXstation II, VAXstation 2000, VAXstation 3100 Series, 
                 VAXstation 3200, VAXstation 3500, VAXstation 3520, 
                 VAXstation 3540
 
VAXserver:	 VAXserver 3100, VAXserver 3300, VAXserver 3400, VAXserver 
                 3500, VAXserver 3600, VAXserver 3602, VAXserver 3800, 
                 VAXserver 3900, VAXserver 6000-210, VAXserver 6000-220, 
                 VAXserver 6000-310, VAXserver 6000-320, VAXserver 6000-410, 
                 VAXserver 6000-420

Processors Not Supported: 
 
MicroVAX I, VAXstation I and VAXstation 8000 
 
Processor Restriction

A TK50 Tape Drive is required for MicroVAX 2000 and VAXstation 2000 
systems

Terminals

	    PC100 1		     VT125
	    PC278 1		     VT131 1
	    PC325 1		     VT220
	    PC350 1		     VT240
	    VT52  2		     VT241
	    VT100		     VT320 3
	    VT100 with AVO	     VT330 3
	    VT101		     VT340 3
	    VT102		     
 

1 Supported in VT102 mode only.

2 VT52 for application execution only. Run-time support for the VT52 is 
  generally the same as for the VT100. When a VT100-specific feature is    
  requested in a form definition, the feature will either be simulated or  
  ignored when the form is displayed on a VT52. Wide forms (132 columns)   
  and forms using scrolled areas cannot be displayed on a VT52.
 
3 VAX TDMS does not support the use of the 25th status line nor the  use   
  of the locator device on VT300-series terminals.

Terminal Emulators

Terminal emulators on VAXstations and personal computers are supported 
only to the extent that the emulator conforms to the VT100, VT220, or 
VT320 environment it is emulating.

Printers

VAX TDMS forms can be printed on any Digital printer. Video attributes 
are not printed and lines are drawn using the characters "-", "+", "|" 
rather than the line-drawing character set.                           

Block Space Requirements (Block Cluster Size = 1):

Disk space required for installation:

Full Development System:				    12,000 blocks
							     (6.1M bytes)

Run-time System:					     3,000 blocks
							     (1.5M bytes)

Disk space required for use (permanent):                  

Full Development System:						 

with samples						    5,000 blocks 
							     (2.5M bytes)

without samples						     2,500 blocks
							     (1.2M bytes)

Run-time System:					       500 blocks
							     (0.2 Mbytes)

These counts refer to the disk space required on the system disk. The 
sizes are approximations; actual sizes may vary depending on the users' 
system environment, configuration and software options selected. 

OPTIONAL HARDWARE

Any device supported by the prerequisite or optional software.

CLUSTER ENVIRONMENT

This layered product is fully supported when installed on any valid and 
licensed VAXcluster* configuration without restrictions.  The HARDWARE 
REQUIREMENTS section of this product's Software Product Description and 
System Support Addendum detail any special hardware required by this 
product.

* V5.x VAXcluster configurations are fully described in the VAXcluster 
  Software Product Description (29.78.xx) and include CI, Ethernet and 
  mixed Interconnect configurations.

SOFTWARE REQUIREMENTS 

Operating System:
									
VMS Operating System V5.0 - V5.2

Layered Products:

VAX CDD/Plus V4.1

VMS Tailoring

For VMS V5.x systems, the following VMS classes are required for full 
functionality of this layered product:

^ VMS Required Save Set

^ Network Support 

^ Programming Support 

^ Utilities 

For more information on VMS classes and tailoring, refer to the VMS 
Operating System Software Product Description (SPD 25.01.xx).

OPTIONAL SOFTWARE   

Certain versions of these products depend upon a specific version of the 
Operating System.  Please see the System Software Addendum of the product 
in question to determine which version is required. 

VAX ACMS V3.1
VAX DATATRIEVE V4.1 - V4.2
VAX Language-Sensitive Editor V2.3

GROWTH CONSIDERATIONS

The minimum hardware/software requirements for any future version of this 
product may be different than the minimum requirements for the current 
version.

DISTRIBUTION MEDIA  

Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

This product is also available as part of the VMS Consolidated Software 
Distribution on CDROM.

ORDERING INFORMATION

Full Development System

Software Licenses:  QL-706A*-**
Software Media:  QA-706A*-**
Software Documentation:  QA-706AA-GZ
Software Product Services:  QT-706A*-**

Run-time System

Software Licenses:  QL-711A*-**
Software Media:  QA-711A*-**
Software Product Services:  QT-711A*-**

* Denotes variant fields.  For additional information on available 
  licenses, services and media, refer to the appropriate price book.

The above information is valid at time of release.  Please contact your 
local Digital office for the most up-to-date information.


November 1989
AE-LT06C-TE


