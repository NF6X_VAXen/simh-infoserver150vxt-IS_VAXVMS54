 








   AV-KF11B-TE

                              IMPORTANT

               READ THIS BEFORE INSTALLING VAX TEAMINFO


1  VAX TEAMINFO Documentation Set

      ___________________________ Note ___________________________

      VAX TEAMINFO is the name under which the Swedish version of
      VAX TEAMDATA is marketed. However, the product is referred
      to by the name VAX TEAMDATA throughout the documentation.

      ___________________________________________________________

1.1  Information for All Users

   The Version 1.3 documentation set contains the following
   components:

   o  VAX TEAMDATA Quick Reference Guide (VAX TEAMDATA
      Referensanvisningar), Version 1.1

   o  Introduction to VAX TEAMDATA (VAX TEAMDATA Introduktion),
      Version 1.1

   o  Using VAX TEAMDATA (VAX TEAMDATA Användarhandbok), Version 1.1

   o  VAX TEAMDATA Command Summary (VAX TEAMDATA
      Kommandobeskrivning), Version 1.1

   o  VAX TEAMDATA Update Manual (Tillägg och förändringar i VAX
      TEAMDATA), Version 1.3

   o  VAX TEAMDATA Installation Guide, Version 1.3 (in English).




                                                                    1

 







   The Version 1.1 documentation provides you with detailed
   information on the basic functionality of VAX TEAMINFO. The
   Version 1.3 documentation provides you with information on the
   new features available with Version 1.3.

1.2  Information for Existing Users

   If you are upgrading from VAX TEAMINFO Version 1.1 to Version 1.3,
   you will have received the following new documentation components
   with your Version 1.3 Update Kit:

   o  VAX TEAMDATA Update Manual (Tillägg och förändringar i VAX
      TEAMDATA), Version 1.3

   o  VAX TEAMDATA Installation Guide, Version 1.3 (in English).

   Please insert the manual Tillägg och förändringar i VAX TEAMDATA
   in your existing TEAMINFO binder in place of the Version 1.1 RALLY
   Application User's Guide (RALLY-tillämpningar Användarhandbok).
   The RALLY Application User's Guide no longer forms part of the VAX
   TEAMINFO documentation set. For information on VAX RALLY, consult
   the VAX RALLY documentation set.

   You should also discard the Version 1.1 VAX TEAMDATA Installation
   Guide. Use the Version 1.3 VAX TEAMDATA Installation Guide to
   install your new VAX TEAMINFO system.

2  Installing New Versions of VAX TEAMINFO

   If you currently have the U.S. VAX TEAMDATA software installed
   on your system, you must do the following before you install your
   Swedish VAX TEAMINFO version:

   1. Delete the files:






2

 







        TDASYSDB.RDB
        TDASYSDB.SNP
        TDASYSDB_NEW.RBR

      These files contain the public folder database, which includes
      the EXAMPLES public folder supplied with the system. You
      must delete these files to ensure that the latest version
      of the EXAMPLES public folder will be made available. This
      is necessary because existing versions of the files are not
      overwritten by the new versions during installation.

      If you wish to continue using any public folders contained in
      the old public folder database, you must recreate these folders
      after you have installed VAX TEAMINFO. To do this:

      o  Use the CREATE PUBLIC_FOLDER (SKAPA GEMENSAM_MAPP) command
         to recreate each of the public folders.

      o  Use the TRANSFER COPY (ÖVERFÖR KOPIERA) command to copy
         entries for tables and spreadsheets to the public folder.
         Refer to the manual VAX TEAMDATA Användarhandbok for more
         information on creating public folders.

   2. Rename the file TDAMENSHR.EXE contained in the SYS$SYSTEM
      directory. Do this as follows:

        $ RENAME TDAMENSHR.EXE TDAMENSHR_US.EXE

      You need to rename this file so that VAX TEAMINFO does not
      check its identification number during installation. The
      identification number of this file is incompatible with
      the number of the new version of the file that you will be
      installing.

      If VAX TEAMINFO finds an existing version of TDAMENSHR.EXE
      during installation, it compares the identification number of
      this file with that of the new version that is being installed.
      If these numbers are incompatible, the installation fails.



                                                                    3

 







3  Product Authorization Key

   During the installation procedure for VAX TEAMINFO Version
   1.3, you will be asked if you have registered and loaded an
   Authorization Key for this product. Depending on the type
   of licence that you have purchased, the name on the Product
   Authorization Key for the Swedish version of VAX TEAMINFO is
   either:

   o  TEAMINFO-UI-SVENSKA or

   o  TEAMINFO-UI-SVENSKA-USER.

   If you have loaded an Authorization Key for either of these two
   names, you should answer YES to the question. If you answer YES,
   the installation then proceeds in the usual manner. If you answer
   NO, you are asked if you wish to continue with the installation.

      ___________________________ Note ___________________________

      The installation procedure always displays the name
      TEAMDATA, irrespective of which type of licence you have
      purchased.

      ___________________________________________________________

4  System Support Addendum

   Please note that the System Support Addendum (SSA) number given
   in the VAX TEAMDATA Installation Guide is the U.S. number.
   The correct number for the Swedish System Support Addendum is:
   29.13.01-A.








4

 







5  Online Release Notes

   You are requested to read the Online Release Notes supplied with
   the software for further information on new features, restrictions
   and limitations of this version of VAX TEAMINFO.




   © Digital Equipment Corporation, 1988. All rights reserved.





























                                                                    5
