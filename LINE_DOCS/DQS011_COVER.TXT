 






          ________________________________________________________________

          VAX Distributed Queuing Service Version 1.1
                                                                   DIGITAL
          AV-MA45A-TE
          July__1988______________________________________________________


          Dear Customer:

          This letter contains important information that will help you
          understand and use the VAX Distributed Queuing Service (DQS)
          V1.1 and its documentation.

          Product Features

          DQS V1.1 supports VMS Versions 4.6, 4.7, and 5.0. DQS V1.1
          features are limited to the support of VMS Version 5.0 and the
          VMS License Management Facility (LMF). Please read the following
          information on VMS Version 5.x for details.

          VMS Version 5.x Information

          VMS Version 5.0 introduces the VMS License Management Facility
          (LMF) to Digital customers. In order to use a Digital software
          product on a VMS Version 5.x system, customers are now required
          to register product license information in the system's license
          database. The DQS V1.1 product comes with a sheet of paper that
          lists its license information. For new customers, this document
          is called a License PAK (Product Authorization Key); For exist-
          ing DQS customers, this document is called a Service Update PAK
          (Product Authorization Key). Prior to installing the DQS V1.1
          product, the customer should register the license information
          listed on the PAK into their system's license database using
          the LMF utility. Instructions for doing so are contained in the
          DQS Installation Guide and in the VMS License Management Utility
          Manual. These licensing procedures are true for all software
          products that support VMS Version 5.0 or later.


        �1988 by Digital Equipment Corporation o All rights reserved o Printed 
         in U.S.A.

 

          Page 2                                               AV-MA45A-TE




          Release Notes

          Please read the on-line DQS V1.1 Release Notes for more infor-
          mation about the content and function of the DQS V1.1 product.
          To read the on-line Release Notes, you must specify the OPTIONS
          N option when installing DQS. Instructions for doing so are
          contained in Chapter 2 of the VAX Distributed Queuing Service
          Installation Guide.

          Documentation

          Only the DQS Installation Guide has been updated for the DQS
          V1.1 release. The three other DQS manuals (Management Guide,
          User's Guide, and Command Reference) remain unchanged from
          the V1.0 release. Any references to DQS V1.0 in the Management
          Guide, User's Guide, or Command Reference also apply to DQS
          V1.1.

          The DQS manuals have been re-packaged with gray covers, in
          keeping with Digital's new standards for its documentation.
          Although the contents of the Management Guide, User's Guide, and
          Command Reference have not changed from the V1.0 release, these
          manuals have been re-issued with gray covers to reflect the new
          packaging for Digital documentation.
