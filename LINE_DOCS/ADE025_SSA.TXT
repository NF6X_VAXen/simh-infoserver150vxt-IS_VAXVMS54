 






     System
     Support
     Addendum

     ________________________________________________________________
     ________________________________________________________________

     PRODUCT NAME:  VAX ADE, Version 2.5               SSA 25.76.09-A
                     Information Management for Office Professionals

     HARDWARE REQUIREMENTS

     Processors Supported

     VAX:        VAXft 3000-310

                 VAX 4000 Model 300

                 VAX 6000 Model 200 Series,
                 VAX 6000 Model 300 Series,
                 VAX 6000 Model 400 Series,
                 VAX 6000 Model 500 Series

                 VAX 8200, VAX 8250, VAX 8300, VAX 8350, VAX 8500,
                 VAX 8530, VAX 8550, VAX 8600, VAX 8650, VAX 8700,
                 VAX 8800, VAX 8810, VAX 8820, VAX 8830, VAX 8840

                 VAX 9000-210, VAX 9000-410

                 VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

     MicroVAX:   MicroVAX II, MicroVAX 2000,
                 MicroVAX 3100, MicroVAX 3300,
                 MicroVAX 3400, MicroVAX 3500,
                 MicroVAX 3600, MicroVAX 3800,
                 MicroVAX 3900



                                  DIGITAL               February 1991

                                                          AE-LT54B-TE

 


     VAX ADE, Version 2.5                              SSA 25.76.09-A
      Information Management for Office Professionals



     VAXstation: VAXstation II, VAXstation 2000,
                 VAXstation 3100 Series, VAXstation 3200,
                 VAXstation 3500, VAXstation 3520,
                 VAXstation 3540

     VAXserver:  VAXserver 3100, VAXserver 3300, VAXserver 3400,
                 VAXserver 3500, VAXserver 3600, VAXserver 3602,
                 VAXserver 3800, VAXserver 3900

                 VAXserver 4000 Model 300

                 VAXserver 6000-210, VAXserver 6000-220, VAXserver
                 6000-310, VAXserver 6000-320, VAXserver 6000-410,
                 VAXserver 6000-420, VAXserver 6000-510, VAXserver
                 6000-520

     Processors Not Supported

     MicroVAX I, VAXstation I, VAX-11/725, VAX-11/782, VAXstation
     8000

     Processor Restrictions

     A TK50 Tape Drive is required for standalone MicroVAX 2000 and
     VAXstation 2000 systems.

     Other Hardware Required

     A minimum system configuration consists of:

     o  At least 1 megabyte of main memory.

     o  A VT52 terminal, a VT100 family terminal, a VT200 family
        terminal, or a Digital Personal Computer in VT102 emulation
        mode.

     o  An R80/RL02 configuration is required for VAX 11/730 systems.

                                     2

 


     VAX ADE, Version 2.5                              SSA 25.76.09-A
      Information Management for Office Professionals


     Note: For interactive use of ADE, it is recommended that termi-
     nals be run on lines with transfer rates of at least 1200 baud.
     It is also recommended that a 250 page minimum working set size
     be used.

     Disk Space Requirements (Block Cluster Size = 1):

     Disk space required for      2,900 blocks
     installation:
                                  (1484.8 Kbytes)

     Disk space required for      2,200 blocks
     use (permanent):
                                  (1126.4 Kbytes)

     These counts refer to the disk space required on the system
     disk. The sizes are approximate; actual sizes may vary depending
     on the user's system environment, configuration, and software
     options.

     CLUSTER ENVIRONMENT

     This layered product is fully supported when installed on any
     valid and licensed VAXcluster* configuration without restric-
     tions. The HARDWARE REQUIREMENTS sections of this product's
     Software Product Description and System Support Addendum detail
     any special hardware required by this product.

     *  V5.X VAXcluster configurations are fully described in the
        VAXcluster Software Product Description (29.78.xx) and in-
        clude CI, Ethernet, and Mixed Interconnect configurations.








                                     3

 


     VAX ADE, Version 2.5                              SSA 25.76.09-A
      Information Management for Office Professionals


     SOFTWARE REQUIREMENTS

     For Systems Using Terminals (No DECwindows Interface):

     VMS Operating System V5.3 - V5.4

     For Workstations Running VWS:

     VMS Operating System V5.3 - V5.4
     VMS Workstation Software as appropriate

     For Workstations Running DECwindows:

     VMS Operating System V5.3 - V5.4 (and necessary components of
     VMS DECwindows)

     This product may run in either of the following ways:

     o  Stand-alone execution - running the X11 display server and
        the client application on the same machine.

     o  Remote execution - running the X11 display server and the
        client application on different machines.

     VMS DECwindows is part of the VMS Operating System but must
     be installed separately. Installation of VMS DECwindows gives
     you the option to install any or all of the following three
     components:

     o  VMS DECwindows Compute Server (Base kit; includes runtime
        support)

     o  VMS DECwindows Device Support

     o  VMS DECwindows Programming Support

     For stand-alone execution, the following DECwindows components
     must be installed on the machine:

     o  VMS DECwindows Compute Server

     o  VMS DECwindows Device Support

                                     4

 


     VAX ADE, Version 2.5                              SSA 25.76.09-A
      Information Management for Office Professionals


     o  VMS DECwindows Programming Support

     For remote execution, the following DECwindows components must
     be installed on the machine:

     Server Machine

     o  VMS DECwindows Compute Server

     o  VMS DECwindows Device Support

     o  VMS DECwindows Programming Support

     Client Machine

     o  VMS DECwindows Compute Server

     o  VMS DECwindows Device Support

     o  VMS DECwindows Programming Support

     VMS Tailoring

     For VMS V5.x systems, the following VMS classes are required for
     full functionality of this layered product:

     o  VMS Required Saveset

     o  Programming Support

     o  System Programming Support

     o  Secure User's Environment

     o  Utilities

     o  VMS Workstation Support

     For more information on VMS classes and tailoring, refer to
     the VMS Operating System Software Product Description (SPD
     25.01.xx).

                                     5

 


     VAX ADE, Version 2.5                              SSA 25.76.09-A
      Information Management for Office Professionals


     GROWTH CONSIDERATIONS

     The minimum hardware/software requirements for any future ver-
     sion of this product may be different from the requirements for
     the current version.

     DISTRIBUTION MEDIA

     Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

     This product is also available as part of the VMS Consolidated
     Software Distribution on CDROM.

     The software documentation for this product is also available as
     part of the VMS Online Documentation Library on CDROM.

     ORDERING INFORMATION

     Software Licenses: QL-425A*-**
     Software Media: QA-425A*-**
     Software Documentation: QA-425AA-GZ
     Software Product Services: QT-425A*-**

     *  Denotes variant fields. For additional information on avail-
        able licenses, services, and media, refer to the appropriate
        price book.

     The above information is valid at time of release. Please con-
     tact your local Digital office for the most up-to-date informa-
     tion.

     [TM] The DIGITAL Logo, DATATRIEVE, DEC, DECstation, DECsys-
          tem, DECwindows, MicroVAX, RC, VAX, VAXcluster, VAXft,
          VAXserver, VAXstation, VMS, and VT are trademarks of Digi-
          tal Equipment Corporation.




                                     6
