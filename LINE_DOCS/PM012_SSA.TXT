
PRODUCT NAME: VAX Software Project Manager, Version 1.2      SSA 27.52.03-A
                                                         
HARDWARE REQUIREMENTS 

Processor Support

VAX: VAX 6210, VAX 6220, VAX 6230, VAX 6240, VAX 6310, VAX 6312, VAX 6320, 
     VAX 6330, VAX 6333, VAX 6340, VAX 6350, VAX 6360, VAX 8200, VAX 8250, 
     VAX 8300, VAX 8350, VAX 8500, VAX 8530, VAX 8550, VAX 8600, VAX 8650, 
     VAX 8700, VAX 8800, VAX 8810, VAX 8820, VAX 8830, VAX 8840, VAX 8842, 
     VAX 8974, VAX 8978

     VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

MicroVAX: MicroVAX II, MicroVAX 2000, MicroVAX 3300, MicroVAX 3400,
          MicroVAX 3500, MicroVAX 3600

VAXstation: VAXstation II, VAXstation 2000, VAXstation 3100, VAXstation 3200, 
            VAXstation 3500

VAXserver: VAXserver 3300, VAXserver 3400, VAXserver 3500, VAXserver 
           3600, VAXserver 3602, VAXserver 6210, VAXserver 6220, VAXserver 
           6310, VAXserver 6320

Not supported: VAX-11/725, VAX-11/782, MicroVAX I, VAXstation I, 
               VAXstation 8000

A VT33X-series terminal, or any of the supported VAXstations listed, is
required for all graphical functions.  Command mode activity may be performed
from character cell terminals.  Hardcopy output of all reports with graphical
content may be directed to printers with graphic capabilities. Note that the
performance characteristics of regis and postscript devices are superior to
sixel devices for such reports.  Text output may be any printer, including line
printers. 

Processor Restrictions

A TK50 tape drive is required for MicroVAX 2000 and VAXstation 2000 
systems.

Block Space Requirements (Block Cluster Size = 1):

Disk space required for installation:           5300 blocks 
                                                (2713K bytes)

Disk space required for use (permanent):        5300 blocks  
                                                (2713K bytes)

These counts refer to the disk space required on the system disk.  The sizes
are approximate; actual sizes may vary depending on the user's system
environment, configuration, and software options. 

CLUSTER ENVIRONMENT

This layered product is fully supported when installed on any valid
and licensed VAXcluster* configuration without restrictions.  The
HARDWARE REQUIREMENTS sections of this product's Software Product 
Description and System Support Addendum detail any special hardware
required by this product.

* V5.x VAXcluster configurations are fully described in the VAXcluster
  Software Product Description (29.78.xx) and include CI, Ethernet, and 
  Mixed Interconnect configurations.

SOFTWARE REQUIREMENTS

For workstations running VMS DECwindows:

^ VMS Operating System (including VMS DECwindows) V5.1 

For Workstations running VMS Workstation Software:

^ VMS Operating System V5.0 

^ VMS Workstation Software V3.0 - V4.0

For systems using terminals:  (No VMS DECwindows interface)

^ VMS Operating System V5.0 - V5.1 

VMS Tailoring 

For VMS V5.x systems, the following VMS classes are required for full 
functionality of this layered product:

^ VMS Required Saveset

^ Utilities

For more information on VMS classes and tailoring, refer to 
VMS Operating System (SPD 25.01.xx).

OPTIONAL SOFTWARE

None

GROWTH CONSIDERATIONS

The minimum hardware/software requirements for any future version of this 
product may be different from the minimum requirements for the current version. 

DISTRIBUTION MEDIA

Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

ORDERING INFORMATION:

Softfware Licenses:  QL-A82A*-**
Software Media:  QA-A82A*-**
Software Documentation: QA-A82AA-GZ
Software Product Services: QT-A82A*-**

*  Denotes variant fields.  For additional information on available 
   licenses, services  and media, refer to the appropriate price book.

The above information is valid at time of release.  Please contact your local 
DIGITAL office for the most up-to-date information.

March 1989
AE-LT25B-TE

