
 






                                                 Order Number: AV-PDSCA-TE


                                       DEC/EDI

                                    READ ME FIRST



          Introduction

             This letter describes some problems with DEC/EDI, and
             gives solutions where possible. You should read this letter
             together with the DEC/EDI Release Notes.

          General Problems With Solutions

              _ When running the Installation Verification Procedure

                (IVP), a checksum error is flagged for the file DECEDI$INTERCHANGE.EXE.
                Provided this is the is only checksum error reported, you
                can ignore it and continue with the IVP. If you get more
                than one checksum error, the installation may be suspect
                and you must repeat the installation.

              _ On a very busy system with window schedules switched

                OFF, it is possible for the communications services to
                be so saturated with files that the whole communications
                services system fails to process further documents. If
                this happens, the "SHOW SYSTEM" command shows you that the
                scheduler and one or more gateways are in RWMBX state. For
                example:

                  $SHOW SYSTEM





                                                                         1

 






                  Pid      Process Name    State  Pri      I/O .......
                  25800081 DECEDI$DS       LEF     16        0 .......
                  25800083 DECEDI$FS_1     LEF     10      138 .......
                  25800085 DECEDI$CNV_E_1  HIB     10      138 .......
                  25800086 DECEDI$TRN_E_1  HIB     16        0 .......
                  25800087 DECEDI$TFB      HIB     16        0 .......
                  25800088 DECEDI$TFS      HIB     16        0 .......
                  25800089 DECEDI$SCHEDULE RWMBX   16        0 .......
                  2580008A DECEDI$IMPEXP   RWMBX   10      138 .......
                  .
                  .
                  .

                The scheduler and gateways are deadlocked and the only way
                to break this deadlock is as follows:

                 1 Use "STOP/ID" on the scheduler and on ALL gateways that
                   are running. That could be for each of: DECEDI$OFTP,
                   DECEDI$X400, DECEDI$BISYNC, and DECEDI$IMPEXP.

                 2 Issue a DEC/EDI startup command:

                     $ @SYS$STARTUP:DECEDI$STARTUP FULL

              _ If the file server gets an error on accessing the Internal

                Format (.IHF) file for an incoming document at TRANSLATED,
                it puts a FAILED_TO_COPY entry in the history but leaves
                the update flag for the TRANSLATED record at "Y". This
                means that, when the file server next wakes up, it tries
                again and adds another record to the history, and so on.

                The error log file contains repeated lines like the
                following:

          %DECEDI-E-ERRACCIHF, error accessing internal format file
          -%RMS-E-FNF, file not found
          -%DECEDI-I-CONDLOGED, condition logged by DECEDI$FS_1\DECEDI$$FS_TSRV_COPY at ..


          2

 






                LIST DOC/SERV=TRANS/FULL shows:

          18-JUN-1990 10:28   Transmission file is being separated
          18-JUN-1990 10:29 Y Document successfully translated into internal format
          18-JUN-1990 10:41   Failed while copying document from translation to applicat
                              ion
          18-JUN-1990 10:46   Failed while copying document from translation to applicat
                              ion
          18-JUN-1990 10:51   Failed while copying document from translation to applicat
                              ion
          18-JUN-1990 10:56   Failed while copying document from translation to applicat
                              ion

                This implies that the document cannot be canceled. However
                if the problem can be corrected the document will be
                copied the next time the file server wakes up.

              _ The file server does not copy a document to the next step

                in the process if the Trading Partner Agreement Table
                entry is missing. This behavior is correct. However, the
                Detailed Listing for that document is wrong and it may
                look like this:

          DETAILED LIST:

          +-------------+                     DEC/EDI                       NODE:PICOST::

          |d|i|g|i|t|a|l|              Full Document Details                  15-JUN-1990

          +-------------+-----------------------------------------------------------------

           Document ID                          Status







                                                                         3

 






           TRANSTEST_O_0000000069               FAILED
           Document Type             : PURORD
           User Reference            : 0000034260
           Partner Id                : DECTEST1
           Destination Node          : PICOST::
           Creation Date             : 15-JUN-1990 08:23
           Last Modified Date        : 15-JUN-1990 08:23
           Acknowledgement Status    :
           Business Ack. Status      : No special action requested
           Tracking Count            : 0000000069
           Message Router ID         :
           Priority                  : NORMAL
           Test Indicator            : Live document

           Document ID                          Status
           TRANSTEST_O_0000000069               FAILED

           Tracking Date       Tracking Status

           15-JUN-1990 08:23   Document created by application interface routines
           15-JUN-1990 08:23   Failed while copying document from application to translat
                               ion

                The error log DECEDI$ERRORS.LOG is much more useful when
                you are trying to find out what the problem is. It looks
                like this:

          %DECEDI-E-NOTPAGREE, no trading partner agreement
          -%RMS-E-RNF, record not found
           -%DECEDI-I-CONDLOGED, condition logged by DECEDI$FS_1\DECEDI$$FS_ASRV_COPY at
           15-JUN-1990 08:23:22 called by DECEDI$$FS_COPY

              _ It has been found that the combination of a 12-character

                USERNAME and a 6-character NODE NAME prevents the user
                from accessing the INTERCHANGE image.

                The solution to this problem is to ensure that the
                combination of USERNAME and NODE NAME is less then 18
                characters.

          4

 






          X12 and TDCC Problems with No Solutions

              _ When elements have been redefined in Trading Partner

                specific documents, the converter should look for Trading
                Partner data labels in outbound documents. If they are not
                there, the converter should fail the document. Currently
                the converter uses Private Data Labels and does not fail
                the document.

                The translator fails inbound documents of this type. This
                is the correct behavior.

              _ Repeating qualifier value pairs do not get inserted into

                the external segment. An error of "more repeating element
                positions" is returned. The first occurrence of the
                repeating qualifier/value pair is successful, however.
                Also, the function works when the $value precedes the
                $qualifier.

          X12 and TDCC Problems with Solutions

              _ If DEC/EDI system or the VAX system halts during the

                processing of high priority X12/TDCC documents (IMMEDIATE
                priority) DEC/EDI will recover only one IMMEDIATE
                document. If more than one IMMEDIATE document is in a
                transient state, you need to start the X12 converter
                several times to recover all the documents.

              _ The Interchange standard and version help is slightly

                misleading. For the standard it says that the only valid
                value is "U". The Interchange standard and version is
                only valid for the ISA segment. At present, "U" is the
                only valid value for X12 and TDCC. These two fields should
                be left blank if the envelope name is BG. BG is the TDCC
                interchange envelope header.

                                                                         5

 






              _ The help for the version field says "must contain the

                value 00200". However, it is valid to leave this field
                empty.

          Management Functions with Solutions

              _ Using the "Edit Another Segment" option in the EDIT DATA_

                LABELS command, you can modify the data labels used in the
                reserved segments (ISA,ST,UNH, and so on). If you decide
                to do this, the result would be a seriously disturbed
                translation service. It is highly recommended that you not
                modify reserved segments data labels.

              _ LIST DOCUMENT is not handling the /ACKNOWLEDGEMENT

                qualifier correctly. It is only valid for the Translation
                Service so should be treated in the same way as /TRANSMISSION_
                FILENAME.

                However the DCL ignores the /ACKNOWLEDGEMENT qualifier
                if /SERV=TRANS is not specified. This results in the
                following:

                  LIST DOC/ACKNOWLEDGEMENT=ACCEPTED

                List everything in the Site Audit Log file: it should list
                accepted records from the Translate audit log file.

                The solution is to always specify /SERVICE=TRANSLATION
                when using /ACKNOWLEDGEMENT.

          Management Functions with No Solutions

             There are problems with using wildcards in the document_id
             for the LIST DOCUMENT command. For example, you should be
             able to list all outgoing documents, by using the following
             command:

               LIST DOCUMENT *_O_*

          6

 






             However, what the command actually does is to find the first
             matching record and then stop searching as soon as it finds a
             record that does not match.





































                                                                         7

 






             First Printing, June 1990

             The information in this document is subject to change without
             notice and should not be construed as a commitment by Digital
             Equipment Corporation. Digital Equipment Corporation assumes
             no responsibility for any errors that may appear in this
             document.

             The software described in this document is furnished under a
             license and may be used or copied only in accordance with the
             terms of such license.

             Digital Equipment Corporation assumes no responsibility for
             the use or reliability of its software on equipment that is
             not supplied by Digital.

             Restricted Rights: Use, duplication, or disclosure by the
             U.S. Government is subject to restrictions as set forth in
             subparagraph (c)(1)(ii) of the Rights in Technical Data and
             Computer Software clause at DFARS 252.227-7013.

             Copyright (c) Digital Equipment Corporation. 1990. All rights
             reserved.

             The following are trademarks of Digital Equipment Corporation:

             DEC                   DECnet                UNIBUS

             VAX P.S.I.            VAX                   VAXcluster

             VMS                   VT100                 VT200

             VT300

             DIGITAL





          8
