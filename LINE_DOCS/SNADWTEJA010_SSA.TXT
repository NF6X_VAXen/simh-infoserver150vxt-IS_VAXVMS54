System
Support
Addendum
================================================================================
日本語 DECwindows  DECnet/SNA  3270 ターミナル・エミュレータ/VMS，バー
ジョン 1.0  システム・サポート付加情報
                                                             SSA 26.K3.00-A


必要なハードウェアの最小構成

●適用プロセッサ

 VAX:     VAX 4000  モデル300,
          VAX 6000  モデル200 シリーズ,
          VAX 6000  モデル300 シリーズ,
          VAX 6000  モデル400 シリーズ,
          VAX 6000  モデル500 シリーズ,
          VAX 8200,  VAX  8250,  VAX  8300
          VAX 8350,  VAX  8500,  VAX  8530,
          VAX 8550,  VAX  8600,  VAX  8650,
          VAX 8700,  VAX  8800,  VAX  8810,
          VAX 8820,  VAX  8830,  VAX  8840,
          VAX 8842,  VAX  8974,  VAX  8978,
          VAX 9000  モデル400 シリーズ,
          VAX 9000-210,  VAXft  3000-310,
          VAX-11/750,  VAX-11/780,  VAX-11/785
 MicroVAX:      MicroVAX  II,  MicroVAX  2000,
                MicroVAX  3100,  MicroVAX  3300,
                MicroVAX  3400,  MicroVAX  3500,
                MicroVAX  3600,  MicroVAX  3800,
                MicroVAX 3900
 VAXstation:       VAXstation  II,VAXstation  2000,
                   VAXstation  3100シリーズ,
                   VAXstation  3200,VAXstation  3500,
                   VAXstation  3520,VAXstation  3540
 VAXserver:      VAXserver  3100,VAXserver  3300,
                 VAXserver  3400,VAXserver  3500,
                 VAXserver  3600,VAXserver  3602,
                 VAXserver  3800,VAXserver  3900,
                 VAXserver 4000 モデル300,
                 VAXserver 6000-210,
                 VAXserver 6000-220,
                 VAXserver 6000-310,
                 VAXserver 6000-320,
                 VAXserver 6000-410,
                 VAXserver 6000-420,
                 VAXserver 6000-510,
                 VAXserver 6000-520

●適用されないプロセッサ

 ・  VAX-11/725

 ・  VAX-11/730

 ・  VAX-11/782

 ・  MicroVAX I

 ・  VAXstation I

 ・  VAXstation 8000


●プロセッサに関する制限事項

サーバとしての PC  でのリモート実行はサポートされません。


●その他の必要なハードウェア

DECnet/SNA  ゲートウェイまたは，VMS/SNA  システムは，IBM  メイン・フ
レーム環境へアクセスするシステムとして必要です。

     DECnet/SNA  Gateway  (DECSA-FA)
     (SPD 30.15.xx)
     DEC  MicroServer  (DEMSA-Ax)
     (SPD 25.C6.xx)
     DEC  MicroServer-SP  (DEMSB-Ax)
     (SPD 25.C6.xx)
     DEC  ChannelServer  (DESNA-Ax)
     (SPD 29.76.xx)
     VMS/SNA  (SPD  27.01)

●必要なディスク容量
(Block  Cluster  Size=1の場合)

 インストール時 :      約8,000ブロック
                       (4,000kバイト)
 システム運用時 :      約5,000ブロック
                       (2,500kバイト)

これらの値は，システム・ディスク上で必要なディスク容量を示したものであ
り，概数です。実際の値はユーザのシステム環境，構成，選択するソフトウェ
アによって異なります。


● 日本語DECwindows  をサポートする際に必要なメモリ

クライアントとサーバが同じシステム上で実行されている，スタンドアロンの
日本語 DECwindows  環境で,  日本語 DECwindows  DECnet/SNA  3270  ター
ミナル・エミュレータを実行するには，少なくとも4MB のメモリが必要で
す。
日本語DECwindows アプリケーションのパフォーマンスとメモリの使用量
は，システム環境によって異なります。
パフォーマンス向上のためには，メモリの増加等が有効です。
もしサーバ（アプリケーションの表示するコンポーネント）が他のノード上に
ある場合は，これよりも少ないメモリ使用量ですみます。
標準ハードウェアのメモリの大きさは，パフォーマンスを良くするために，最
低6MB は必要です。


クラスタ環境

日本語 DECwindows  DECnet/SNA  3270  ターミナル・エミュレータは，正規
のライセンスを持つクラスタ構成(*) 上にインストールされている場合には，
機能上の制約を受けません。本ソフトウェアで必要とされるハードウェアにつ
いては，必要なハードウェアの最小構成の項に記述されているとおりです。
(*) バージョン5.x のVAXcluster 構成についてはVAXcluster の「Software
Product  Description(SPD  29.78.xx)」 に詳しく説明されています。ま
た，この構成にはCI，Ethernet およびMixed Interconnect 構成が含ま
れます。


必須ソフトウェア

● DECnet/SNA  Gateway  または VMS/SNA ソフトウェア

以下に示す環境により，次のソフトウェアのいずれかが必要です。

 ・  VMS/SNA  V2.0  (SPD  27.01.xx)

 ・  DECnet/SNA  Gateway-CT  V1.0  (SPD  29.76.xx)

 ・  DECnet/SNA  Gateway-ST  V1.1  (SPD  25.C6.xx)


DECnet/SNA  Gateway  環境

 ・  VMS  オペレーティング・システム V5.3-1〜V5.4 （および日本語VMS
     DECwindows に必要なコンポーネント）

 ・  DECnet-VAX V5.3 〜V5.4

 ・  DEMSA，DEMSB，DESNA を使用したゲートウェイのロード・ホスト・
     ノードとして DECnet/SNA  VMS  Gateway  Management  V2.0  が必要で
     す。

VMS/SNA 環境

 ・  VMS  オペレーティング・システム V5.3-1〜V5.4 （および日本語VMS
     DECwindows に必要なコンポーネント）

 ・  VMS/SNA  ソフトウェア V2.0

● 日本語DECwindows  を実行するワークステーション

 ・  スタンドアロン環境での実行：X11 ディスプレイ・サーバとクライアン
     ト・アプリケーションを同じマシン上で実行します。

 ・  リモート環境での実行：X11 ディスプレイ・サーバとクライアント・ア
     プリケーションを違うマシン上で実行します。

日本語VMS  DECwindows  のインストレーションの際に，オプションで以下の
3 つのコンポーネントのいずれか，またはすべてをインストールできます。

 ・  VMS  DECwindows  Compute  Server（Base  kit;  ランタイム・サポー
     トを含む）

 ・  VMS  DECwindows  Device  Support

 ・  VMS  DECwindows  Programming  Support

スタンドアロン実行では，以下の日本語DECwindows コンポーネントがイン
ストールされていることが必要です。

 ・  VMS  DECwindows  Compute  Server

 ・  VMS  DECwindows  Device  Support

リモート実行では，以下の日本語DECwindows コンポーネントがインストー
ルされていることが必要です。

 ・  サーバ・マシン：VMS  DECwindows  Device  Support

 ・  クライアント・マシン：VMS  DECwindows  Compute  Server


●VMS テーラリング

VMS バージョン5.x 以降のシステムでは, 本レイヤード・プロダクトの全機能
を得るには次のVMS 構成要素が必要です。

 ・  VMS  Required  Saveset

 ・  Network Support

 ・  Utilities

VMS のクラスおよびテーラリングについては, 「VMS オペレーティング・シス
テム ソフトウェア仕様書(SPD  25.01.xx)」を参照してください。

オプション・ソフトウェア

なし


バージョンアップの考慮

本製品の将来のバージョンでは, 必要なハードウェアおよびソフトウェアの最
小構成が変更されることがあります。


提供媒体

 テープ:    9 トラック 1600BPI  磁気テープ,
            TK50  ストリーム・テープ


注文情報

最寄りの日本DEC の各支店/ 営業所にお問い合わせください。



注：DECwindows，DECnet，MicroVAX，VAX，VAXstation  は米国ディジ
タルイクイップメント社の商標です。


1991 年3 月
AE-B574A-TE-JO
