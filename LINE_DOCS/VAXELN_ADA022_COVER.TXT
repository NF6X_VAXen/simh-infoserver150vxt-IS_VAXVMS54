 






          DIGITAL

          Read Before Installing or Using VAXELN Ada Version 2.2

          AV-HJ46E-TE

          Whether you are a new or a continuing VAXELN Ada customer,
          please take time to read the following information about your
          product.

          New VAXELN Ada customers should receive the following:

          o  The media required for Version 2.2 of VAXELN Ada

          o  User documentation

          o  Supplemental materials and any additionally ordered items

          Continuing VAXELN Ada customers should receive the following:

          o  The media required for Version 2.2 of VAXELN Ada

          o  Supplemental materials and any additionally ordered items

          Installation Information

          VAXELN Ada Version 2.2 requires the following software:

          o  VMS Version 5.4 or higher

          o  VAXELN Version 4.1 or higher

          o  VAX Ada Version 2.0 or higher

          If you are installing VAXELN Ada Version 2.2 on a newly-licensed
          node or cluster, you must first register a License Product
          Authorization Key (License PAK) using the License Management
          Facility (LMF). The License PAK may be shipped along with the
          kit if you ordered the license and media together; otherwise, it
          is shipped separately to a location based on your license order.

 






          If you are installing VAXELN Ada Version 2.2 as an update on
          a node or cluster already licensed for this software, you must
          register the Service Update PAK (SUP) that is included in your
          Service Update Kit. If you already registered a License PAK or a
          SUP for this product on this node or cluster, you do not need to
          register the SUP.

          If you are installing an update of VAXELN Ada but lack a service
          contract, call your Digital representative for instructions on
          how to get a License PAK.

          See the VMS License Management Utility Manual for registration
          instructions.

          Previous versions of VAXELN Ada do not need to be installed
          before this version.

          You should use the Version 2.2 kit name (VAXELN_ADA022) when in-
          voking
          VMSINSTAL.




             � Digital Equipment Corporation. 1991. All rights reserved.














                                          2

 






          Release Notes Information

          The release notes for VAXELN Ada Version 2.2 contain important
          information about compatibility, changes, current restrictions
          and restrictions removed, validation information, and documenta-
          tion errors.

          You can read the release notes before installing VAXELN Ada by
          invoking
          VMSINSTAL with the release notes option, N. You can also read
          the release notes following the installation of VAXELN Ada by
          typing:

               $ HELP VAXELN_ADA Release_Notes

          Compatibility Information

          Before installing VAXELN Ada Version 2.2, you should read the
          release notes. The release notes contain important information
          on the compatibility of VAXELN Ada Version 2.2 with previous
          versions of VAXELN Ada. In particular:

          o  VAXELN Ada Version 2.2 requires VAX Ada Version 2.0 or
             higher. Beginning with Version 2.0, VAX Ada uses a program
             library format that is incompatible with the format for pre-
             vious versions of VAX Ada. For more information, see the VAX
             Ada release notes.

          o  The package VAXELN_SERVICES has been bundled with VAX Ada
             since VAX Ada Version 1.4. This package has had some spec-
             ification corrections and changes in VAX Ada Versions 2.0,
             2.1, and 2.2. You do not need to reenter this package as a
             result of installing VAXELN Ada Version 2.2. However, you
             may need to modify your source files to accommodate any
             changes made to VAXELN_SERVICES in a VAX Ada release. For
             more information, see the VAX Ada release notes.

          o  If you are using VAX Ada Version 2.2, then you need VAXELN
             Ada Version 2.2. You cannot use the VAX Ada Version 2.2 com-
             piler as the compiler for VAXELN Ada Version 2.0 or earlier.

                                          3

 






          o  The shareable image ADA$FMATH.EXE is no longer provided
             in the VAXELN shareable image library ELN$:RTLSHARE.OLB.
             Instead, the modules containing the services provided by
             ADA$FMATH.EXE are now provided either in the VAXELN object
             library ELN$:RTL.OLB or in another VAXELN-supplied image.
             ADA$FMATH.EXE is, however, still provided in the ELN$ di-
             rectory for use by previously linked programs that reference
             it.

          You can obtain the VAX Ada Version 2.2 release notes from the
          file
          SYS$HELP:ADA022.RELEASE_NOTES. You can also read the VAX Ada
          release notes by typing:

               $ HELP ADA Release_Notes

          Contents of This Kit

          o  Indented Bill Report (BIL) and Bill of Materials (BOM)

             Please read the BIL and BOM enclosed in this kit and check to
             see that all items listed are actually in your kit. If your
             kit is damaged or any items are missing, call your Digital
             representative.

          o  Media

             If you ordered media, you will find the media and the VAXELN
             Ada Installation Guide in this kit. Consult the VAXELN Ada
             Installation Guide for information about installing VAXELN
             Ada on your system.








                                          4

 






          o  Documentation

             Depending on your order, this kit may include copies of the
             following VAXELN Ada documentation:

             -  VAX Ada and VAXELN Ada Technical Summary-This booklet
                summarizes the features of VAX Ada and VAXELN Ada. It is
                designed to allow users and vendors to compare-at a very
                technical level-the VAX Ada and VAXELN Ada implementations
                with other Ada implementations.

             -  VAXELN Ada User's Manual-This manual presents the features
                of
                VAXELN Ada and explains how to compile, link, build, and
                execute Ada applications using the VAXELN real-time execu-
                tive. It also presents the differences between VAXELN Ada
                and VAX Ada.

             Note that the VAXELN Ada Installation Guide is included with
             the media.

          o  Software Product Description (SPD)

             The SPD provides an overview of the VAXELN Ada kit and its
             features.

          o  System Support Addendum (SSA)

             The SSA describes the technical environment in which the
             product is supported.

          o  Software Performance Report (SPR)

             Use this form to report any problems with VAXELN Ada, pro-
             vided you have purchased warranty services.




                                          5

 






          A Final Note

          Digital prides itself on responding to customer needs. In order
          to continue serving you, we need your comments. Each manual
          contains preaddressed, postage-paid Reader's Comments forms
          at the back. If you find errors in a manual or want to make
          comments about it, please fill out one of these forms and send
          it to us. If you find errors in the software, please submit an
          SPR, provided you have purchased warranty services.






























                                          6
