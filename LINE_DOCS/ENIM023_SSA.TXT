 






     System
     Support
     Addendum

     ________________________________________________________________
     ________________________________________________________________

     PRODUCT NAME:  NMCC/VAX ETHERnim, Version 2.3     SSA 26.96.05-A

     HARDWARE REQUIREMENTS

     Processors Supported

     VAX:      VAX 9000-210, VAX 9000-410,
               VAX 6000 Model 200 Series,
               VAX 6000 Model 300 Series,
               VAX 6000 Model 400 Series

               VAX 8200, VAX 8250, VAX 8300, VAX 8350,
               VAX 8500, VAX 8530, VAX 8550, VAX 8600,
               VAX 8650, VAX 8700, VAX 8800, VAX 8810,
               VAX 8820, VAX 8830, VAX 8840, VAX 8842,
               VAX 8974, VAX 8978

               VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

     MicroVAX: MicroVAX II, MicroVAX 2000,
               MicroVAX 3100, MicroVAX 3300,
               MicroVAX 3400, MicroVAX 3500,
               MicroVAX 3600, MicroVAX 3800,
               MicroVAX 3900

     VAXstationVAXstation II, VAXstation 2000,
               VAXstation 3100, VAXstation 3200,
               VAXstation 3500, VAXstation 3520,
               VAXstation 3540



                                  DIGITAL               November 1990

                                                          AE-MJ58C-TE

 


     NMCC/VAX ETHERnim, Version 2.3                    SSA 26.96.05-A




     VAXserver:VAXserver 3100, VAXserver 3300,
               VAXserver 3400, VAXserver 3500,
               VAXserver 3600, VAXserver 3602,
               VAXserver 3800, VAXserver 3900

               VAXserver 6000-210, VAXserver 6000-310,
               VAXserver 6000-410, VAXserver 6000-420

     Processors Not Supported

     VAX-11/725, VAX-11/730, VAX-11/750, VAX-11/782, MicroVAX I,
     MicroVAX 2000, VAXstation I, VAXstation 2000, VAXstation 8000

     Other Hardware Required

     A VT24x, VT330, or VT340 Terminal is required for systems with
     no DECwindows user interface. Block Space Requirements (Block
     Cluster Size = 1):

     Disk space required for      4,000 blocks
     installation:
                                  (2048K bytes)

     Disk space required for      5,500 blocks
     use (permanent):
                                  (2816K bytes)

     These counts refer to the disk space required on the system
     disk. The sizes are approximate; actual sizes may vary depending
     on the user's system environment, configuration, and software
     options.

     *ETHERnim installs a database with a default size of 1500
     blocks.

     OPTIONAL HARDWARE

     For the Host System:

                                     2

 


     NMCC/VAX ETHERnim, Version 2.3                    SSA 26.96.05-A



     For extended NMCC/VAX ETHERnim sessions and support of large
     network configurations, additional disk storage should be com-
     puted from the following:

     o  Network Database (in blocks) = 48 + (3 x # of Nodes) + 100

     o  Segment Database (in blocks) = 15 + (1 x # of Segments) + 50

     o  Session History (in blocks) = 500 blocks x # hours of run-
        time

     CLUSTER ENVIRONMENT

     This layered product is fully supported when installed on any
     valid and licensed VAXcluster* configuration without restric-
     tions. The HARDWARE REQUIREMENTS sections of this product's
     Software Product Description and System Support Addendum detail
     any special hardware required by this product.

     *  V5.x VAXcluster configurations are fully described in the
        VAXcluster Software Product Description (29.78.xx) and in-
        clude CI, Ethernet and Mixed Interconnect configurations.

















                                     3

 


     NMCC/VAX ETHERnim, Version 2.3                    SSA 26.96.05-A



     SOFTWARE REQUIREMENTS

     For the Host System:

     VMS Operating System V5.3 - V5.4
     DECnet-VAX V5.3 - V5.4

     For the Target System (for optional software use:)

     VMS Operating System V5.3 - V5.4
     DECnet-VAX V5.3 - V5.4

     VMS Tailoring

     For VMS V5.x, the following VMS classes are required for full
     functionality of this layered product:

     o  VMS Required Saveset

     o  Network Support

     o  Programming Support

     o  Secure User's Environment

     o  Utilities

     For more information on VMS classes and tailoring, refer to
     the VMS Operating System Software Product Description (SPD
     25.01.xx).

     OPTIONAL SOFTWARE

     LAN Traffic Monitor (LTM) VMS V1.2 (SPD 27.80.xx)

     GROWTH CONSIDERATIONS

     The minimum hardware/software requirements for any future ver-
     sion of this product may be different from the requirements for
     the current version.

     DISTRIBUTION MEDIA

     Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

                                     4

 


     NMCC/VAX ETHERnim, Version 2.3                    SSA 26.96.05-A



     This product is also available as part of the VMS Consolidated
     Software Distribution on CDROM.

     ORDERING INFORMATION

     Software Licenses: QL-514A*-**
     Software Media: QA-514A*-**
     Software Documentation: QA-514AA-GZ
     Software Product Services: QT-514A*-**

     *  Denotes variant fields. For additional information on avail-
        able licenses, services and media, refer to the appropriate
        price book.

     The above information is valid at time of release. Please con-
     tact your local Digital office for the most up-to-date informa-
     tion.

     [TM]  The DIGITAL Logo, DECnet, DECwindows, VAX ETHERnim,
           MicroVAX, VAX, VAXserver, VAXstation, VMS, and VT are
           trademarks of Digital Equipment Corporation.


















                                     5
