PRODUCT NAME: DECnet/SNA VMS 3270 Data Stream Programming      SSA 26.87.05-A
              Interface, Version 1.4

HARDWARE REQUIREMENTS 

Processor Support 

VAX:   VAX 6210, VAX 6220, VAX 6230, VAX 6240, VAX 6310, VAX 6320, VAX 6330, 
       VAX 6340, VAX 6350, VAX 6360, VAX 6312, VAX 6333, VAX 8200, VAX 8250, 
       VAX 8300, VAX 8350, VAX 8600, VAX 8650, VAX 8500, VAX 8530, VAX 8550, 
       VAX 8700, VAX 8800, VAX 8810, VAX 8820, VAX 8830, VAX 8840, VAX 8842, 
       VAX 8974, VAX 8978

       VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

MicroVAX:   MicroVAX II, MicroVAX 2000, MicroVAX 3500/3600, MicroVAX 3300/3400, 
            MicroVAX 3800/3900

VAXstation: VAXstation II, VAXstation 2000, VAXstation 3100, VAXstation 
            3200/3500, VAXstation 3520/3540, VAXstation 8000

VAXserver:  VAXserver 3300/3400, VAXserver 3500/3600/3602, VAXserver 3800/3900,            
            VAXserver 6210/6220/6310/6320

Processor Restrictions  

A TK50 Tape Drive is required for standalone MicroVAX 2000 and VAXstation 2000 
systems.

Block Space Requirements (Block Cluster Size = 1):

Disk space required for installation:				    1300 blocks
								   (650K bytes)

Disk space required for use (permanent):			    1100 blocks
								   (550K bytes)

These counts refer to the disk space required on the system disk.  The sizes 
are approximations; actual sizes may vary depending on the user's system 
environment, configuration, and software options selected.

OPTIONAL HARDWARE

None

CLUSTER ENVIRONMENT

This layered product is fully supported when installed on any valid and 
licensed VAXcluster* configuration without restrictions.  The HARDWARE 
REQUIREMENTS sections of this product's Software Product Description and System 
Support Addendum detail any special hardware required by this product.

* V5.x VAXcluster configurations are fully described in the VAXcluster Software 
  Product Description (29.78.xx) and include CI, Ethernet and Mixed 
  Interconnect configurations.

SOFTWARE REQUIREMENTS 

In a DECnet/SNA Gateway Environment:

oo VMS Operating System V5.0 - V5.1

oo DECnet-VAX V5.0

In a VMS/SNA environment:

oo VMS Operating System V5.0 - V5.1

oo VMS/SNA V2.0

DECnet/SNA VMS 3270 Data Stream Programming Interface software requires one of 
the following products:

Refer to the respective SPDs for prerequisite hardware and software.

oo VMS/SNA V2.0 (SPD 27.01.xx)
oo DECnet/SNA Gateway-CT V1.0 (SPD 29.76.xx)
oo DECnet/SNA Gateway-ST V1.0 (SPD 25.C6.xx)
oo DECnet/SNA Gateway V1.5 (SPD 30.15.xx)

VMS Tailoring 

For VMS V5.0 and V5.1 systems, the following VMS classes are required for full 
functionality of this layered product:

oo VMS Required Saveset
oo Network Support
oo Utilities
oo Programming Support

For more information on VMS classes and tailoring, refer to the VMS Operating 
System Software Product Description (SPD 25.01.xx).

OPTIONAL SOFTWARE

DECnet/SNA VMS Gateway Management for DECSA V1.5 
DECnet/SNA VMS Gateway Management V2.0

GROWTH CONSIDERATIONS

The minimum hardware/software requirements for any future version of this 
product may be different from the minimum requirements for the current version.


DISTRIBUTION MEDIA

Tape:  9-track 1600 BPI Magtape, TK50 Streaming Tape 
    
This product is also available as part of the VMS Consolidated Software 
Distribution on CDROM.

ORDERING INFORMATION

Software Licenses: QL-363A*-**
Software Media: QA-363A*-**
Software Documentation: QA-363AA-GZ
Software Product Services: QT-363A*-**

* Denotes variant fields.  For additional information on available licenses, 
  services and media refer to the appropriate price book.

The above information is valid at time of release.  Please contact your local 
DIGITAL office for the most up-to-date information.


BH-LT53B-TE
