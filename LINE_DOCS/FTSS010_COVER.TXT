 






                                AV-NL36A-TE

             Before You Install VAXft System Services Software

          Before you install your VAXft System Services software, please
          read the online Release Notes you received with the prod-
          uct and assemble this software documentation kit.

          VAXft System Services Release Notes

          Release Notes are available on the software medium, and can
          be printed or displayed as an ASCII text file. To view the
          file FTSS010.Release_Notes, use the OPTIONS N qualifier dur-
          ing your installation, as defined in the VAXft System Ser-
          vices Installation Guide. During installation, the file is
          copied to the HELP directory on your VAXft 3000 system.

          Software Documentation Kit Contents

          This kit contains two binders, a spine insert package, and
          five manuals.

          Software Documentation Kit Assembly

          To assemble this kit:

          1. Place each spine insert in the spine pocket of a binder.

          2. Place this Before You Install letter and the Software
             Product Description in the inside pocket of the System
             Management binder.

          3. Insert on the rings of the System Management binder two
             books in the following order:

             o  VAXft System Services Manager's Guide

             o  VAXft System Services Installation Guide

          4. Insert on the rings of the Reference Information binder
             three books in the following order:

             o  VAXft System Services Reference Manual

 






                Place internal dividers within this book as indicated
                with black-barred locator pages. The internal dividers
                begin the following sections:

                -  VAXft System Services DCL Commands

                -  FTSS$CONTROL

                -  Failover Set Manager

             o  VAXft System Services Error Message Manual

             o  VAXft System Services Master Index

          � Digital Equipment Corporation. 1990. All Rights Reserved.

 






          VMS Installation

          The VMS Upgrade and Installation Supplement: VAXft 3000 which
          you have received with your VAXft 3000 VMS kit will be in
          the binder of the VMS documentation set that contains the
          VMS V5.4 Release Notes.

                                    NOTE

             You must install the VMS operating system and VMS Vol-
             ume Shadowing before you install VAXft System Ser-
             vices software.

          Overview and Hardware Documentation

          For overview information on the VAXft 3000 system, see the
          VAXft 3000 Guide to VAX Fault Tolerant Systems you have re-
          ceived as part of the hardware documentation with your VAXft
          3000 system.
