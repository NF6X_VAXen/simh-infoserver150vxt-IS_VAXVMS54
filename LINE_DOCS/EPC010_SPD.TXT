          Software
          Product
          Description

          ________________________________________________________________

          PRODUCT NAME:  DECtrace for VMS, Version 1.0        SPD 25.G1.00

          DESCRIPTION

          DECtrace is a VMS layered product that collects and reports on
          event-based data. This data is helpful for performance analysis,
          capacity planning, database tuning, error logging and other ar-
          eas in which detailed application or layered product specific
          performance and/or event information is useful. DECtrace event
          data can be gathered from any VMS layered product or combination
          of products that contains DECtrace service routine calls. Cus-
          tomers can also put DECtrace event calls in their application
          code to collect event and performance data. DECtrace is designed
          to operate with minimal performance impact on the system and
          can be used in both development and production environments.
          DECtrace supports both single-node and VAXcluster environments.

          DECtrace considers an event to be an application-defined occur-
          rence. An event can have a start and an end (duration event) or
          can simply occur (point event). DECtrace allows events within
          layered products or customer application programs to be defined
          and data items to be associated with each event. These data
          items can be resource utilization statistics (for example, CPU
          time). Additionally, data items specific to the Digital layered
          product, third party layered product, or customer application
          can be associated with each event. These facility-specific data
          items might include information supporting database tuning,
          application-level error logging, and a range of other informa-
          tion logging needs.

          DECtrace provides two major components:

          o  A set of service routines - Calls to these services, repre-
             senting the application events, are placed within the layered
             application software.

          o  The DECtrace administration component - Through the command
             line interface, the user can register the layered products
             and layered applications (called facilities), selectively
             collect from one or more of these facilities, display infor-
             mation about facilities and collections, and later format the
             data into the DECtrace VAX Rdb/VMS database or RMS file.

          DECtrace provides detail, summary, and frequency reports based
          upon the formatted event data in the DECtrace VAX Rdb/VMS
          database. Since the event data can be stored in a VAX Rdb/VMS
          database or an RMS file, users can write customized reports
          using Datatrieve, DECdecision, VAX RALLY, other 4GL tools, or
          standard 3GL languages such as VAX COBOL. The DECtrace formatted
          VAX Rdb/VMS database definitions can optionally be stored in VAX
          CDD/Plus. DECtrace event data can also be used by other applica-
          tions such as DEC RdbExpert, which optimizes the performance of
          VAX Rdb/VMS and VAX DBMS databases.

          DECtrace differs from other collectors; it is event based while
          most other collectors are timer-based. An event-based collector
          collects data at defined locations in the application code each
          time that code is executed. Timer-based collectors perform data
          collection at random places within the code.

          DECtrace advantages:

          o  Provides an easy way to collect and report on the actual
             resources used for each event.

          o  Determines the actual frequency of execution of events rather
             than an average or estimated frequency.


          DECtrace does not attempt to analyze or modify the performance
          of an application. Its function is to collect and report the
          event data. Interpreting the reports is the responsibility of
          the user. Descriptions of the DECtrace data collected from Dig-
          ital Layered products are available in the Using DECtrace with
          Digital Products guide.  For a current list of supported layered
          products, refer to the System Support Addemdum (SSA 25.G1.00-A)

          Features

          DECtrace allows application or layered product software develop-
          ers to:

          o  Define events and data items specific to their software.

          o  Place DECtrace service routine calls at appropriate locations
             in the program source code. These locations are determined by
             the type of information to be collected and the way it will
             be used and/or interpreted by the user.

          o  Register these application programs with DECtrace.

          Collection can be turned on and off from the command line inter-
          face.

          Users of DECtrace data may consist of applications developers,
          applications performance analysts, database administrators,
          system managers, capacity planners, and others interested in
          event or performance data. The DECtrace user can:

          o  Display information about facilities and collections.

          o  Collect event data and format it into a VAX Rdb/VMS database
             or RMS file.

          o  Record the DECtrace formatted VAX Rdb/VMS database defini-
             tions into CDD/Plus, if CDD/Plus is installed on the system.

          o  Merge data from different collection instances.

          o  Produce default detail, frequency, and summary reports from
             the Rdb/VMS formatted database.

          o  Generate customized reports from the VAX Rdb/VMS database 
             or RMS data file using 3GL or 4GL language products.

          HARDWARE REQUIREMENTS

          VAX, MicroVAX, VAXserver or VAXstation configuration as speci-
          fied in the System Support Addendum (SSA 25.G1.00-x).

          SOFTWARE REQUIREMENTS

          For Systems Using Terminals (DECwindows not required):

          o  VMS Operating System

          o  VAX Rdb/VMS  Run-Time License. The Run-Time
             License is required for DECtrace to create and access its
             administration and formatted databases.

          Note: The VAX Rdb/VMS Run-Time License is included with VMS
          V5.1, however, the user needs the VAX Rdb/VMS Run-Time media to
          obtain the software.

          Refer to the System Support Addendum (SSA 25.G1.00-x) for avail-
          ability and required versions of prerequisite/optional software.

          Country Components Software

          None

          ORDERING INFORMATION

          Software Licenses: QL-VW2A*-**
          Software Media: QA-VW2AA-**
          Software Documentation: QA-VW2AA-GZ
          Software Product Services: QT-VW2A*-**

          *  Denotes variant fields. For additional information on avail-
             able licenses, services and media, refer to the appropriate
             price book.

          SOFTWARE LICENSING

          Full Development Option

          DECtrace supports only a full development option. This option
          provides a library of DECtrace service routine calls and the
          ability to register facilities, schedule collections, format the
          collected data, and report it.

          This software is furnished under the licensing provisions of the
          Digital Equipment Corporation's Standard Terms and Conditions.

          For more information about Digital licensing terms and policies,
          contact your local Digital office.

          LICENSE MANAGEMENT FACILITY SUPPORT

          This layered product supports the VMS License Management Facil-
          ity.

          License units for this product are allocated on a CPU-capacity
          and Clusterwide basis.

          For more information on the License Management Facility, refer
          to the VMS Operating System Software Product Description (SPD
          25.01.xx) or the License Management Facility manual of the VMS
          Operating System documentation set.

          For more information about Digital licensing terms and policies,
          contact your local Digital office.

          SOFTWARE PRODUCT SERVICES

          A variety of service options are available from Digital. For
          more information, contact your local Digital office.

          SOFTWARE WARRANTY

          Warranty for this software product is provided by Digital with
          the purchase of a license for the product as defined in the
          Software Warranty Addendum of this SPD.

          [R]  The DIGITAL Logo is a registered trademark of Digital
               Equipment Corporation.

          [TM] DECdecision, DECtrace, MicroVAX, Rdb/VMS, VAX RALLY, VAX
               CDD/Plus, VAX DBMS, VAXserver and VAXstation are trademarks
               of Digital Equipment Corporation.

	
