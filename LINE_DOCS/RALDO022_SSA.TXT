 






     System
     Support
     Addendum

     ________________________________________________________________
     ________________________________________________________________

     PRODUCT NAME:  VAX RALLY, Version 2.2             SSA 27.03.05-A

     HARDWARE REQUIREMENTS

     Processors Supported

     VAX:      VAXft 3000-310

               VAX 4000 Model 300

               VAX 6000 Model 200 Series,
               VAX 6000 Model 300 Series,
               VAX 6000 Model 400 Series,
               VAX 6000 Model 500 Series

               VAX 8200, VAX 8250, VAX 8300, VAX 8350,
               VAX 8500, VAX 8530, VAX 8550, VAX 8600,
               VAX 8650, VAX 8700, VAX 8800, VAX 8810,
               VAX 8820, VAX 8830, VAX 8840

               VAX 9000-210, VAX 9000-410

               VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

     MicroVAX: MicroVAX II, MicroVAX 2000,
               MicroVAX 3100, MicroVAX 3300,
               MicroVAX 3400, MicroVAX 3500,
               MicroVAX 3600, MicroVAX 3800,
               MicroVAX 3900



                                  DIGITAL                January 1991

                                                          AE-LX94F-TE

 


     VAX RALLY, Version 2.2                            SSA 27.03.05-A




     VAXstationVAXstation II, VAXstation 2000,
               VAXstation 3100 Series, VAXstation 3200,
               VAXstation 3500, VAXstation 3520,
               VAXstation 3540

     VAXserver:VAXserver 3100, VAXserver 3300,
               VAXserver 3400, VAXserver 3500,
               VAXserver 3600, VAXserver 3602,
               VAXserver 3800, VAXserver 3900

               VAXserver 4000 Model 300

               VAXserver 6000-210, VAXserver 6000-220,
               VAXserver 6000-310, VAXserver 6000-320,
               VAXserver 6000-410, VAXserver 6000-420,
               VAXserver 6000-510, VAXserver 6000-520

     Processors Not Supported

     VAX-11/725, VAX-11/782, MicroVAX I, VAXstation I, VAXstation
     8000

     Processor Restrictions

     A TK50 Tape Drive is required for standalone MicroVAX 2000 and
     VAXstation 2000 systems.

     A minimum of 4 MB of memory is required, although typical con-
     figurations will require more than 4 MB of memory. There is a
     recommendation of 0.75 MB per concurrent user.

     Other Hardware Required

     Terminals

     VAX RALLY Development Option and VAX RALLY Run-Time Option can
     be run on any VT100 (with AVO), VT100 compatible terminal emula-
     tors on VAXstations, VT102, VT125 (with AVO), VT220, VT240/241,
     VT320, VT330, VT340, or VT420 terminal.

                                     2

 


     VAX RALLY, Version 2.2                            SSA 27.03.05-A



     Disk Space Requirements (Block Cluster Size =1):

     Disk space required for installation:

     VAX RALLY Development Option:

     With Examples:               26,000 blocks
                                  (13.3 Mbytes)

     Without Examples:            18,000 blocks
                                  (9.2 Mbytes)

     VAX RALLY Run-Time Option:

     With Examples:               10,000 blocks
                                  (5.1 Mbytes)

     Without Examples:            7,000 blocks
                                  (3.6 Mbytes)

     Disk space required for use (permanent):

     VAX RALLY Development Option:

     With Examples:               20,000 blocks
                                  (10.2 Mbytes)

     Without Examples:            15,000 blocks
                                  (7.7 Mbytes)

     VAX RALLY Run-Time Option:

     With Examples:               8,000 blocks
                                  (4.1 Mbytes)

     Without Examples:            2,000 blocks
                                  (1.0 Mbytes)


                                     3

 


     VAX RALLY, Version 2.2                            SSA 27.03.05-A



     In addition, VAX RALLY Development Option includes a text-based
     instruction course (TBI) entitled "Getting Started with VAX
     RALLY." If this course is installed, it requires:

     Disk space required for      6,000 blocks
     installation:
                                  (3.1 Mbytes)

     Disk space required for      3,000 blocks
     use (permanent):
                                  (1.5 Mbytes)

     Note: These amounts are IN ADDITION to the amounts listed previ-
     ously.

     These counts refer to the disk space required on the system
     disk. The sizes are approximate; actual sizes may vary depending
     on the users' system environment, configuration, and software
     options.

     OPTIONAL HARDWARE

     Any device supported by the prerequisite/optional software.

     CLUSTER ENVIRONMENT

     This layered product is fully supported when installed on any
     valid and licensed VAXcluster* configuration without restric-
     tions. The HARDWARE REQUIREMENTS sections of this product's
     Software Product Description and System Support Addendum detail
     any special hardware required by this product.

     *  V5.x VAXcluster configurations are fully described in the
        VAXcluster Software Product Description (29.78.xx) and in-
        clude CI, Ethernet, and Mixed Interconnect configurations.




                                     4

 


     VAX RALLY, Version 2.2                            SSA 27.03.05-A



     SOFTWARE REQUIREMENTS

     For Systems Using Terminals (No DECwindows Interface):

     VMS Operating System V5.2-V5.4

     For Workstations Running VWS:

     VMS Operating System V5.2-V5.4
     VMS Workstation Software V4.2-V4.3

     For Workstations Running DECwindows:

     VMS Operating System V5.2-V5.4 (and necessary components of VMS
     DECwindows)

     This product may run under DECterm in either of the following
     ways:

     o  Stand-alone execution-running the X11[TM] display server and
        the client application on the same machine.

     o  Remote execution-running the X11 display server and the
        client application on different machines.

     VMS DECwindows is part of the VMS Operating System but must be
     installed separately on versions prior to V5.4. Installation of
     VMS DECwindows gives you the option to install any or all of the
     following three components:

     o  VMS DECwindows Compute Server (Base kit; includes runtime
        support)

     o  VMS DECwindows Device Support

     o  VMS DECwindows Programming Support

     For stand-alone execution, the following DECwindows components
     must be installed on the machine:

     o  VMS DECwindows Compute Server

     o  VMS DECwindows Device Support

                                     5

 


     VAX RALLY, Version 2.2                            SSA 27.03.05-A



     o  VMS DECwindows Programming Support

     For remote execution, the following DECwindows components must
     be installed on the machine:

     Server Machine

     o  VMS DECwindows Compute Server

     o  VMS DECwindows Device Support

     o  VMS DECwindows Programming Support

     Client Machine

     o  VMS DECwindows Compute Server

     o  VMS DECwindows Device Support

     o  VMS DECwindows Programming Support

     Layered Products

     VAX Rdb/VMS (Interactive or Development System) V3.1B-V4.0 is
     required for the VAX RALLY Development Option. VAX Rdb/VMS V4.0
     is required for the Two-Phase Commit Option. VAX Rdb/VMS (Run-
     Time, Interactive or Development) is required for the VAX RALLY
     Run-Time Option if the application uses VAX Rdb/VMS. If using
     the VAX RALLY Run-Time Option with RMS files only, VAX Rdb/VMS
     is NOT required.

     VAX CDD/Plus V4.0-V4.2A is required for the VAX RALLY Develop-
     ment Option, but not required for the VAX RALLY Run-Time Option.

     VMS Tailoring

     For VMS V5.x systems, the following VMS classes are required for
     full functionality of this layered product:

     o  VMS Required Saveset

     o  Secure User's Environment

                                     6

 


     VAX RALLY, Version 2.2                            SSA 27.03.05-A



     o  Programming Support (Not required for VAX RALLY Run-Time
        Option)

     For more information on VMS classes and tailoring, refer to
     the VMS Operating System Software Product Description (SPD
     25.01.xx).

     OPTIONAL SOFTWARE

     ALL-IN-1 V2.3
     VAX TEAMDATA V1.4
     VAX DATATRIEVE V5.1
     VAX DBMS V4.2
     VAX Language-Sensitive Editor/
        Source Code Analyzer V3.1


     GROWTH CONSIDERATIONS

     The minimum hardware/software requirements for any future ver-
     sion of this product may be different from the requirements for
     the current version.

     DISTRIBUTION MEDIA

     Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

     This product is also available as part of the VMS Consolidated
     Software Distribution on CDROM.

     ORDERING INFORMATION

     VAX RALLY Development Option (includes VAX RALLY Run-Time Op-
     tion):

     Software Licenses: QL-A86A*-**
     Software Media: QA-A86A*-**
     Software Documentation: QA-A86AA-GZ
     Software Product Services: QT-A86A*-**

                                     7

 


     VAX RALLY, Version 2.2                            SSA 27.03.05-A



     VAX RALLY Run-Time Option:

     Software Licenses: QL-VF4A*-**
     Software Media: QA-VF4A*-**
     Software Documentation: QA-VF4AA-GZ
     Software Product Services: QT-VF4A*-**

     *  Denotes variant fields. For additional information on avail-
        able licenses, services, and media, refer to the appropriate
        price book.

     The above information is valid at time of release. Please con-
     tact your local Digital office for the most up-to-date informa-
     tion.

     [TM] X11 is a trademark of Massachusetts Institute of Technol-
          ogy.
     [TM] The DIGITAL Logo, ALL-IN-1, CI, DATATRIEVE, DBMS, DECterm,
          DECwindows, MicroVAX, RALLY, Rdb/VMS, TEAMDATA, TK50, VAX,
          VAX ACMS, VAX CDD/Plus, VAX DOCUMENT, VMS, VAXstation,
          VAXserver, VAXcluster, VT, and WPS-PLUS are trademarks of
          Digital Equipment Corporation.

















                                     8
