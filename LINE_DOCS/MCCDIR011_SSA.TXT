 






     System
     Support
     Addendum

     ________________________________________________________________
     ________________________________________________________________

     PRODUCT NAME:  DECmcc Director, Version 1.1       SSA 32.46.01-A

     HARDWARE REQUIREMENTS

     Processors Supported

     VAX:      VAXft 3000-310
               VAX 4000 Model 200 Series,
               VAX 4000 Model 300 Series,
               VAX 6000 Model 200 Series,
               VAX 6000 Model 300 Series,
               VAX 6000 Model 400 Series,
               VAX 9000-210, VAX 9000 Model 400 Series

               VAX 8200, VAX 8250, VAX 8300, VAX 8350,
               VAX 8500, VAX 8530, VAX 8550, VAX 8600,
               VAX 8650, VAX 8700, VAX 8800, VAX 8810,
               VAX 8820, VAX 8830, VAX 8840

               VAX-11/750, VAX-11/780, VAX-11/785

     MicroVAX: MicroVAX II, MicroVAX 2000,
               MicroVAX 3100, MicroVAX 3300,
               MicroVAX 3400, MicroVAX 3500,
               MicroVAX 3600, MicroVAX 3800,
               MicroVAX 3900






                                  DIGITAL                  March 1991

                                                          AE-PC7KB-TE

 


     DECmcc Director, Version 1.1                      SSA 32.46.01-A




     VAXstationVAXstation II, VAXstation 2000,
               VAXstation 3100 Series, VAXstation 3200,
               VAXstation 3500, VAXstation 3520,
               VAXstation 3540

     VAXserver:VAXserver 3100, VAXserver 3300,
               VAXserver 3400, VAXserver 3500,
               VAXserver 3600, VAXserver 3602,
               VAXserver 3800, VAXserver 3900

               VAXserver 4000 Model 300
               VAXserver 6000-210, VAXserver 6000-220,
               VAXserver 6000-310, VAXserver 6000-320,
               VAXserver 6000-510, VAXserver 6000-520

     Processors Not Supported

     VAX-11/725, VAX-11/730, VAX-11/782, MicroVAX I, VAXstation I,
     VAXstation 8000

     Processor Restrictions

     A TK50 Tape Drive is required for standalone MicroVAX 2000 and
     VAXstation 2000 systems.

     Disk Space Requirements (Block Cluster Size = 1):

     Disk space required for      62,000 blocks
     installation:
                                  (31,744K bytes)

     Disk space required for      60,000 blocks
     use (permanent):
                                  (30,720K bytes)




                                     2

 


     DECmcc Director, Version 1.1                      SSA 32.46.01-A



     These counts refer to the disk space required on the system
     disk. The sizes are approximate; actual sizes may vary depending
     on the user's system environment, configuration, and software
     options.

     OPTIONAL HARDWARE

     VT1300 Color Terminal is supported.

     CLUSTER ENVIRONMENT

     This layered product is fully supported when installed on any
     valid and licensed VAXcluster* configuration without restric-
     tions. The HARDWARE REQUIREMENTS sections of this product's
     Software Product Description and System Support Addendum detail
     any special hardware required by this product.

     *  V5.x VAXcluster configurations are fully described in the
        VAXcluster Software Product Description (29.78.xx) and in-
        clude CI, Ethernet and Mixed Interconnect configurations.

     SOFTWARE REQUIREMENTS

     VMS Operating System V5.3 - V5.4

     Layered Products:

     DECnet-VAX V5.3 - V5.4
     VAX Distributed Name Service V1.1

     For Workstations Running DECwindows:

     VMS Operating System V5.3 - V5.4






                                     3

 


     DECmcc Director, Version 1.1                      SSA 32.46.01-A



     Layered Products:

     DECnet-VAX V5.3 - V5.4
     VAX Distributed Name Service V1.1

     VMS Tailoring

     For VMS V5.x, the following VMS classes are required for full
     functionality of this layered product:

     o  VMS Required Saveset

     o  Network Support

     o  Programming Support

     o  Utilities

     For more information on VMS classes and tailoring, refer to
     the VMS Operating System Software Product Description (SPD
     25.01.xx).

     OPTIONAL SOFTWARE

     None

     GROWTH CONSIDERATIONS

     The minimum hardware/software requirements for any future ver-
     sion of this product may be different from the minimum require-
     ments for the current version.

     DISTRIBUTION MEDIA

     Tape: TK50 Streaming Tape

     ORDERING INFORMATION

     Software Licenses: QL-VM9A9-AA
     Software Media: QA-VM9AA-H5
     Software Documentation: QA-VM9AA-GZ
     Software Product Services: QT-VM9A*-**

                                     4

 


     DECmcc Director, Version 1.1                      SSA 32.46.01-A



     *  Denotes variant fields. For additional information on avail-
        able licenses, services and media, refer to the appropriate
        price book.

     The above information is valid at time of release. Please con-
     tact your local Digital office for the most up-to-date informa-
     tion.

     [TM]  The DIGITAL Logo, VAX, MicroVAX, VAXstation, VAXserver,
           VMS, DECmcc and DECnet are trademarks of Digital Equipment
           Corporation.




























                                     5
