

HARDWARE REQUIREMENTS

Processors Supported

VAX:	 VAX 6000 Model 200 Series, VAX 6000 Model 300 Series, VAX 6000 Model 
         400 Series

	 VAX 8200, VAX 8250, VAX 8300, VAX 8350, VAX 8500, VAX 8530 VAX 8550, 
         VAX 8600, VAX 8650, VAX 8700, VAX 8800, VAX 8810 VAX 8820, VAX 8830, 
         VAX 8840, VAX 8842, VAX 8974, VAX 8978

	 VAX-11/750, VAX-11/780, VAX-11/785

MicroVAX:     MicroVAX II, MicroVAX 2000, MicroVAX 3100, MicroVAX 3300, 
              MicroVAX 3400, MicroVAX 3500, MicroVAX 3600, MicroVAX 3800, 
              MicroVAX 3900

VAXstation:   VAXstation II, VAXstation 2000, VAXstation 3100, VAXstation 3200, 
              VAXstation 3500, VAXstation 3520, VAXstation 3540, VAXstation 
              8000

Processor Restriction

A TK50 Tape Drive is required for standalone MicroVAX 2000 and VAXstation 2000 
systems.

Processors Not Supported

VAX-11/725, VAX-11/730, VAX-11/782, MicroVAX I, VAXstation I VAXserver 
6000-210, VAXserver 6000-310,  VAXserver 6000-410, VAXserver 6000-420

Block Space Requirements (Block Cluster Size = 1):

Disk space required for installation				   1,200 blocks
								   (614K bytes)

Disk space required for use					     600 blocks
								   (307K bytes)

These counts refer to the disk space required on the system disk. The sizes are 
approximate;  actual sizes may vary depending on the user's system environment, 
configuration, and software options.

CLUSTER ENVIRONMENT

This layered product is fully supported when installed on any valid and 
licensed VAXcluster* configuration without restrictions.  The HARDWARE  
REQUIREMENTS sections of this product's Software Product Description and System 
Support Addendum detail any special hardware required by this product.

*     V5.x VAXcluster configurations are fully described in the VAXcluster 
      Software Product Description (29.78.xx) and include CI, Ethernet, and 
      Mixed Interconnect configurations.

SOFTWARE REQUIREMENTS

VMS Operating System V5.0 - V5.3

VAX CIT Applications Interface V2.0 (CIT V2.0) must be installed on the same 
node as the CITMD software.

VAX CIT Server V2.0 (CITSR V2.0) software must be installed either on the same 
node as the CIT Applications Interface software or on another node linked by 
DECnet to the node with the CIT Applications Interface software. 

VMS Tailoring Classes

For VMS V5.x systems, the following VMS classes are required for full 
functionality of this layered product:

VMS Required Saveset
Network Support
Utilities
 
For more information on VMS classes and tailoring, refer to the VMS Operating 
System Software Product Description (SPD 25.01.xx).

GROWTH CONSIDERATIONS

The minimum hardware/software requirements for any future version of this 
product may be different from the minimum requirements for the current version.

DISTRIBUTION MEDIA

Tape:	9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

This product is also available as part of the VMS Consolidated Software 
Distribution on CDROM.

ORDERING INFORMATION

Software Licenses:  QL-YG8A*-**
Software Media: QA-YG8AA-**
Software Documentation: QA-YG8AA-GZ
Software Product Services:  QT-YG8A*-**

*     Denotes variant fields.  For additional information on available 
      licenses, services and media, refer to the appropriate price book.

The above information is valid at time of release.  Please contact your local 
Digital office for the most up-to-date information.

F

RO   The DIGITAL Logo is a registered trademark of Digital Equipment 
Corporation.

TM   VMS, MicroVAX, VAX, VAXstation, VAXserver and VAXcluster are trademarks of 
Digital Equipment Corporation.
F
