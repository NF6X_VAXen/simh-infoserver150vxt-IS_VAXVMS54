 






                     VMS Workstation Software V4.3 Cover Letter

                               Order No.: AV-JK76J-TE

          Workstations Engineering is happy to introduce V4.3 of VMS
          Workstation Software.

          In this kit, you will find all software necessary to install VWS
          V4.3 on your workstation.

                                        NOTE

             This V4.3 kit is not limited to VWS. It includes compo-
             nents that will aid you in migrating from VWS to DECwin-
             dows.

             o  VAXuisx Run-Time Library for VMS (NEW)

             o  UIS Source Code Annotator

             o  UIS to DDIF Converter

             o  DECwindows X11 Server

             o  Guide to Migrating VWS Applications to DECwindows

          VMS Workstation Software V4.3 incorporates both new and revised
          documentation.

          New Manuals:

          o  VAXuisx Release Notes

          o  VAXuisx User's Guide

          o  UIS Source Code Annotator

          Revised Manuals:

          o  VMS Workstation Software Read Me First, AV-GS96J-TE

          o  Installation Guide for VMS Workstation Software and Migration
             Tools, AA-HX11J-TE (This now includes installation procedures
             for all VWS and Migration products.)

                                                                         1

 






          o  VMS Workstation Software Release Notes, AA-KP45F-TE

          Before you attempt to install the VMS Workstation Software V4.3
          kit, be sure to read the "Read Me First" pages included with
          your documentation and any relevant portions of the installation
          guide.

                            CONTENTS OF YOUR SOFTWARE KIT

          Your VWS kit consists of one the following media:

          o  A set of 22 RX33 floppy disks

          o  A TK50 tape cartridge

          o  A 9 track magnetic tape




          ________________________________________________________________

          Copyright � 1990 Digital Equipment Corporation
















          2

 






                                  PROGRAMMING NOTE:

          In the VWS V4.1 release, all the DOP definitions in VWSSYSDEF
          were inadvertently changed to uppercase. This created an in-
          compatibility for case-sensitive programming languages. The VWS
          development team apologizes for any inconvenience this might
          have caused you. The DOP definitions will remain uppercase for
          all future releases.

                                      REMEMBER:

          o  To use VWS V4.3, you must have VMS V5.0 or above installed on
             your system.

          o  To use SIGHT, HCUIS or the VWS DEMO Kit, you must have VMS
             V5.0 or above installed on your system.

          o  To use the VAXuisx Run-Time Library, you must have VMS V5.3
             or above installed on your system.

          o  To use the UIS to DDIF Converter and the UIS Source Code
             Annotator, you must have VMS V5.1 or above installed on your
             system.

          o  To use the DECwindows X11 server, you must have VMS V5.1 or
             VMS V5.2 installed on your system.

          o  To use some of the migraton tools in this kit, you must have
             DECwindows installed on your system. If you are running VMS
             V5.1, you must install DECwindows separately. If you are
             running VMS V5.2 or VMS V5.3, DECwindows can be installed as
             part of the normal VMS installation.

                                        NOTE

             After V4.3, RX33 distribution kits will no longer be
             available as distribution media for VWS or VWS Migration
             Tools.

                                       CAUTION

             If you receive the following warning message:

                                                                         3

 






                  %SYSTEM-W-NOSUCHFILE, no such file
                  \VMI$ROOT:[SYSLIB]UISSHR.EXE

             and it causes your installation to fail, please reinstall
             VWS after executing the following commands:

                  $ INSTALL REMOVE SYS$SHARE:UISSHR.EXE
                  $ INSTALL CREATE SYS$SHARE:UISSHR.EXE

             The original installation failed because INSTALL could no
             longer locate the file associated with the installed file
             header information. The above commands will ensure that
             INSTALL can find the current copy of the file.


























          4
