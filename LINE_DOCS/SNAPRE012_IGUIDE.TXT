 












                                                                 Chapter 1


                                                Preparing for Installation
          ________________________________________________________________


             Before you install the DECnet/SNA VMS Printer Emulator,
             referred to as PrE, you should prepare your system.

             This installation involves writing to your system disk.
             Digital Equipment Corporation recommends that you make a
             backup copy of your system disk before you begin.

                                          NOTE

                 Unless stated otherwise, the term Gateway refers to
                 the DECnet/SNA Gateway or VMS/SNA when used in this
                 manual.

          __________________________________________________________

          1.1  Checking the Distribution Kit

             Before beginning the installation, check that you have a
             complete distribution kit by comparing the kit against the
             bill of materials (BOM). If any part of the kit is missing,
             contact your Digital representative. Each PrE kit consists
             of one or more volumes of software media (depending on the
             media) and a set of documentation.



                                           Preparing for Installation  1-1

 





          __________________________________________________________

          1.2  VMS System Requirements

             This section discusses VMS requirements. Before you begin to
             install the PrE software, you need to know what software,
             system privileges, and disk space are required for your
             installation.

          __________________________________________________________

          1.2.1  Installation Requirements

             Table 1-1 lists the installation requirements that you must
             satisfy before you can begin to install the PrE software.

          Table_1-1:__PrE_Software_Installation_Specifications____________

          Requirements__________Specifications____________________________

          Distribution media    TK50 tape cassette distribution kit, or
                                magnetic tape (1600 bpi) distribution kit

          Required software     VMS V5.0 or V5.1
                                DECnet-VAX V5.0 or later

          Privileges            System manager level

          Disk space            450 blocks minimum during installation
                                400 blocks minimum after installation

          BYTCNT quota          7000 bytes minimum

          Estimated time to     5 to 15 minutes
          install

          PAK                   Product Authorization Key for SNA-PRE



          1-2  Preparing for Installation

 






          Table_1-1_(Cont.):__PrE_Software_Installation_Specifications____

          Requirements__________Specifications____________________________

          Associated documents  The Guide to VMS Software Installation
                                contains general installation information.

                                The VMS System Manager's Reference Manual
                                contains information on setting recom-
          ______________________mended_user_account_quotas._______________

          __________________________________________________________

          1.2.1.1  VAX Supported Processors

             Refer to the Software Product Description (SPD) for the
             latest official list of supported processors.

          __________________________________________________________

          1.2.1.2  VMS/SNA and Gateway Support

             Refer to the Software Product Description (SPD) or the System
             Support Addendum (SSA) for the latest official list of sup-
             ported versions of VMS/SNA software, and Gateway software and
             hardware.

          __________________________________________________________

          1.2.2  VMS License Management Facility Requirements

             Before you install PrE, you should register and load your
             Product Authorization Key (PAK) with the License Management
             Facility (LMF). The PAK, which contains information about the
             license, is a paper certificate shipped with the product.

             During the installation, you are asked if you have regis-
             tered the PrE license and loaded the appropriate PAK. If you
             have not already done so, you can complete the installation.
             However, PrE and the IVP will not run if you have not reg-
             istered the license or loaded the PAK. Once you perform the
             license registration and have loaded the PAK, you will be
             able to run PrE and the IVP.

                                           Preparing for Installation  1-3

 






             To register and load the license, log in to the system man-
             ager's account. Then type the following:

               @SYS$UPDATE:VMSLICENSE.COM

             When you are prompted for information, enter the data from
             your PAK. For more information on LMF, see the VMS License
             Management Utility Manual in the VMS documentation set.

          __________________________________________________________

          1.2.3  VMS Tailor Requirements

             The required classes are: VMS Required Save Set, Network
             Support, Secure Users, and Utilities. Be certain these
             classes are installed on your system before installing the
             product. Refer to the VMS Tailor documentation in the VMS
             documentation set for more information.

          __________________________________________________________

          1.3  Checking the background process

             If PrE is already installed, use the VMS SHOW SYSTEM command
             to make sure the background process (SNAPREDET) is not
             running. If it is, set your privilege to WORLD and stop the
             process with the STOP/IDENTIFICATION=pid command, where pid
             is the process identity of SNAPREDET.

          __________________________________________________________

          1.4  IVP Requirements

             If you choose to run IVP, make sure you know the following
             values:

              o The Gateway node name (the name must be of a Gateway that
                is running)

              o The access name

              o The circuit name

          1-4  Preparing for Installation

 






              o The session address

                                          NOTE

                 The VMS Software Installation Guide suggests you shut
                 down DECnet before running VMSINSTAL. Note, however,
                 that to run the IVP, DECnet must be running.

          __________________________________________________________

          1.5  VMSINSTAL Requirements

             Installing PrE on a VMS system involves running the VMSINSTAL
             procedure. The dialogue is self-explanatory. The system
             tells you to answer questions and waits for you to tell it
             to continue. Most questions require a simple YES (Y) or NO
             (N) answer. The questions display default answers (where
             applicable) in the following way:

               [YES]:

             To answer a question with the default YES, press <RET>.

             When you invoke VMSINSTAL, it checks that:

              o You are logged in to a privileged account.

              o You have adequate quotas for installation.

              o All users are logged off.

              o DECnet is shut down.

             If VMSINSTAL detects any violations to these conditions at
             the beginning of the installation, it notifies you and asks
             if you want to continue the installation. In some instances,
             you can enter YES to continue. To stop the installation
             process, enter NO or press <RET>. Then change the condition
             and restart the installation.

                                           Preparing for Installation  1-5

 





          __________________________________________________________

          1.5.1  Privileges

             Installation of PrE requires that you have the privileges
             necessary to run VMSINSTAL. Log in to system manager's
             account to install the PrE software.

          __________________________________________________________

          1.5.2  Disk space

             Make sure that you have adequate disk space before you
             install the PrE software. You can check your available disk
             space by entering the following command:

               $ SHOW DEVICE system-disk

             where system-disk refers to the device name of the system
             disk. For more information about the SHOW DEVICE command,
             refer to the VMS DCL Dictionary.

          __________________________________________________________

          1.6  Accessing the On-line Release Notes

             This product provides on-line release notes. You should
             review the release notes before installing the product.
             They contain the latest enhancements to the product, which
             may include changes to the installation procedure. If
             you specify OPTIONS N with VMSINSTAL, you are prompted
             for how you would like to see the release notes. After
             installing the product, you can read the release notes in
             the SYS$HELP:SNAPRE$vvu.RELEASE_NOTES, where vv is the major
             software version number and u is the update software version
             number. For example, SNAPRE$012 is version 1.2.




          1-6  Preparing for Installation

 












                                                                 Chapter 2


                                               Installing the PrE Software
          ________________________________________________________________


             This chapter describes how to install DECnet/SNA VMS Printer
             Emulator, which this chapter refers to as PrE. It contains a
             step-by-step description of the installation considerations.
             The final section includes an actual system installation log
             using PAK information and VMSINSTAL.

          __________________________________________________________

          2.1  The Installation Procedure

             The PrE installation consists of a series of questions and
             informational messages. The process takes 5 to 15 minutes to
             complete.

          __________________________________________________________

          2.1.1  Running VMSINSTAL

             Digital recommends that you use a hard-copy terminal for
             installing PrE if you would like a copy of the installation
             process. If you don't have a hard-copy terminal, you can
             produce a copy of the installation procedure in a file by
             typing:

               $ SET HOST 0/LOG=filename

             where:

                                          Installing the PrE Software  2-1

 






             filename is the name of the file in which you want the log
             file stored.

             Step 1: Log in to system manager's account.

             To start the installation, invoke the VMSINSTAL command
             procedure from a privileged account, such as the SYSTEM
             account.

               Username: SYSTEM [RET]
               Password:        [RET]

             Step 2: Invoke VMSINSTAL.

             Use the following syntax to invoke VMSINSTAL:

               $ @SYS$UPDATE:VMSINSTAL SNAPREnnn ddcn: OPTIONS N

             Replace nnn in the product name with the version number of
             the software, for example, SNAPRE012.

             Replace ddcn with the name of the device on which you plan
             to mount the media, where dd is the device name, c is the
             controller ID, and n is the unit number.

             OPTIONS N is an optional parameter that indicates you want
             to be prompted to display or print the release notes, or
             copy them to SYS$HELP. If you do not include the OPTIONS N
             parameter, VMSINSTAL does not prompt you to display, print,
             or copy the release notes. Please read the release notes
             before proceeding with this installation. The following
             message displays:

                          VAX/VMS Software Product Installation Procedure Vn.n

               It is dd-mmm-yyyy at hh::mm.
               Enter a question mark (?) at any time for help.


          2-2  Installing the PrE Software

 






             Step 3: Choose to continue the installation.

             DECnet should be shut down before beginning the installation.
             A DECnet warning message displays if DECnet software is
             running.

               %VMSINSTAL-W-DECNET, Your DECnet network is up and running.
               * Do you want to continue anyway [NO]? YES

             Step 4: Check your system disk backup.

             Verify that you are satisfied with the backup copy of your
             system disk before continuing with the installation.

               * Are you satisfied with the backup of your system disk [YES]?

             Step 5: Mount your media.

             When installing from the distribution medium (not from copied
             savesets) VMSINSTAL checks that the device is mounted. Then
             you are prompted to mount the distribution volume, and asked
             if you are ready to continue the installation.

               Please mount the first volume of the set on ddcn:
               * Are you ready? YES

             Step 6: Product Installation Begins.

             VMSINSTAL displays a message that the media containing
             PrE has been mounted on the specified device and that the
             installation has begun.

               %MOUNT-I-MOUNTED, SNAPRE mounted on _ddcn:
               The following products will be processed:
               SNAPRE Vn.n

                     Beginning installation of SNAPRE Vn.n at hh::mm

               %VMSINSTAL-I-RESTORE, Restoring product saveset A...

             Vn.n is automatically replaced with the version number of
             PrE, for example, V1.2.

                                          Installing the PrE Software  2-3

 






             Step 7: Select a release notes option.

             If you specified OPTIONS N when you invoked VMSINSTAL,
             the installation procedure prompts you for a release notes
             option.

               Release Notes Options:

                   1.  Display release notes
                   2.  Print release notes
                   3.  Both 1 and 2
                   4.  Copy release notes to SYS$HELP
                   5.  Do not display, print or copy release notes.

               Select option [2]:

             If you select option 1, VMSINSTAL displays the release notes
             immediately on your terminal. You can terminate the display
             at any time by pressing <CTRL/C>.

             If you select option 2 or 3, VMSINSTAL prompts you for a
             print queue name:

               Queue name [SYS$PRINT]:

             Enter a queue name or press RETURN to send the file to the
             default output print device, SYS$PRINT.

             No matter which option you select, VMSINSTAL then asks you if
             you want to continue the installation. Answer YES to continue
             or NO to exit VMSINSTAL.

               * Do you want to continue the installation? YES
               %VMSINSTAL-I-RELMOVED, The products release notes have been successfully
               moved to SYS$HELP.




          2-4  Installing the PrE Software

 






             Step 8: Register the product with the License Management
             Facility.

             The installation procedure prints out information specific to
             the particular license and asks if you have registered and
             loaded your PAK for PrE.

               Product:              SNA-PRE
               Producer:             DEC
               Version:              n.n
               Release Date:         dd-mmm-yyyy

               * Does this product have an authorization key registered and loaded?

             If you have not registered and loaded your PAK, answer NO to
             this question. The installation reminds you to register the
             PAK before you run the product. The installation continues.

             Step 9: Choose to purge files.

             You have the option to purge files from previous versions of
             the PrE that are superseded by this installation. Purging is
             recommended because it will save disk space.

               * Do you want to purge files replaced by this installation [YES]?

             Step 10: Choose to run the IVP.

             The system now asks if you want to have the IVP run
             automatically at the end of the installation. The IVP for
             PrE checks to be sure that the installation is successful.
             You should run the IVP immediately after installation.

               * Do you want to run the IVP after the installation [YES]?





                                          Installing the PrE Software  2-5

 






             Step 11: The IVP is run.

             If you answered YES to the IVP question, the IVP begins
             automatically. The following message displays:

               Starting PrE Installation Verification Procedure (IVP)

             When the IVP begins, you must supply the access name,
             gateway, and session address. It then performs the following:

              1.Creates a characteristics file.

              2.Displays the characteristics in this file.

              3.Starts a connection.

              4.Stops the connection.

              5.Displays the even log file for that connection.

             Make sure that the only messages in the event log file are
             the SNAPRE-CONNECT and SNAPRE-STOPEOS messages, and possibly
             the SNAPRE-SESACT and SNAPRE-SESNOLACT messages.

             The following messages indicate that the entire installation
             procedure is complete:

                     Installation of SNAPRE Vn.n completed at hh::mm

                     VMSINSTAL procedure done at hh::mm

          __________________________________________________________

          2.2  Postinstallation Considerations

             Verification of the installation can be done either during
             installation through the VMSINSTAL utility or at any other
             time by running the SNAPRE$IVP.COM command procedure found in
             SYS$TEST. This command procedure verifies that the PrE has
             been installed correctly on your system. It does not verify
             connection between IBM and the DECnet/SNA Gateway or VMS/SNA.
             Section 2.2.2 shows the command procedure for running the
             IVP.

          2-6  Installing the PrE Software

 





          __________________________________________________________

          2.2.1  Files Created During Installation

             During installation, the files listed below are placed on
             your system.

          Table_2-1:__PrE_Files___________________________________________

          File_Name_______________________Description_____________________

          SYS$HELP:SNAPRE$012.RELEASE_NOTERelease Notes

          SYS$HELP:SNAPREHLP.HLB          PrE help library

          SYS$SYSTEM:SNAPRE.EXE           User interface

          SYS$SYSTEM:SNAPREDET.EXE        Background process

          SYS$STARTUP:SNAPRE$STARTUP.COM  Startup command file

          SYS$STARTUP:SNAPRE$DETACHED.COM Used by the Startup command file

          SYS$MESSAGE:SNAPREMSG.EXE       PrE message file

          SYS$TEST:SNAPRE$IVP.COM         PrE Installation Verification
          ________________________________Procedure_______________________

          __________________________________________________________

          2.2.2  Running the Installation Verification Procedure

             The Installation Verification Procedure (IVP) verifies the
             success of the installation. It starts the detached process,
             starts and stops a connection, and displays the log file. It
             is a first pass at problem detection. If a problem develops
             with PrE, run the IVP first. To run the IVP after you install
             the product, invoke the command:

               $ @SYSTEST:SNAPRE$IVP

                                          Installing the PrE Software  2-7

 






             If the IVP fails, correct the situation, and run the IVP
             again.

          __________________________________________________________

          2.2.3  Starting PrE

             When VMSINSTAL has finished, run the command file
             SNAPRE$STARTUP.COM, which starts the background process.

             Each time the system is rebooted, this command procedure
             must be run on the system where the PrE is installed. To do
             this, add the following command to the system startup file
             SYS$MANAGER:SYSTARTUP_V5.COM:

               @SYS$STARTUP:SNAPRE$STARTUP parameter1 parameter2 parameter3

             where:

             first parameter  defines the disk and directory
                              (or directories) to contain PrE's
                              characteristics files.

             second           defines the file or device to contain PrE's
             parameter        error stream. The error stream records error
                              messages which cannot be sent to the event
                              log file.

             third parameter  when specified, closes the output file when
                              the session ends. When not specified, closes
                              the output file when PrE receives an End
                              Bracket Indicator (EBI)
          .

             For example, both the characteristics files and the error
             logging stream could be held on the system disk in a
             directory called [SNAPRE]. The error logging stream could
             be in a file called ERROR.OUT.

          2-8  Installing the PrE Software

 








               $@ SYS$STARTUP:SNAPRE$STARTUP SYS$COMMON:[SNAPRE] -
               _$ SYS$COMMON:[SNAPRE]ERROR.OUT

                                          NOTE

                 The characteristics files have the file type
                 file-name.SCF. Do not use this file type for any other
                 files in the same directory.

          __________________________________________________________

          2.2.4  Retry feature for the START command

             In addition to starting the background process, the startup
             command file defines the logicals shown in Table 2-2. If
             you are using the Retry feature, this is a good place to
             add the SNAPRE$RETRY_INTERVAL logical definition. For more
             information on the Retry feature, refer to the START command
             in chapter 3 Controlling PrE, in the DECnet/SNA VMS Printer
             Emulator Use manual.

             The background defines the following logicals:

          Table_2-2:__Defined_Logicals____________________________________

          Logicals________________________Definition______________________

          SNAPRE$CHARACTERISTICS          Points to the directory that
                                          stores the characteristics
                                          files.

          SNAPRE$ONE_FILE_PER_BRACKET     PrE interprets EBI (end bracket
                                          indicator) as the end of file,
                                          when defined to any valid system
          ________________________________logical_value.__________________


                                          Installing the PrE Software  2-9

 





          __________________________________________________________

          2.2.5  Testing the Product

             When you have installed PrE and started the background
             process, run a test to make sure that the interconnect
             system, the link with the IBM host system and all hardware is
             working properly.

             Ask somebody on the IBM host system to send some data to PrE.
             If you have any problems, refer to DECnet/SNA VMS Printer
             Emulator Use, which explains error messages, and the problem
             determination guide for your interconnect system.

          __________________________________________________________

          2.2.6  Running PrE in a VAXcluster

             To run PrE on multiple nodes of a VAXcluster, check to
             see that you have the appropriate software license. After
             installing PrE perform the following steps:

              1.Issue the LICENSE LOAD command to activate the license
                on each node in the VAXcluster, as described in the VMS
                License Management Utility Manual.

              2.Run the startup file SNAPRE$STARTUP on the node where you
                want SNAPREDET to run. Then run the startup file on all
                other nodes. Before beginning the process on all other
                nodes, SNAPREDET will wait until the process running on
                the first node stops.

              3.Define the system executive logical SNAPRE$HOST to be the
                system where the background process will run. This enables
                SNAPRE to quickly locate the background process.

              4.Ensure that each node has access to the directory
                containing the characteristics files.

             After performing the above steps, all cluster users can
             access SNAPREDET and print files.

          2-10  Installing the PrE Software

 





          __________________________________________________________

          2.3  Error Recovery

             If errors occur during the installation or when the IVP is
             running, VMSINSTAL displays error messages.

             Errors can occur during the installation if any of the
             following conditions exist:

              1.The operating system version is incorrect.

              2.A prerequisite software version is incorrect.

              3.Quotas necessary for successful installation are
                insufficient.

              4.System parameter values for successful installation are
                insufficient.

              5.The VMS help library is currently in use.

              6.The product is not properly licensed.

              7.Your system has insufficient disk space.

             For descriptions of the error messages generated by these
             conditions, see the VMS documentation on system messages,
             recovery procedures, and VMS software installation. If any
             of these conditions exist, you should take the appropriate
             action as described in the message. (You might need to change
             a system parameter or increase an authorized quota value.)
             These requirements are part of the installation requirements
             in Chapter 1 of this manual.






                                         Installing the PrE Software  2-11

 





          __________________________________________________________

          2.4  Sample Installation on a VAX System with OPTIONS N

             A sample of the entire installation procedure follows:

               $ @SYS$UPDATE:VMSINSTAL SNAPRE012 MUA0 OPTIONS N

                VAX/VMS Software Product Installation Procedure A5.0-2

               It is 13-MAR-1989 at 08:59.
               Enter a question mark (?) at any time for help.
               %VMSINSTAL-W-DECNET, Your DECnet network is up and running.
               * Do you want to continue anyway [NO]? Y
               * Are you satisfied with the backup of your system disk [YES]?

               Please mount the first volume of the set on  MUA0:.
               * Are you ready? Y
               %MOUNT-I-MOUNTED, SNAPRE mounted on _MUA0:
               The following products will be processed:
                 SNAPRE V1.2

                Beginning installation of SNAPRE V1.2 at 09:01

               %VMSINSTAL-I-RESTORE, Restoring product saveset A ...

                   Release Notes Options:

                1.  Display release notes
                2.  Print release notes
                3.  Both 1 and 2
                4.  Copy release notes to SYS$HELP
                5.  Do not display, print or copy release notes

               * Select option [2]: 4
               * Do you want to continue the installation? Y
               %VMSINSTAL-I-RELMOVED , The product's release notes have been
               successfully moved to SYS$HELP.


          2-12  Installing the PrE Software

 






                       Product:      SNA-PRE
                       Producer:     DEC
                       Version:      1.2
                       Release Date: 03-APR-89

               * Does this product have an authorization key registered and loaded? Y

               * Do you want to purge files replaced by this installation [YES]?

               You can verify the installation using the IVP. This starts and stops a
               connection, then asks you to verify the contents of the event log file. To do
               this, you need the values of the following parameters:

                       * The DECnet/SNA gateway node name
                       * The access name
                       * The session address (secondary logical unit number)

               The terms used above are explained in the User's Guide. Please read the
               Software Limitations section of the Release Notes before running the IVP.

               * Do you want to run the IVP after the installation [YES]?

               %SNAPRE-I-ASKDONE, All installation questions have been asked.  No more input
               -SNAPRE-I-ASKDONE, from the terminal will be required until the IVP runs.

               To start the SNAPREDET background process at system startup the following line
               must be added to the SYS$MANAGER:SYSTARTUP_V5.COM file:

                    @SYS$STARTUP:SNAPRE$STARTUP char-acct err-log

               char-acct is the directory where the PrE characteristic files are to be kept and
               err-log is the file specification for the background process' error stream.

               %VMSINSTAL-I-MOVEFILES, Files will now be moved to their target directories...

               Starting PrE Installation Verification Procedure (IVP)

               Starting the background process...

               %SNAPRE-I-INIBRAK, close file when bracket encountered
               %RUN-S-PROC_ID, identification of created process is 26400104
               %SNAPRE-I-OK, DECnet/SNA PrE Background Process started OK

               You will now be asked for the parameters needed to verify the installation

                                         Installing the PrE Software  2-13

 






               %SNAPRE-I-NEWCHAR, creating new characteristics file SNAPREIVP
               Access_name []: TSO
               Gateway []: NYWAY
               Session_address [1]: 29

               These are the connection's characteristics :

                   SESSION ADDRESS:  29      PAGE LENGTH:   66          PAGE WIDTH: 132
                   CASE:            MIXED    SPACING:     SINGLE        NOIMAGE
                   ACCESS NAME:     TSO                                 WAIT
                   GATEWAY:         NYWAY
                   OUTPUT STREAM:   SYS$SYSTEM:SNAPREIVP.LIS
                   LOG_FILE:        SYS$SYSTEM:SNAPREIVP.LOG

               The connection will now be started, then stopped:

               %SNAPRE-S-CONNECT, connection started with IBM host using characteristics name
               SNAPREIVP, SNAPRE V1.2
               %SNAPRE-S-STOPPING, connection is stopping for characteristics name SNAPREIVP

               The connection's event log file will now be displayed.

               13-MAR-1989 09:04:38.29 %SNAPRE-CONNECT, connection started with IBM host using
               characteristics name  SNAPREIVP, SNAPRE V1.2

               13-MAR-1989 09:04:39.61 %SNAPRE-STOPPING, connection is stopping for
               characteristics name SNAPREIVP

               Please check that the event log file contains an SNAPRE-CONNECT message
               followed by an SNAPRE-STOPPING message. It may also contain SNAPRE-SESACT
               and SNAPRE-SESNOLACT messages. In either case all is well.

               If any other messages are shown please consult the Users's Guide, Appendix B,
               for their meaning.

               Were the contents of the event log file OK? Y

               Background process stopped.

               SNAPRE-IVPSUCC, IVP ran successfully

                Installation of SNAPRE V1.2 completed at 09:05

          2-14  Installing the PrE Software

 






                VMSINSTAL procedure done at 09:05

               $




































                                         Installing the PrE Software  2-15
