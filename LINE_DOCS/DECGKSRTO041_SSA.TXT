Product Name:  DEC GKS for VMS, Version 4.1			SSA 26.20.09-A

HARDWARE REQUIREMENTS

Processors Supported

VAX:	      VAX 6000 Model 200 Series, VAX 6000 Model 300 Series, VAX 6000 
              Model 400 Series

	      VAX 8200, VAX 8250, VAX 8300, VAX 8350, VAX 8500, VAX 8530, VAX 
              8550, VAX 8600, VAX 8650, VAX 8700, VAX 8800, VAX 8810, VAX 8820, 
              VAX 8830, VAX 8840, VAX 8842, VAX 8974, VAX 8978

	      VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

MicroVAX:	   MicroVAX II, MicroVAX 2000, MicroVAX 3100, MicroVAX 3300, 
                   MicroVAX 3400, MicroVAX 3500, MicroVAX 3600, MicroVAX 3800, 
                   MicroVAX 3900

VAXstation:	   VAXstation II, VAXstation 2000, VAXstation 3100 Series, 
                   VAXstation 3200, VAXstation 3500, VAXstation 3520, 
                   VAXstation 3540, 

VAXserver:	   VAXserver 3100, VAXserver 3300, VAXserver 3400, VAXserver 
                   3500, VAXserver 3600, VAXserver 3602, VAXserver 3800, 
                   VAXserver 3900, VAXserver 6000-210, VAXserver 6000-310, 
                   VAXserver 6000-410, VAXserver 6000-420

Processors Not Supported

MicroVAX I, VAXstation I, VAXstation 8000, VAX-11/725, VAX-11/782

Processor Restriction

A TK50 Tape Drive is required for standalone MicroVAX 2000 and 
VAXstation 2000 systems.
F
RO Tektronix is a registered trademark of Tektronix Incorporated.
RO PostScript is a registered trademark of Adobe Systems, Inc.
TM LaserWriter is a trademark of Apple Computer, Inc.
TM HPGL is a trademark of Hewlett-Packard Company.
TM MPS-2000 is a trademark of Lasergraphics.
F

Terminals

One of the following devices is required:

VT125 with black and white or optional color monitor (ReGIS)

VT240 with black and white monitor (ReGIS, Tek 4014)

VT241 with color monitor (ReGIS, Tek 4014)

VT330 with black and white monitor

VT340 with color monitor

TektronixRO 4014 with enhanced graphics module (Option 34) or equivalent
Note:	 The emulation of a Tektronix 4014 is not supported on any 
         hardware.

Tektronix 4107 terminal

Tektronix 4207 terminal

Tektronix 4128 terminal

Tektronix 4129 terminal

VAXstation II, VAXstation II/GPX

VAXstation 2000, VAXstation 3000 series

Block Space Requirements (Block Cluster Size = 1):

Full DEC GKS Kit

Disk space required for installation:			   10,300 blocks
							    (5.3M bytes)

Disk space required for use (permanent):
a maximum of						    9,600 blocks
							    (4.9M bytes)

which may be less depending on the options selected during installation.

DEC GKS Run-Time Kit

Disk space required for installation:			    5,600 blocks
							    (2.9M bytes)

Disk space required for use (permanent):
a maximum of						    5,000 blocks
							    (2.6M bytes)

which may be less depending on the options selected during installation.

These block counts refer to the disk space required on the system disk. 
The sizes are approximate; actual sizes may vary depending on the user's 
system environment, configuration, and software options selected. 

Memory Requirements for DECwindows Support

The minimum supported memory for this application running in a 
standalone DECwindows environment with both the client and server 
executing on that same system is 5MB.

OPTIONAL HARDWARE

DEC GKS also supports a variety of hardcopy output devices and devices          
which are strictly compatible.  Hardcopy devices supported by DEC GKS           
are:

Compatible Sixel Devices

oo   Digital LN03 PLUS Laser Printer

oo   LN03 with LN03S-UA upgrade kit

oo   A34 with graphics option

oo   LA100

oo   LA75

oo   LA50 (restricted to a 2:1 aspect ratio)

oo   LA210

oo   Tektronix 4611 hardcopy unit when connected to the Tektronix 4014 computer 
    display terminal.

Compatible Hewlett Packard Graphics Language (HPGLTM) Devices:

oo   Digital LVP16 pen plotter

oo   HP7475 Hewlett Packard Pen Plotter

oo   HP7550 Hewlett Packard Pen Plotter

oo   HP7580 Hewlett Packard Pen Plotter

oo   HP7585 Hewlett Packard Pen Plotter

Film Recorders

oo   Lasergraphic MPS-2000 TM Film Recorder

Ink Jet Plotters

oo   Digital LJ250 Companion Color Printer

oo   Digital LCG01 Color Ink Jet Plotter (ReGIS)

Compatible PostScript RO Devices

oo   Apple LaserWriter TM

oo   Apple LaserWriter +

oo   LPS40 Laser Printer

oo   LN03R ScriptPrinter Laser Printer

CLUSTER ENVIRONMENT

This layered product is fully supported when installed on an valid and 
licensed VAXcluster* configuration without restrictions.  The HARDWARE 
REQUIREMENTS sections of this product's Software Product Description and 
System Support Addendum detail any special hardware required by this 
product.

*   V5.x VAXcluster configurations are fully described in the VAXcluster 
    Software Product Description (29.78.xx) and include CI, Ethernet, and 
    Mixed Interconnect configurations.

SOFTWARE REQUIREMENTS*

For Systems Using Terminals (No DECwindows Interface):

VMS Operating System V5.1 - V5.3

For Workstations Running VWS:

VMS Operating System V5.1 - V5.3

VMS Workstation Software V4.0 - V4.1

For Workstations Running DECwindows:

VMS Operating System V5.1 - V5.3 (and the necessary components of VMS 
DECwindows)

This product may run in either of the following ways:

oo   Standalone execution - Running the X11 display server and the client 
    application on the same machines.

oo   Remote Execution - Running the X11 display server and the client 
    applicaton on different machines.

VMS DECwindows is a part of the VMS Operating System but must be 
installed separately.  Installation of the VMS DECwindows gives the user 
the option of installing any of the following three components:

oo   VMS DECwindows Base kit (Runtime Support)

oo   VMS DECwindows Device support

oo   VMS DECwindows Programming Language support

For standalone execution, the following DECwindows components must be 
installed on the machine.

oo   VMS DECwindows Base kit (Runtime Support)

oo   VMS DECwindows Device support

For remote execution, the following DECwindows components must be 
installed on the machines:

Server Machine

VMS DECwindows Base Kit
(Runtime support)

VMS DECwindows Device support
Language support 

Client Machine

VMS DECwindows Base Kit
(Runtime support)

Language Support

For the development of application programs that use DEC GKS under VMS, 
one of the DEC GKS supported languages is also required.

VMS Tailoring

For VMS V5.x systems, the following VMS classes are required for full 
functionality of this layered product:

oo   VMS Required Saveset

oo   Network Support

oo   Programming Support

OPTIONAL SOFTWARE

VAX FORTRAN V5.4
VAX Ada V2.1
VAX PASCAL V4.0
VAX C V3.1
VAX PL/I V3.3
VAX BASIC V3.3
VAX COBOL V4.3
VAX BLISS-32 Implementation Language V4.6

GROWTH CONSIDERATIONS

The minimum hardware/software requirements for any future version of 
this product may be different from the minimum requirements of the 
current version.

DISTRIBUTION MEDIA

Tape:	      9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

This product is also available as part of the VMS Consolidated Software 
Distribution on CDROM.

ORDERING INFORMATION

Development System

Software Licenses: QL-810A*-**
Software Media: QA-810A*-**
Software Documentation: QA-810AA-GZ
Software Product Services: QT-810A*-**

Run-Time Option

Software Licenses: QL-811A*-**
Software Media: QA-811A*-**
Software Product Services: QT-811A*-** 

*   Denotes variant fields.  For additional information on available licenses, 
    services and media, refer to the appropriate price book.

The above information is valid at time of release.  Please contact your 
local Digital office for the most up-to-date information.

March 1990
AE-LT69C-TE

(R) The DIGITAL Logo is a registered trademark of Digital Equipment 
Corporation.

(TM)   VAX, VMS, MicroVAX, VAXstation, VAXserver, VAXcluster, DECwindows, 
DEC GKS, ReGIS, VT, LN03, LA100, LA75, LA50, LA210, LVP16, LPS40 and 
LN03R are trademarks of Digital Equipment Corporation.

