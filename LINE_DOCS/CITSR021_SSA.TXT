 






    System
    Support
    Addendum

 _____________________________________________________________________

 PRODUCT NAME:  DEC Computer Integrated Telephony     SSA 29.91.03-A
                 Server for VMS[*], Version 2.1


HARDWARE REQUIREMENTS
Processors Supported                   VAXstationVAXstation II,
                                                 VAXstation 3200,
   VAX:    VAX 4000 Model 300                    VAXstation 3500,
                                                 VAXstation 3520,
           VAX 6000 Model 200                    VAXstation 3540
           Series,
           VAX 6000 Model 300          VAXserver:VAXserver 3100,
           Series,                               VAXserver 3300,
           VAX 6000 Model 400                    VAXserver 3400,
           Series,                               VAXserver 3500,
           VAX 6000 Model 500                    VAXserver 3600,
           Series                                VAXserver 3602,
                                                 VAXserver 3800,
           VAX 8200, VAX 8250,                   VAXserver 3900,
           VAX 8300,                             VAXserver 4000
           VAX 8350, VAX 8500,                   Model 300
           VAX 8530,
           VAX 8550, VAX 8600,                   VAXserver 6000-
           VAX 8650,                             210, VAXserver
           VAX 8700, VAX 8800,                   6000-220,
           VAX 8810,                             VAXserver 6000-
           VAX 8820, VAX 8830,                   310, VAXserver
           VAX 8840                              6000-320,
                                                 VAXserver 6000-
           VAX 9000-210, VAX                     410, VAXserver
           9000-410                              6000-420,
                                                 VAXserver 6000-
           VAX-11/750, VAX-                      510, VAXserver
           11/780, VAX-11/785                    6000-520

   MicroVAX: MicroVAX II,           Processor Restriction

             MicroVAX 2000,         A TK50 Tape Drive is required
             MicroVAX 3100,         for standalone MicroVAX 2000.
             MicroVAX 3300,
             MicroVAX 3400,
             MicroVAX 3500,
             MicroVAX 3600,
             MicroVAX 3800,
             MicroVAX 3900




                               DIGITAL                   January 1991

                                                          AE-LN57D-TE

 


DEC Computer Integrated Telephony                      SSA 29.91.03-A



Processors Not Supported            In addition, a customer must
                                    have a CIT-compatible PBX
   VAX:    VAX-11/725, VAX-         configured to support the
           11/730, VAX-11/782       particular PBX link protocol.
           VAXft 3000-310           PBXs supported are:

   MicroVAXMicroVAX I                  o  The Mitel SX2000[TM]
                                          with Host Command
   VAXstationVAXstation                   Interface (HCI) Software
             I, VAXstation                Version

             2000, VAXstation          o  The Northern Telecom
             3100 Series,                 Meridian[TM] 1 PBX with
             VAXstation 8000              the Meridian Link
Disk Space Requirements (Block            Protocol (ISDN/AP)
Cluster Size = 1):                        Software X11, Release 16

                                       o  Siemens HICOM PBXs (340-
   Disk space required for     1,500 block370 and the 390) with
   installation:               (750K bytesVersion 2.3 Software

                                       o  The British Telecom
   Disk space required for     1,050 blockRegent 257 with Host
   use:                        (525K bytesCommand Interface (HCI)
These counts refer to the disk            Software

space required on the system        Information regarding this
disk. The sizes are                 software and any other PBX
approximate; actual sizes may       hardware requirements is
vary depending on the user's        available from the distributor
system environment,                 of the specific PBX.
configuration, and software            Note: Whereas the PBX
options.                               manufacturer is responsible

OPTIONAL HARDWARE                      for supplying any modems or

PBX Requirements                       modem eliminators which may
                                       be required, Digital is
For the link to a PBX, the             responsible for supplying
customer must have a                   the cable connecting the
CIT-compatible PBX and a               synchronous interface on
synchronous communications             the VAX to the modem or
device installed on the VAX.           modem eliminator.
Supported synchronous devices       The CIT Server can support more
include:                            than one link to PBXs running the

   o  DPV11 and DSV11 for           same protocol. The number of links
      Q-bus VAX systems             is dependent on the number of syn-

   o  DMF32 for UNIBUS VAX          chronous ports on the VAX and the
      systems                       number of ports available on the
   o  DMB32 and DSB32 for           PBXs. If the PBXs are running dif-
      BI-bus VAX systems            ferent protocols, one CIT Server
                                    cannot be connected to more than
   o  DSH32 and DST32 for           one PBX.
      Bus-less VAX systems          DECvoice Requirements

                                  2

 


DEC Computer Integrated Telephony                      SSA 29.91.03-A



For communication with a               o  DEC CIT Applications
DECvoice module, the customer             Interface for VMS V2.1
must have at least one                    software must be
DECvoice DTC04 Q-bus module               installed on either the
and Telephone Line Interface              same node as the DEC CIT
(TLI) installed on the CIT                Server software, or on
server.                                   another node linked by
Refer to the DECvoice System              DECnet to the CIT Server
Support Addendum (SSA                     host.

29.97.xx-x) for details of the      In addition to Digital's
processors supported for the        software requirements, the
DECvoice module.                    software requirements for any
                                    PBX connected to a CIT server
CLUSTER ENVIRONMENT                 must also be observed.

This layered product is fully       VMS Tailoring

supported when installed on         For VMS V5.x systems, the
any valid and licensed              following VMS classes are
VAXcluster* configuration           required for full
without restrictions. The           functionality of this layered
HARDWARE REQUIREMENTS sections      product:
of this product's Software             o  VMS Required Saveset
Product Description and System
Support Addendum detail any            o  Network Support

special hardware required by        For more information on VMS
this product.                       classes and tailoring, refer

   *  V5.x VAXcluster               to the VMS Operating System
      configurations are fully      Software Product Description
      described in the              (SPD 25.01.xx).

      VAXcluster Software
      Product Description           OPTIONAL SOFTWARE
      (29.78.xx) and include
      CI, Ethernet, and Mixed       For a link to a PBX, the
      Interconnect                  software and hardware
      configurations.               requirements of the PBX must
                                    be observed.

SOFTWARE REQUIREMENTS               For DECvoice support, the
                                    DECvoice Software Kit, V1.1
   o  VMS Operating System          must be installed on the
      V5.0 - V5.4.                  server node. Note this is
   o  DECnet-VAX V5.0 - V5.4        available only in North
      (end-node or                  America. Refer to the DECvoice
      full-function node            Software Software Product
      license) if more than         Description (SPD 29.97.xx) for
      one VAX is part of the        further details.

      configuration.                P.S.I. Restriction

   o  VAX Wide Area Network
      (WAN) Device Drivers
      V1.1.

                                  3

 


DEC Computer Integrated Telephony                      SSA 29.91.03-A



If the DEC CIT Server software      The above information is valid
is required to support PBX          at time of release. Please
functions, the server software      contact your local Digital
cannot be installed on the          office for the most up-to-date
same system as VAX P.S.I.           information.
However, it can be used on the
same system as the VAX P.S.I.          *  Formerly VAX Computer
Access routines.                          Integrated Telephony
                                          Server (VAX CIT Server).

GROWTH CONSIDERATIONS                  [TMHICOM is a trademark of

The minimum hardware/software             Siemens AG.
requirements for any future
version of this product may be         [TMMeridian and Meridian
different from the                        Link are trademarks of
requirements for the current              Northern Telecom.
version.
                                       [TMSX2000 is a trademark of
DISTRIBUTION MEDIA                        Mitel Corporation.

Tape: 9 track 1600 BPI Magtape         [TMThe DIGITAL Logo, BI,
(PE), TK50 Streaming Tape                 DECnet-VAX, Q-bus,
This product is also available            UNIBUS, VAX, MicroVAX,
as part of the VMS                        VAXstation, VAXserver,
Consolidated Software                     and VMS are trademarks
Distribution on CDROM.                    of Digital Equipment
                                          Corporation.

ORDERING INFORMATION

   Software Licenses:
   QL-VGYA*-**
   Software Media: QA-VGYAA-**
   Software Documentation:
   QA-VGYAA-GZ
   Software Product Services:
   QT-VGYA*-**

   *  Denotes variant fields.
      For additional
      information on available
      licenses, services, and
      media, refer to the
      appropriate price book.











                                  4
