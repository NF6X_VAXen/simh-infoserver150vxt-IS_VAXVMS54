 






     System
     Support
     Addendum

     ________________________________________________________________
     ________________________________________________________________

     PRODUCT NAME:  DEC RdbExpert for VMS, Version 1.0 SSA 31.72.00-A

     HARDWARE REQUIREMENTS

     Processors Supported

     VAX:        VAXft 3000-310,
                 VAX 4000 Model 300,
                 VAX 6000 Model 200 Series,
                 VAX 6000 Model 300 Series,
                 VAX 6000 Model 400 Series,
                 VAX 6000 Model 500 Series

                 VAX 8200, VAX 8250, VAX 8300, VAX 8350, VAX 8500,
                 VAX 8530, VAX 8550, VAX 8600, VAX 8650, VAX 8700,
                 VAX 8800, VAX 8810, VAX 8820, VAX 8830, VAX 8840,
                 VAX 9000-210, VAX 9000-410

                 VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

     MicroVAX:   MicroVAX II, MicroVAX 2000,
                 MicroVAX 3100, MicroVAX 3300,
                 MicroVAX 3400, MicroVAX 3500,
                 MicroVAX 3600, MicroVAX 3800,
                 MicroVAX 3900

     VAXstation: VAXstation II, VAXstation 2000,
                 VAXstation 3100 Series, VAXstation 3200,
                 VAXstation 3500, VAXstation 3520,
                 VAXstation 3540


                                  DIGITAL               December 1990

                                                          AE-PB8ZA-TE

 


     DEC RdbExpert for VMS, Version 1.0                SSA 31.72.00-A




     VAXserver:  VAXserver 3100, VAXserver 3300, VAXserver 3400,
                 VAXserver 3500, VAXserver 3600, VAXserver 3602,
                 VAXserver 3800, VAXserver 3900, VAXserver 4000 Model
                 300

                 VAXserver 6000-210, VAXserver 6000-220, VAXserver
                 6000-310, VAXserver 6000-320, VAXserver 6000-410,
                 VAXserver 6000-420, VAXserver 6000-510, VAXserver
                 6000-520

     Processors Not Supported

     MicroVAX I, VAX-11/725, VAX-11/782, VAXstation I, VAXstation
     8000

     Processor Restrictions

     o  All processors require mass storage units to backup and
        restore established databases.

     o  All processors require a specified minimum memory (refer to
        the following chart).

     o  All processors require a device for installation of the
        software (refer to the following chart ).

        Magnetic Tape = One 9-track, 1600 BPI Magnetic Tape

     o  Certain processors have other requirements (refer to the
        following chart).








                                     2

 


     DEC RdbExpert for VMS, Version 1.0                SSA 31.72.00-A


     ________________________________________________________________
                             Installation
                   Minimum               Other
     System________Memory____Device______Requirements________________

     VAX-11/750    8         Magnetic    ECO level 3 or later
                             tape
     VAX-11/780    12        Magnetic    ECO level 4 or later
     or VAX-                 tape        (VAX-11/780)
     11/785                              WCS microcode version 123 or
                                         later
     VAX 82xx      12        Magnetic
                             tape
     VAX 83xx      12        Magnetic
                             tape
     VAX 8974      12        Magnetic
     and VAX       (per      tape
     8978          pro-
                   ces-
                   sor)

     All other     12        Magnetic
     supported               tape
     VAX Sys-                or TK50
     tems                    tape
     ________________________drive___________________________________














                                     3

 


     DEC RdbExpert for VMS, Version 1.0                SSA 31.72.00-A



     Block Space Requirements (Block Cluster Size = 1)

     Disk space required for      17,000 blocks
     installation:
                                  (8,704K bytes)

     Disk space required for      10,600 blocks
     use (permanent):
                                  (5,427K bytes)

     These counts refer to the disk space required on the system
     disk. The sizes are approximate; actual sizes may vary depending
     on the user's system environment, configuration, and software
     options.

     CLUSTER ENVIRONMENT

     This layered product is fully supported when installed on any
     valid and licensed VAXcluster* configuration without restric-
     tions. The HARDWARE REQUIREMENTS sections of this product's
     Software Product Description and System Support Addendum detail
     any special hardware required by this product.

     *  V5.x VAXcluster configurations are fully described in the
        VAXcluster Software Product Description (29.78.xx) and in-
        clude CI, Ethernet, and Mixed Interconnect configurations.

     SOFTWARE REQUIREMENTS

     For Systems Using Terminals (No DECwindows Interface):

     o  VMS Operating System V5.2 - V5.4

     o  VAX Rdb/VMS (Run-Time Option) V3.1 - V4.0

        The Run-Time software is required for DEC RdbExpert to create
        and access its physical design repository.


                                     4

 


     DEC RdbExpert for VMS, Version 1.0                SSA 31.72.00-A



     For Workstations Running DECwindows:

     o  VMS Operating System V5.4 (and necessary components of VMS
        DECwindows)

     o  VAX Rdb/VMS (Run-Time Option) V3.1 - V4.0

        The Run-Time software is required for DEC RdbExpert to create
        and access its physical design repository.

     VMS Tailoring

     For VMS V5.x systems, the following VMS classes are required for
     full functionality of this layered product:

     o  VMS Required Saveset

     o  Network Support

     o  VMS Workstation Support

     For more information on VMS classes and tailoring, refer to VMS
     Operating System (SPD 25.01.xx).

     OPTIONAL SOFTWARE

     Certain versions of these products depend upon a specific ver-
     sion of the Operating System. Refer to the System Support Ad-
     dendum of the product in question to determine which version is
     necessary.

     o  VAX Rdb/VMS Version 3.1 - V4.0 interactive or development
        software

        The VAX Rdb/VMS interactive or development software is re-
        quired to implement the DEC RdbExpert suggestions.

     o  DECtrace for VMS Version 1.0

        DECtrace can optionally collect database transaction workload
        information directly from running VAX Rdb/VMS database for
        use by DEC RdbExpert in generating enhanced physical designs.

                                     5

 


     DEC RdbExpert for VMS, Version 1.0                SSA 31.72.00-A




        Note: Application programs do NOT have to be coded with
        DECtrace procedure calls to make use of this functionality.

     GROWTH CONSIDERATIONS

     The minimum hardware/software requirements for any future ver-
     sion of this product may be different from the requirements for
     the current version.

     DISTRIBUTION MEDIA

     Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

     This product is also available as part of the VMS Consolidated
     Software Distribution on CDROM.

     ORDERING INFORMATION

     Software Licenses: QL-VFJA*-**
     Software Media: QA-VFJA*-**
     Software Documentation: QA-VFJAA-GZ
     Software Product Services: QT-VFJA*-**

     *  Denotes variant fields. For additional information on li-
        censes, services and media, refer to the appropriate price
        book.

     The above information is valid at time of release. Please con-
     tact your local Digital office for the most up-to-date informa-
     tion.

     [TM] The DIGITAL Logo, DECwindows, VAX Rdb/VMS, VAX, MicroVAX,
          VMS, VAXserver and VAXstation are trademarks of Digital
          Equipment Corporation.




                                     6
