 






          DIGITAL

          Read Before Installing or Using VAX Ada Version 2.2

          AV-EF83J-TE

          Whether you are a new or a continuing VAX Ada customer, please
          take time to read the following information about your product.

          New VAX Ada customers should receive the following:

          o  The media required for Version 2.2 of VAX Ada

          o  User documentation

          o  Supplemental materials and any additionally ordered items

          Continuing VAX Ada customers should receive the following:

          o  The media required for Version 2.2 of VAX Ada

          o  Supplemental materials and any additionally ordered items

          Installation Information

          For this release, VAX Ada Version 2.2 requires VMS Version 5.4
          or higher.

          Note that for this release, an Ada run-time library is installed
          as part of the VAX Ada kit.

          If you are installing VAX Ada Version 2.2 on a newly-licensed
          node or cluster, you must first register a License Product
          Authorization Key (License PAK) using the License Management
          Facility (LMF). The License PAK may be shipped along with the
          kit if you ordered the license and media together; otherwise, it
          is shipped separately to a location based on your license order.

          If you are installing VAX Ada Version 2.2 as an update on a node
          or cluster already licensed for this software, you must register
          the Service Update PAK (SUP) that is included in your Service
          Update Kit. If you already registered a License PAK or a SUP

 






          for this product on this node or cluster, you do not need to
          register the SUP.

          If you are installing an update of VAX Ada but lack a service
          contract, call your Digital representative for instructions on
          how to get a License PAK.

          See the VMS License Management Utility Manual for registration
          instructions.

          Previous versions of VAX Ada do not need to be installed before
          this version.




             � Digital Equipment Corporation. 1991. All rights reserved.






















                                          2

 






          The Version 2.2 media kit consists of the following:

          1. A kit, named ADA022, that contains the following components:

             -  The VAX Ada compiler (ADA.EXE)

             -  The VAX Ada program library manager (ACS.EXE)

             -  The VAX Ada run-time library (ADARTL.EXE)

             -  The VAX Ada message file (ADAMSG.EXE)

             -  The Ada-from-CDD translator utility (ADA$FROM_CDD.EXE)

             -  DCL command definitions for the compiler, program library
                manager, and Ada-from-CDD translator

             -  The VAX Ada predefined library [SYSLIB.ADALIB], which is
                associated with the logical name ADA$PREDEFINED

             -  A predefined VAX Source Code Analyzer (SCA) library
                [SYSLIB.ADA$SCALIB], which is associated with the logical
                name ADA$SCA_PREDEFINED

          2. A kit, named ADAHLP022, that contains the VAX Ada help file

          Release Notes Information

          The release notes for VAX Ada Version 2.2 contain important
          installation-related information, a summary of technical changes
          and new features, compatibility information, current restric-
          tions and restrictions removed, and performance information.

          You can read the release notes before installing VAX Ada by
          invoking VMSINSTAL with the release notes option, N.

          After installing VAX Ada Version 2.2, you can obtain the release
          notes from the file SYS$HELP:ADA022.RELEASE_NOTES. The release
          notes are also included in the VAX Ada help kit (ADAHLP022).
          After you install the help kit, you can obtain the release notes
          by typing:

               $ HELP ADA Release_Notes

                                          3

 






          Note that the release notes in the help kit are more extensive:
          they accumulate information across point releases, and include
          validation and documentation information.

          Compatibility Information

          Before installing VAX Ada Version 2.2, you should read the re-
          lease notes. The release notes contain important information on
          the compatibility of VAX Ada
          Version 2.2 with previous versions of VAX Ada. In particular:

          o  The VAX Ada Version 2.2 compiler is incompatible with VAXELN
             Ada Version 2.0 or lower. It requires VAXELN Ada Version 2.2.

          o  VAX Ada Version 2.2 introduces support for library search
             paths. This support is compatible with Version 2.0 and 2.1
             libraries; no library upgrade or conversion is required.

          o  Beginning with VAX Ada Version 2.0, VAX Ada uses a new pro-
             gram library format that is incompatible with the format for
             VAX Ada Versions 1.0 through 1.5. If you are installing VAX
             Ada Version 2.2 on a system that is currently running a Ver-
             sion 1 compiler, program library manager, and so on, you must
             convert any Version 1 libraries to the Version 2 format using
             the ACS CONVERT LIBRARY command. The conversion makes all
             units in the library obsolete; you must recompile before you
             can link or perform other operations that require units that
             are current. You may also need to manually enter or reenter
             entered units, depending on the order in which you convert
             your libraries and make them current.

                                           NOTE

                If you are installing VAX Ada Version 2.2 on a system
                that is running VAX Ada Version 2.0 or 2.1, no new
                library conversion is required.



                                          4

 






             A number of feature changes may affect the compatibility of
             VAX Ada Version 2.2 with previous versions of VAX Ada. See
             the release notes for more information on new VAX Ada Version
             2.2 features. In particular, note that the following VAX Ada
             predefined units have changed and/or have been recompiled:

                ASSERT
                ASSERT_GENERIC (specification and body)
                CONTROL_C_INTERCEPTION (body)
                DIRECT_MIXED_IO (specification and body)
                DTK (specification)
                FLOAT_TEXT_IO
                INDEXED_MIXED_IO (specification and body)
                INTEGER_TEXT_IO
                LBR (specification)
                LIB (specification)
                LONG_FLOAT_TEXT_IO
                LONG_LONG_FLOAT_TEXT_IO
                MTH (specification)
                NCS (specification)
                OTS (specification)
                PPL (specification)
                RELATIVE_MIXED_IO (specification and body)
                RMS_ASYNCH_OPERATIONS (specification and body)
                SEQUENTIAL_MIXED_IO (specification and body)
                SHORT_INTEGER_TEXT_IO
                SHORT_SHORT_INTEGER_TEXT_IO
                SOR (specification)
                SMG (specification)
                STARLET (specification)
                STR (specification)
                TASKING_SERVICES (specification and body)
                TEXT_IO (specification and body)
                VAXELN_SERVICES (specification and body)





                                          5

 






          o  The installation procedure allows you to save the previous
             version of the compiler and its environment when you install
             VAX Ada Version 2.2. The installation procedure also creates
             command procedures that allow you to switch back and forth
             between the compilers that you have saved and the newly
             installed compiler.

































                                          6

 






          Contents of This Kit

          o  Indented Bill Report (BIL) and Bill of Materials (BOM)

             Please read the BIL and BOM enclosed in this kit and check to
             see that all items listed are actually in your kit. If your
             kit is damaged or any items are missing, call your Digital
             representative.

          o  Media

             If you ordered media, you will find the media and the VAX
             Ada Installation Guide in this kit. Consult the VAX Ada
             Installation Guide for information about installing VAX Ada
             on your system.

          o  Documentation

             Depending on your order, this kit may include copies of the
             following VAX Ada documentation:

             -  VAX Ada and VAXELN Ada Technical Summary-This booklet
                summarizes the features of VAX Ada and VAXELN Ada. It is
                designed to allow users and vendors to compare-at a very
                technical level-the VAX Ada and VAXELN Ada implementations
                with other Ada implementations.

             -  VAX Ada Language Reference Manual-This manual combines
                the text of the Ada ANSI Standard (ANSI/MIL-STD-1815A-
                1983) with specific information about VAX Ada. Use it as a
                reference for preparing VAX Ada source programs.

             -  Developing Ada Programs on VMS Systems-This manual de-
                scribes how to use the VAX Ada program library manager,
                and shows how to compile, link, execute, and debug VAX Ada
                programs on the VMS operating system.

             -  VAX Ada Run-Time Reference Manual-This manual gives
                system-specific information about input-output, stor-
                age allocation, mixed-language programming, system service
                calls, exception handling, and tasking. It also gives
                information on how to improve the run-time performance.

                                          7

 






             Note that the VAX Ada Installation Guide is included with the
             media.

          o  Software Product Description (SPD)

             The SPD provides an overview of the VAX Ada kit and its
             features.

          o  System Support Addendum (SSA)

             The SSA describes the technical environment in which the
             product is supported.

          o  Software Performance Report (SPR)

             Use this form to report any problems with VAX Ada, provided
             you have purchased warranty services.

          A Final Note

          Digital prides itself on responding to customer needs. In order
          to continue serving you, we need your comments. Each manual
          contains preaddressed, postage-paid Reader's Comments forms
          at the back. If you find errors in a manual or want to make
          comments about it, please fill out one of these forms and send
          it to us. If you find errors in the software, please submit an
          SPR, provided you have purchased warranty services.












                                          8
