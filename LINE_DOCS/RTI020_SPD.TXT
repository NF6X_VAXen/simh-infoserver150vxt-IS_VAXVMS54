 






     Software
     Product
     Description

     ________________________________________________________________

     PRODUCT NAME:  DEC Realtime Test Integrator for VMS, Version 2.0
     SPD 28.30.05
                    (DEC RT Integrator for VMS)

     DESCRIPTION

     DEC Realtime Test Integrator (RT Integrator) is an icon-based,
     graphical programming environment. Instead of using a conven-
     tional programming language, users create and run realtime (data
     acquisition, IEEE-488 and RS-232 instrument control, test and
     measurement) applications by drawing them graphically as flow
     diagrams.

     Each DEC RT Integrator icon represents a function such as an
     analog or digital input, an arithmetic operation, or a logical
     function. DEC RT Integrator provides several libraries of such
     functions. The application is built by using the point-and-click
     method now standard in graphical user interfaces. By pointing
     and clicking the mouse, icons are moved from the libraries to
     the worksurface and then connected to the icons with either
     data flow or signal flow lines. Once the icons are setup, the
     application is ready to run.

     The worksurface acts as a graphical editor, enabling users
     to move, copy, delete, add, cut and paste icons representing
     devices and sections of code. This allows users to create and
     change applications more quickly than traditional programming
     languages.

     Each icon has a set-up menu that allows customization of its
     function for the application. Once built, the application can
     be saved and restored without repeating the set-up operations.
     Since the configuration is saved as an ASCII text file, it can

                                  DIGITAL                January 1991

                                                          AE-PEHMA-TE

 


     DEC Realtime Test Integrator for VMS, Version 2.0   SPD 28.30.05
     (DEC RT Integrator for VMS)


     be mailed to any other system running DEC RT Integrator and re-
     stored there. This feature allows users to build an application
     on a VMS system and transport it to an ULTRIX system or vice
     versa, and run it without modifications.

     DEC RT Integrator extends its capabilities by allowing users
     to create and modify icons by following a template and detailed
     instructions. Since all icons are modular functions, the indi-
     vidually created icons can be used in all future applications.

     By putting icons together and shrinking them to form a single
     icon, macros can be built and manipulated like any other indi-
     vidual icon. This feature allows the user to create higher order
     representations of an application from basic building blocks.

     For any DEC RT Integrator application, a control panel can be
     created using the data display and user input icons. The con-
     trol panel is a simple user interface representing an instrument
     control panel that novice users and non-programmers can use to
     interact with the application. Little knowledge of DEC RT In-
     tegrator is required to run an application using the control
     panel. Building the control panel requires selecting the ap-
     propriate icons and positioning the user input devices on the
     control panel surface.

     Every application has two conceptual views: a data flow view
     and a signal flow view. DEC RT Integrator allows users to switch
     between both views with the click of a mouse button. In the data
     flow view, the connections between icons represent the flow of
     data through the application. The signal flow view displays the
     application's logic structure.

     Context-sensitive, on-line HELP makes the software environment
     easy to learn. Using the point-and-click method, HELP is pro-
     vided on the specific icon selected. When building an individual
     icon, users can add help text for that icon.



                                     2

 


     DEC Realtime Test Integrator for VMS, Version 2.0   SPD 28.30.05
     (DEC RT Integrator for VMS)


     DEC RT Integrator was designed to simplify IEEE-488 instrument
     control. The user directly programs the instrument and needs
     no knowledge of the bus itself. String manipulation and other
     support icons simplify the construction of command strings.
     The service request (SRQ) can be detected as a signal in the
     signal flow view and the SRQ status byte is available as data
     in the data flow view. These mechanisms allow instrument events
     to be detected and serviced with the same graphical programming
     techniques used to create the rest of the application.

     DEC RT Integrator allows users to monitor alpha-numeric data
     as it is being collected, and display it as a line plot, point
     plot, or scrolling text. Users can insert the display icons in
     any path of the data flow diagram and use them as a probe to
     trace the data as it flows through the application. In addition
     to monitoring the data, users can also generate PostScript[R]
     files of the plots for hardcopy records.

     In addition to the traditional mathematical and trigonometric
     operations, there are icons to perform noise filtering, scaling,
     multiplexing, or signal processing functions such as FFTs and
     power spectrum analysis. A range check icon allows users to
     determine if a data value falls outside a given range and take
     corrective action.

     Time generation functions allow users to trigger applications or
     events using either a realtime clock or a system clock. A time
     stamp can also be put on each data value as it is being stored
     to a file.

     DEC RT Integrator software is compatible with DECwindows so
     applications can be controlled from or displayed on any worksta-
     tion, personal computer, or terminal that is compatible with the
     X Window System[TM].

     In addition, users can add support for any foreign device for
     which a GKS handler exists.


                                     3

 


     DEC Realtime Test Integrator for VMS, Version 2.0   SPD 28.30.05
     (DEC RT Integrator for VMS)


     DEC Realtime Test Integrator for VMS supports the IEZ11, which
     is an IEEE-488 bus-controller for Digital workstations with a
     SCSI interface. DEC RT Integrator for VMS manages all SCSI and
     RS232 interactions allowing users to communicate directly with
     the IEEE-488 bus. This product also supports the VAXBI clock.

     Multiple plotting windows are supported on all terminals.

     HARDWARE REQUIREMENTS

     VAX, MicroVAX, VAXserver or VAXstation configuration as speci-
     fied in the System Support Addendum (SSA 28.30.05-x).

     SOFTWARE REQUIREMENTS

     For Workstations Running VWS:

     VMS Operating System

     VMS Workstation Software

     For Workstations Running DECwindows:

     VMS Operating System (and necessary components of VMS DECwin-
     dows)

     DEC GKS for VMS - required if plotting routines are used

     VAX C - required to develop icons

     DRB32 VMS Drivers - prerequisite for LIO support of the DRB32
     and DRB32W devices and I/O subsystems that interface to the
     VAXBI through the DRB32 and DRB32W

     Refer to the System Support Addendum (SSA 28.30.05-x) for avail-
     ability and versions of required software and for information
     regarding components of VMS DECwindows.


                                     4

 


     DEC Realtime Test Integrator for VMS, Version 2.0   SPD 28.30.05
     (DEC RT Integrator for VMS)


     ORDERING INFORMATION

     Development License

     This license is SUBSCRIBER/USER based and can be found in the
     User/Generic section of the U.S. Price Book.

     Software Licenses: QL-YWQA9-QB
     Software Media: QA-YWQAA-**
     Software Documentation: QA-YWQAA-GZ
     Software Product Services: QT-YWQA*-**

     Run-Time License

     This license is ClusterWide and can be found in the ClusterWide
     section of the U.S. Price Book.

     Software Licenses: QL-B15A9-J*
     Software Media: QA-B15AA-**
     Software Product Services: QT-B15A*-**

     *  Denotes variant fields. For additional information on li-
        censes, services, and media, refer to the appropriate price
        book.

     SOFTWARE LICENSING

     This software is furnished under the licensing provisions of
     Digital Equipment Corporation's Standard Terms and Conditions.
     For more information about Digital's licensing terms and poli-
     cies, contact your local Digital office.

     LICENSE MANAGEMENT FACILITY

     This layered product supports the License Management Facil-
     ity. License units for this product are allocated on a per-CPU
     capacity basis.


                                     5

 


     DEC Realtime Test Integrator for VMS, Version 2.0   SPD 28.30.05
     (DEC RT Integrator for VMS)


     For more information on the License Management Facility, refer
     to the VMS Operating System Software Product Description (SPD
     25.01.xx) or the VMS Operating System documentation set.

     For more information on Digital's licensing policies, contact
     your local Digital office.

     SOFTWARE PRODUCT SERVICES

     A variety of service options are available from Digital. For
     more information, contact your local Digital office.

     SOFTWARE WARRANTY

     Warranty for this software product is provided by Digital with
     the purchase of a license for the product as defined in the
     Software Warranty Addendum of this SPD.

     [R]  PostScript is a registered trademark of Adobe Systems Inc.

     [TM] X Window System is a trademark of Massachusetts Institute
          of Technology.

     [TM] The DIGITAL Logo, CI, DEC, DECwindows, LA, Q-bus, MicroVAX,
          VAXcluster, VAXserver, VAXstation, VAX, VAX Ada, VAX BASIC,
          VAX C, VAX FORTRAN, VAX Pascal, VMS and VT are trademarks
          of Digital Equipment Corporation.












                                     6
