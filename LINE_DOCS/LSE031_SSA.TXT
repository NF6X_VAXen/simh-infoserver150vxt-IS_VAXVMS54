 






          System
          Support
          Addendum

          ________________________________________________________________

          PRODUCT NAME:  VAX Language-Sensitive Editor/Source Code Ana-
          lyzer,                                            SSA 26.59.11-A
                         Version 3.1
          HARDWARE REQUIREMENTS

          Processors Supported

          VAX:      VAX 4000 Model 300,
                    VAXft 3000 Model 310,
                    VAX 6000 Model 200 Series,
                    VAX 6000 Model 300 Series,
                    VAX 6000 Model 400 Series

                    VAX 8200, VAX 8250, VAX 8300, VAX 8350,
                    VAX 8500, VAX 8530, VAX 8550, VAX 8600,
                    VAX 8650, VAX 8700, VAX 8800, VAX 8810,
                    VAX 8820, VAX 8830, VAX 8840, VAX 8842,
                    VAX 8974, VAX 8978

                    VAX 9000-210, VAX 9000-410

                    VAX-11/730, VAX-11/750, VAX-11/780, VAX-11/785

          MicroVAX: MicroVAX II, MicroVAX 2000,
                    MicroVAX 3100, MicroVAX 3300,
                    MicroVAX 3400, MicroVAX 3500,
                    MicroVAX 3600, MicroVAX 3800,
                    MicroVAX 3900





                                       DIGITAL              September 1990

                                                               AE-LT70E-TE

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A




          VAXstationVAXstation II, VAXstation 2000,
                    VAXstation 3100 Series, VAXstation 3200,
                    VAXstation 3500, VAXstation 3520,
                    VAXstation 3540

          VAXserver:VAXserver 3100 VAXserver 3300,
                    VAXserver 3400, VAXserver 3500,
                    VAXserver 3600, VAXserver 3602,
                    VAXserver 3800, VAXserver 3900,

                    VAXserver 6000-210, VAXserver 6000-310,
                    VAXserver 6000-410, VAXserver 6000-420

          Processors Not Supported

          VAX-11/725, VAX-11/782, MicroVAX I, VAXstation I, VAXstation
          8000

          Processor Restrictions

          o  A TK50 Tape Drive is required for standalone MicroVAX 2000
             and VAXstation 2000 systems.

          o  The VAX Language-Sensitive Editor/Source Code Analyzer re-
             quires a valid VAX, MicroVAX or VAXstation configuration. For
             more information on valid system configurations, please refer
             to the VMS Operating System Software Product Description (SPD
             25.01.xx).

          Terminals

          To use the Language-Sensitive Editor and Source Code Analyzer
          component's Character Cell Interface:

          o  A VT1xx, VT2xx, VT3xx or ANSI CRT



                                          2

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A



          To use the Language-Sensitive Editor component's DECwindows
          Interface:

          o  A valid DECwindows workstation configuration

          Block Space Requirements (Block Cluster Size = 1):

          For the Language-Sensitive Editor component only with no tem-
          plates installed:

          Disk space required for      10,000 blocks
          installation:
                                       (5.1M bytes)

          Disk space required for      6,700 blocks
          permanent use:
                                       (3.4M bytes)

          Note: Approximately 275 blocks should be added for each language
          you choose to have supported by the Editor.
          For installation of the Language-Sensitive Editor component with
          support for all the languages listed in List 1:

          Disk space required (min-    13,025 blocks
          imum, during installa-
          tion):
                                       (6.7M bytes)

          Disk space required          9,725 blocks
          (after installation):
                                       (5.0M bytes)

          For the Source Code Analyzer component only:

          Disk space required for      8,000 blocks
          installation:
                                       (4.1M bytes)


                                          3

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A





          Disk space required for      5,300 blocks
          permanent use:
                                       (2.75M bytes)

          These block counts refer to the disk space required on the
          system disk. The sizes are approximations; actual sizes may
          vary depending on the user's system environment, configuration
          and software options.

          Memory Requirements for DECwindows Support

          The minimum supported memory for the Language-Sensitive Editor
          component running in a standalone DECwindows environment with
          both the client and server executing on that same system is 8
          MB.

          The performance and memory usage of DECwindows applications are
          particularly sensitive to system configuration. Less memory may
          be required on the client system (the system where the software
          is installed and executed if the server (the component that
          displays the application) resides on another system. More memory
          may be required on a system with several applications running or
          may be desirable to improve the performance of an application.

          OPTIONAL HARDWARE

          None

          CLUSTER ENVIRONMENT

          This layered product is fully supported when installed on any
          valid and licensed VAXcluster* configuration without restric-
          tions. The HARDWARE REQUIREMENTS sections of this product's
          Software Product Description and System Support Addendum detail
          any special hardware required by this product.


                                          4

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A



          *  V5.x VAXcluster configurations are fully described in the
             VAXcluster Software Product Description (29.78.xx) and in-
             clude CI, Ethernet, and Mixed Interconnect configurations.

          SOFTWARE REQUIREMENTS

          For systems using terminals: (No DECwindows interface)

          o  VMS Operating System V5.2 - V5.4

          For workstations running VWS:

          o  VMS Operating System V5.2 - V5.4

          o  VMS Workstation Software V4.2

          For workstations running DECwindows:

          o  VMS Operating System (and necessary components of VMS DECwin-
             dows) V5.2 - V5.4

          The Language-Sensitive Editor component may run in either of the
          following ways:

          o  Stand-alone execution - Running the X11 display server and
             the client application on the same machine.

          o  Remote execution - Running the X11 display server and the
             client application on different machines.

          VMS DECwindows is part of the VMS Operating System but must
          be installed separately. Installation of VMS DECwindows gives
          you the option to install any or all of the following three
          components:

          o  VMS DECwindows Compute Server (Base kit; provides runtime
             support)

          o  VMS DECwindows Device support

          o  VMS DECwindows Programming Support

                                          5

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A



          For stand-alone execution, the following DECwindows component
          must be installed on the machine:

          o  VMS DECwindows Compute Server (Base kit; provides runtime
             support)

          For remote execution, the following DECwindows component must be
          installed on the machines:

          Server Machine

          o  VMS DECwindows Compute Server (Base Kit; provides runtime
             support)

          Client Machine

          o  VMS DECwindows Compute Server (Base Kit; provides runtime
             support)

          Note: For full use of the Language-Sensitive Editor component
          features described in this SPD, at least one of the VAX lan-
          guages in List 1 or List 2 is required.

          For full use of the Source Code Analyzer component features
          described in this SPD, at least one of the VAX languages in
          List 1, or VAX SCAN in List 2, and the Language-Sensitive Editor
          component are required. (The Language-Sensitive Editor component
          is not required to initally install the Source Code Analyzer
          component.

          Certain features of this product are not available when using
          language pre-processors such as those supplied for VAX Rdb/VMS
          and VAX DBMS. Consult your local Digital representative for
          additional information.

          List 1

          The following list specifies VAX languages which are fully
          supported by the VAX Language-Sensitive Editor/Source Code
          Analyzer. Templates for those languages are included with the
          Language-Sensitive Editor component.

                                          6

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A



          - VAX Ada V2.1
          - VAX BASIC V3.4
          - VAX BLISS-32 Implementation Language V4.6
          - VAX C V3.1
          - VAX COBOL V4.3
          - VAX FORTRAN V5.4 - V5.5
          - VAX MACRO V5.2
          - VAX PASCAL V4.0 - V4.1
          - VAX PL/I V3.3 - V3.4

          List 2

          The following list specifies VAX languages which are fully
          supported by the Language-Sensitive Editor component. Templates
          for these languages are included with the product and not the
          Language-Sensitive Editor component.

          - VAX CDD/Plus V4.2
          - VAX DOCUMENT V1.0 - V1.2
          - VAX DIBOL V4.1
          - VAXELN PASCAL V4.0
          - VAX SCAN V1.2
          - VAX Rdb/VMS V3.1

          List 3

          The following list specifies VAX languages which have partial
          support for the Language-Sensitive Editor component (Refer
          to the product 's SPD for more details). Templates for these
          products are included with the product and not the Language-
          Sensitive Editor component.

          - VAX ACMS V3.1
          - VAX DATATRIEVE V5.0
          - VAX TDMS V1.9
          - VAX DECwindows with VMS V5.2 - V5.4

          List 4

                                          7

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A



          The following list specifies VAX languages which process pseu-
          docode entry information for the Source Code Analyzer component:

          - VAX Ada V2.1
          - VAX BASIC V3.4
          - VAX BLISS-32 Implementation Language V4.6
          - VAX C V3.1
          - VAX COBOL V4.3
          - VAX FORTRAN V5.4 - V5.5
          - VAX PASCAL V4.0 - V4.1

          VMS Tailoring

          The following VMS classes are required for full functionality of
          this layered product:

          o  VMS Required Saveset

          o  Utilities

          For more information on VMS classes and tailoring, refer to
          the VMS Operating System Software Product Description (SPD
          25.01.xx).

          OPTIONAL SOFTWARE

          VAX DEC/CMS V3.4
          VAX FORTRAN High-Performance Option V1.0

          GROWTH CONSIDERATIONS

          The minimum hardware/software requirements for any future ver-
          sion of this product may be different from the minimum require-
          ments for the current version.

          DISTRIBUTION MEDIA

          Tape: 9-track 1600 BPI Magtape (PE), TK50 Streaming Tape

          This product is also available as part of the VMS Consolidated
          Software Distribution on CDROM.

                                          8

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A



          ORDERING INFORMATION

          Software Licenses: QL-057A*-**
          Software Media: QA-057A*-**
          Software Documentation: QA-057AA-GZ
          Software Product Services: QT-057A*-**

          *  Denotes variant fields. For additional information on avail-
             able licenses, services and media, refer to the appropriate
             price book.





























                                          9

 


          VAX Language-Sensitive Editor/Source Code AnalyzerSSA 26.59.11-A



          The above information is valid at time of release. Please con-
          tact your local Digital office for the most up-to-date informa-
          tion.

          [TM]  The DIGITAL Logo, VMS, DECwindows, VAX, MicroVAX, VAXs-
                tation, VAXserver and VAXset are trademarks of Digital
                Equipment Corporation.
































                                         10
