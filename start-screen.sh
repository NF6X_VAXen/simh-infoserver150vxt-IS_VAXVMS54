#!/bin/sh
#
# Start the emulation in a detachable screen session.
# Escape key is control-p instead of the default control-e,
# leaving control-e available for interrupting simh.

screen -e^Pp -h 2048 -S IS_VAXVMS54 infoserver150vxt IS_VAXVMS54.ini 
